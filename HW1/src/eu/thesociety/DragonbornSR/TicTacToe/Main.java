package eu.thesociety.DragonbornSR.TicTacToe;


/**
 * Class that calls out game and starts it.
 * @author ROG
 *
 */
public class Main {

	/**
	 * Main method.
	 * @param args
	 */
	public static void main(String[] args) {
		TicTacToe t = new TicTacToe();
		try {
			t.Run();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
