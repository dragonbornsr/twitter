package eu.thesociety.DragonbornSR.TicTacToe;

import org.lwjgl.opengl.GL11;
import org.lwjgl.util.vector.Vector2f;
import org.newdawn.slick.Color;
import org.newdawn.slick.opengl.Texture;
import org.newdawn.slick.opengl.TextureImpl;

import com.coffeagame.Engine.Game;
import com.coffeagame.Engine.Utility.TextureTools;
import com.coffeagame.Engine.event.interfaces.IMouseEvent;
import com.coffeagame.Engine.event.types.MouseEvent;
import com.coffeagame.Engine.gui.Gui;
import com.coffeagame.Engine.gui.GuiTile;
import com.coffeagame.Engine.media.ResourceManager;

/**
 * Tile that represents a square on a play table
 * where player or AI can place his turn (X or O).
 * @author ROG
 *
 */
public class TileSquare extends GuiTile implements IMouseEvent {

	/** Boolean that holds if game is paused. */
	public boolean isGamePaused;
	
	/**
	 * Enum that describes owner.
	 * @author ROG
	 *
	 */
	public enum Owner {
		/** No owner. */
		NULL (0),
		/** Owner is player. */
		PLAYER (-1),
		/** Owner is AI. */
		AI (1);
		/** owner. */ 
		private final int owner;
		/** Constructor. */
		Owner(int owner) {
			this.owner = owner;
		}
		/**
		 *  Returns the owner.
		 * @return Owner.
		 */
		public int getOwner() {
			return owner;
		}
	}
	
	/**
	 * Owner of the square on the play table.
	 * @author ROG
	 *
	 */
	public Owner owner = Owner.NULL;
	
	/**
	 * States, if mouse is currently hovering the.
	 * 
	 */
	private boolean hoverState = false;
	
	/**
	 * Texture of the tile.
	 */
	private Texture tex;
	
	/**
	 * Corners of the texture, allowing off-setting the texture to change
	 * the look accordingly to hover and the owner.
	 */
	private Vector2f c1 = new Vector2f(0, 0),
			 		 c2 = new Vector2f(0, 0),
			 		 c3 = new Vector2f(0, 0),
			 		 c4 = new Vector2f(0, 0);
	
	/**
	 * Initialize the tile.
	 * @param parent parent
	 * @param tileID tile 
	 */
	public TileSquare(Gui parent, int tileID) {
		super(parent, tileID);
		tex = ResourceManager.getTexture("tiles");
		this.texOffY = 170;
		this.setSize(170, 170);
		updateTextureCorners();
	}
	
	/**
	 * Constructor.
	 * @param parent parent
	 * @param tileID tile
	 * @param posX tile pos X
	 * @param posY tile pos y
	 */
	public TileSquare(Gui parent, int tileID, int posX, int posY) {
		this(parent, tileID);
		this.setPosition(posX, posY);
	}

	
	@Override
	public void update(float delta) {
		updateTextureCorners();
		if ((this.owner == Owner.NULL) && (!isGamePaused)) {
			if ((this.hover) && (!this.hoverState)) {
				this.texOffY = 170;
				this.texOffX = 170;
				this.updateTextureCorners();
				this.texOffX = 0;
			} else 
			if ((!this.hover) && (this.hoverState)) {
				this.texOffY = 170;
				this.texOffX = 0;
				this.updateTextureCorners();
			}		
		}
	}
	
	@Override
	public void draw(float Update) {
		if (parent.visible) {
			Game.startGLMode2D();
			GL11.glPushMatrix();
				GL11.glBlendFunc(GL11.GL_SRC_ALPHA, 
						GL11.GL_ONE_MINUS_SRC_ALPHA);
				GL11.glEnable(GL11.GL_BLEND);
				TextureImpl.bindNone();
				Color.white.bind();
				tex.bind();
				GL11.glBegin(GL11.GL_QUADS);
					GL11.glTexCoord2f(c1.x,c1.y);
					GL11.glVertex2f(posX, posY);
					GL11.glTexCoord2f(c2.x,c2.y);
					GL11.glVertex2f(posX + width, posY);
					GL11.glTexCoord2f(c3.x,c3.y);
					GL11.glVertex2f(posX + width, 
							posY + height);
					GL11.glTexCoord2f(c4.x,c4.y);
					GL11.glVertex2f(posX, posY + height);
				GL11.glEnd();
			GL11.glPopMatrix();
			Game.endGLMode2D();
		}
	}
	
	/**
	 * Player pushed that tile. If that tile is free,
	 * set player as tile owner.
	 */
	@Override
	public void mousePressed(MouseEvent e) {
		if ((inBounds(e)) && (this.owner == Owner.NULL)
				&& (e.key == 0) && (!isGamePaused)) {
			this.owner = Owner.PLAYER;
			this.texOffX = 340;
			this.texOffY = 170;
			updateTextureCorners();
			super.mousePressed(e);
		}
		
	}

	/**
	 * Method to update texture offset.
	 */
	public void updateTextureCorners() {
		c1 = TextureTools.getTextureCorner(this.tex, texOffX, texOffY);
		c2 = TextureTools.getTextureCorner(this.tex, 
				texOffX + 170, texOffY);
		c3 = TextureTools.getTextureCorner(this.tex, 
				texOffX + 170, texOffY + 170);
		c4 = TextureTools.getTextureCorner(this.tex, 
				texOffX, texOffY + 170);
	}
	
	/**
	 * Set tile ownership back to default (Owner.NULL) 
	 * and set texture back to default.
	 */
	public void reset() {
		this.owner = Owner.NULL;
		this.texOffX = 0;
		updateTextureCorners();
	}
	
	/**
	 * Set tile owner to AI.
	 */
	public void setOwnerAI() {
		this.texOffX = 340;
		this.texOffY = 0;
		this.updateTextureCorners();
		this.owner = Owner.AI;
	}
}
