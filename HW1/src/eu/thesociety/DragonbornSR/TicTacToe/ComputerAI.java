package eu.thesociety.DragonbornSR.TicTacToe;

import eu.thesociety.DragonbornSR.TicTacToe.GameLogic.Combination;
import eu.thesociety.DragonbornSR.TicTacToe.TileSquare.Owner;


/**
 * AI base class that holds methods to decide different move tactics.
 * @author ROG
 *
 */
public class ComputerAI {

	/**
	 * Game logic is GUI that holds game components.
	 */
	protected GameLogic parent;
	
	/**
	 * Make AI move.
	 * @return true if move was made.
	 */
	public boolean makeMove() {
		return false;
	}
	
	/**
	 * Get AI name.
	 * @return AI name.
	 */
	public String getName() {
		return "NULL";
	}
	
	/**
	 * Checks if some combination is filled with 2 same 
	 * player combinations and suggests move/counter-move, 
	 * depending on perspective (if owner is AI, then move, if
	 * PLAYER, then counter-move). Returns null if there is 
	 * no combinations avabile.
	 * @param owner perspective.
	 * @return suggested tile
	 */
	protected TileSquare getTwoInRow(Owner owner) {
		for (Combination c : parent.comb) {
			if ((c.a.owner == c.b.owner) 
					&& (c.c.owner == Owner.NULL) 
					&& (c.a.owner == owner)) {
				return c.c;
			}
			if ((c.b.owner == c.c.owner) 
					&& (c.a.owner == Owner.NULL) 
					&& (c.b.owner == owner)) {
				return c.a;
			}
			if ((c.a.owner == c.c.owner) 
					&& (c.b.owner == Owner.NULL) 
					&& (c.a.owner == owner)) {
				return c.b;
			}
		}
		return null;
	}
	
	/**
	 * Look if you can take a tile in the opposite corner owned by 'owner'.
	 * @param owner perspective
	 * @return suggested move.
	 */
	protected TileSquare getOppositeCorner(Owner owner) {
		if ((parent.q1.owner == owner) 
				&& (parent.q9.owner == Owner.NULL)) {
			return parent.q9;
		}
		if ((parent.q9.owner == owner) 
				&& (parent.q1.owner == Owner.NULL)) {
			return parent.q1;
		}
		if ((parent.q7.owner == owner) 
				&& (parent.q3.owner == Owner.NULL)) {
			return parent.q3;
		}
		if ((parent.q3.owner == owner) 
				&& (parent.q7.owner == Owner.NULL)) {
			return parent.q7;
		}
		return null;
	}
	
	/**
	 * Look if you can take a tile opposing to other owned by 'owner'.
	 * @param owner perspective
	 * @return suggested move.
	 */
	protected TileSquare getOppositeFace(Owner owner) {
		if ((parent.q2.owner == owner) 
				&& (parent.q8.owner == Owner.NULL)) {
			return parent.q8;
		}
		if ((parent.q8.owner == owner) 
				&& (parent.q2.owner == Owner.NULL)) {
			return parent.q2;
		}
		if ((parent.q4.owner == owner) 
				&& (parent.q6.owner == Owner.NULL)) {
			return parent.q6;
		}
		if ((parent.q6.owner == owner) 
				&& (parent.q4.owner 
						== Owner.NULL)) {
			return parent.q4;
		}
		return null;
	}
	
	/**
	 * Look if you can take a corner next to other owned by 'owner'.
	 * @param owner (Perspective)
	 * @return TileSquare (Preferred move)
	 */
	protected TileSquare getNextCorner(Owner owner) {
		if ((parent.q1.owner == owner) 
				&& (parent.q3.owner == Owner.NULL)) {
			return parent.q3;
		}
		if ((parent.q3.owner == owner) 
				&& (parent.q1.owner == Owner.NULL)) {
			return parent.q1;
		}
		if ((parent.q7.owner == owner) 
				&& (parent.q9.owner == Owner.NULL)) {
			return parent.q9;
		}
		if ((parent.q9.owner == owner) 
				&& (parent.q7.owner == Owner.NULL)) {
			return parent.q7;
		}
		if ((parent.q7.owner == owner) 
				&& (parent.q1.owner == Owner.NULL)) {
			return parent.q1;
		}
		if ((parent.q1.owner == owner) 
				&& (parent.q7.owner == Owner.NULL)) {
			return parent.q7;
		}
		return null;
	}
	
	/**
	 * Look if there is situation where owner has to opposite 
	 * corners at his proposal, make contermove.
	 * @param owner perspective
	 * @return suggested move.
	 */
	protected TileSquare getJumpoverSituation(Owner owner) {
		if ((parent.q1.owner == owner) && (parent.q9.owner == owner)
				|| (parent.q3.owner == owner) 
				&& (parent.q7.owner == owner)) {
			if (parent.q2.owner == Owner.NULL) {
				return parent.q2;
			}
			if (parent.q6.owner == Owner.NULL) {
				return parent.q6;
			}
			if (parent.q8.owner == Owner.NULL) {
				return parent.q2;
			}
			if (parent.q4.owner == Owner.NULL) {
				return parent.q4;
			}
		}
		return null;
	}
	
	/**
	 * Look if there is situation where enemy has corner that
	 * you can put your tile next to!
	 * @param owner perspective
	 * @return suggested tile.
	 */
	protected TileSquare getNextToFace(Owner owner) {
		if (parent.q2.owner == owner) {
			if (parent.q1.owner == Owner.NULL) {
				return parent.q1;
			}
			if (parent.q3.owner == Owner.NULL) {
				return parent.q3;
			}
		}
		if (parent.q4.owner == owner) {
			if (parent.q7.owner == Owner.NULL) {
				return parent.q7;
			}
			if (parent.q1.owner == Owner.NULL) {
				return parent.q1;
			}
		}
		if (parent.q8.owner == owner) {
			if (parent.q7.owner == Owner.NULL) {
				return parent.q7;
			}
			if (parent.q9.owner == Owner.NULL) {
				return parent.q9;
			}
		}
		if (parent.q6.owner == owner) {
			if (parent.q9.owner == Owner.NULL) {
				return parent.q9;
			}
			if (parent.q3.owner == Owner.NULL) {
				return parent.q3;
			}
		}	
		return null;
	}
}
