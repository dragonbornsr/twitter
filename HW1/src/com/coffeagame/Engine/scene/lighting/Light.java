package com.coffeagame.Engine.scene.lighting;

import java.nio.FloatBuffer;

import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;

public class Light {
	float[] ambient = { 0.2f, 0.2f, 0.2f, 1.0f };
	float[] diffuse = { 1.0f, 1.0f, 1.0f, 1.0f };
	float[] specular ={ 1.0f, 1.0f, 1.0f, 1.0f };
	float[] position = { 200.0f, 300.0f, 100.0f, 0.0f };
	
	public void Light() {
		//GL11.glLight(GL11.GL_LIGHT0, GL11.GL_AMBIENT, asFloatBuffer(ambient));
		GL11.glLight(GL11.GL_LIGHT0, GL11.GL_DIFFUSE, asFloatBuffer(diffuse));
		GL11.glLight(GL11.GL_LIGHT0, GL11.GL_SPECULAR, asFloatBuffer(specular));
		GL11.glLight(GL11.GL_LIGHT0, GL11.GL_POSITION, asFloatBuffer(position));
		GL11.glEnable(GL11.GL_LIGHT0);
		GL11.glEnable(GL11.GL_LIGHTING);
		GL11.glEnable(GL11.GL_COLOR_MATERIAL);
		GL11.glShadeModel(GL11.GL_SMOOTH);
	}
	
	protected FloatBuffer asFloatBuffer(float[] f) {
		FloatBuffer buffer = BufferUtils.createFloatBuffer(f.length);
		buffer.put(f);
		buffer.flip();
		return buffer;
	}
}
