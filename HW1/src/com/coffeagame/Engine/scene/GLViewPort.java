/*
	Copyright (c) 2014, Coffea Games
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	1. Redistributions of source code must retain the above copyright
	   notice, this list of conditions and the following disclaimer.
	2. Redistributions in binary form must reproduce the above copyright
	   notice, this list of conditions and the following disclaimer in the
	   documentation and/or other materials provided with the distribution.
	3. All advertising materials mentioning features or use of this software
	   must display the following acknowledgement:
	   This product includes software developed by the Coffea Games.
	4. Neither the name of the Coffea Games nor the
	   names of its contributors may be used to endorse or promote products
	   derived from this software without specific prior written permission.
	
	THIS SOFTWARE IS PROVIDED BY COFFEA GAMES ''AS IS'' AND ANY
	EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
	WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL COFFEA GAMES BE LIABLE FOR ANY
	DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
	(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
	LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
	ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
	(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
package com.coffeagame.Engine.scene;

import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.ByteBuffer;

import javax.imageio.ImageIO;

import org.lwjgl.LWJGLException;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;
import org.lwjgl.opengl.GLContext;
import org.lwjgl.util.glu.GLU;
import org.lwjgl.util.vector.Vector3f;
import org.newdawn.slick.util.ResourceLoader;

import com.coffeagame.Engine.Game;
import com.coffeagame.Engine.Objects.CoreObject;
import com.coffeagame.Engine.Objects.Cursor;
import com.coffeagame.Engine.Utility.Camera;
import com.coffeagame.Engine.event.interfaces.IKeyEvent;
import com.coffeagame.Engine.event.interfaces.IMouseEvent;
import com.coffeagame.Engine.event.types.KeyEvent;
import com.coffeagame.Engine.event.types.MouseEvent;

public class GLViewPort extends CoreObject implements IKeyEvent, IMouseEvent,
		Camera {

	protected static Vector3f position = new Vector3f(0, 0, 0);
	protected static Vector3f rotation = new Vector3f(0, 0, 0);
	protected static float walkingSpeed = 50;
	protected static float fov = 90;
	protected static float aspectRatio = 1;
	protected static float zNear = 0.01f;
	protected static float zFar = 100f;
	protected static float mouseSpeed = 2f;

	private GLViewPort(Builder builder) {
		register();
		GLViewPort.position.x = builder.x;
		GLViewPort.position.y = builder.y;
		GLViewPort.position.z = builder.z;
		GLViewPort.rotation.x = builder.pitch;
		GLViewPort.rotation.y = builder.yaw;
		GLViewPort.rotation.z = builder.roll;
		GLViewPort.aspectRatio = builder.aspectRatio;
		GLViewPort.zNear = builder.zNear;
		GLViewPort.zFar = builder.zFar;
		GLViewPort.fov = builder.fov;
		Mouse.setGrabbed(true);
	}

	@Override
	public void update(float delta) {
		if (Cursor.cursorMode == 1) {
			processKeyboard(delta);
			processMouse(delta);
			applyTranslations();
		} else if (Cursor.cursorMode == 0) {
			Cursor.updateCursor(Mouse.getDX(), Mouse.getDY());
		}

		/*
		 * if (Display.isVisible()) Mouse.setCursorPosition(Game.SCREEN_WIDTH/2,
		 * Game.SCREEN_HEIGHT/2);
		 */
	}

	/**
	 * Creates a new camera with the given aspect ratio. It's located at [0 0 0]
	 * with the orientation [0 0 0]. It has a zNear of 0.3, a zFar of 100.0, and
	 * an fov of 90.
	 */
	public GLViewPort() {
		register();
		GLViewPort.zNear = 0.3f;
		GLViewPort.zFar = 100;
	}

	/**
	 * Creates a new camera with the given aspect ratio. It's located at [0 0 0]
	 * with the orientation [0 0 0]. It has a zNear of 0.3, a zFar of 100.0, and
	 * an fov of 90.
	 * 
	 * @param aspectRatio
	 *            the aspect ratio (width/height) of the camera
	 * 
	 * @throws IllegalArgumentException
	 *             if aspectRatio is 0 or smaller than 0
	 */
	public GLViewPort(float aspectRatio) {
		register();
		if (aspectRatio <= 0) {
			throw new IllegalArgumentException("aspectRatio " + aspectRatio
					+ " was 0 or was smaller than 0");
		}
		GLViewPort.aspectRatio = aspectRatio;
		GLViewPort.zNear = 0.3f;
		GLViewPort.zFar = 100;
	}

	/**
	 * Creates a new camera with the given aspect ratio and location.
	 * 
	 * @param aspectRatio
	 *            the aspect ratio (width/height) of the camera
	 * @param x
	 *            the first location coordinate
	 * @param y
	 *            the second location coordinate
	 * @param z
	 *            the third location coordinate
	 * 
	 * @throws IllegalArgumentException
	 *             if aspectRatio is 0 or smaller than 0
	 */
	public GLViewPort(float aspectRatio, float x, float y, float z) {
		this(aspectRatio);
		GLViewPort.position.x = x;
		GLViewPort.position.y = y;
		GLViewPort.position.z = z;
	}

	/**
	 * Creates a new camera with the given aspect ratio, location, and
	 * orientation.
	 * 
	 * @param aspectRatio
	 *            the aspect ratio (width/height) of the camera
	 * @param x
	 *            the first location coordinate
	 * @param y
	 *            the second location coordinate
	 * @param z
	 *            the third location coordinate
	 * @param pitch
	 *            the pitch (rotation on the x-axis)
	 * @param yaw
	 *            the yaw (rotation on the y-axis)
	 * @param roll
	 *            the roll (rotation on the z-axis)
	 * 
	 * @throws IllegalArgumentException
	 *             if aspectRatio is 0 or smaller than 0
	 */
	public GLViewPort(float aspectRatio, float x, float y, float z,
			float pitch, float yaw, float roll) {
		this(aspectRatio, x, y, z);
		GLViewPort.rotation.x = pitch;
		GLViewPort.rotation.y = yaw;
		GLViewPort.rotation.z = roll;
	}

	/**
	 * Creates a new camera with the given aspect ratio, location, zNear, zFar
	 * and orientation.
	 * 
	 * @param aspectRatio
	 *            the aspect ratio (width/height) of the camera
	 * @param x
	 *            the first location coordinate
	 * @param y
	 *            the second location coordinate
	 * @param z
	 *            the third location coordinate
	 * @param pitch
	 *            the pitch (rotation on the x-axis)
	 * @param yaw
	 *            the yaw (rotation on the y-axis)
	 * @param roll
	 *            the roll (rotation on the z-axis)
	 * 
	 * @throws IllegalArgumentException
	 *             if aspectRatio is 0 or smaller than 0 or if zNear is 0 or
	 *             smaller than 0 or if zFar is the same or smaller than zNear
	 */
	public GLViewPort(float aspectRatio, float x, float y, float z,
			float pitch, float yaw, float roll, float zNear, float zFar) {
		register();
		if (aspectRatio <= 0) {
			throw new IllegalArgumentException("aspectRatio " + aspectRatio
					+ " was 0 or was smaller than 0");
		}
		if (zNear <= 0) {
			throw new IllegalArgumentException("zNear " + zNear
					+ " was 0 or was smaller than 0");
		}
		if (zFar <= zNear) {
			throw new IllegalArgumentException("zFar " + zFar
					+ " was smaller or the same as zNear " + zNear);
		}
		GLViewPort.aspectRatio = aspectRatio;
		GLViewPort.position.x = x;
		GLViewPort.position.y = y;
		GLViewPort.position.z = z;
		GLViewPort.rotation.x = pitch;
		GLViewPort.rotation.y = yaw;
		GLViewPort.rotation.z = roll;
		GLViewPort.zNear = zNear;
		GLViewPort.zFar = zFar;
	}

	@Override
	public void processMouse(float delta) {
		float mouseDX = (float) Mouse.getDX();
		float mouseDY = (float) Mouse.getDY();
		Cursor.updateCursor(mouseDX, mouseDY);
		mouseDX = mouseDX * mouseSpeed * 0.16f;
		mouseDY = mouseDY * mouseSpeed * 0.16f;

		// yaw - y - max 89
		// pitch - x - max 360

		rotation.y = rotation.y + mouseDX;
		rotation.x = rotation.x - mouseDY;

		if (rotation.y > 360)
			rotation.y = rotation.y - 360;
		if (rotation.y < 0)
			rotation.y = -rotation.y + 360;

		if (rotation.x > 89)
			rotation.x = 89;
		if (rotation.x < -89)
			rotation.x = -89;
	}

	/**
	 * Processes keyboard input and converts into camera movement.
	 * 
	 * @param delta
	 *            the elapsed time since the last frame update in milliseconds
	 * 
	 * @throws IllegalArgumentException
	 *             if delta is 0 or delta is smaller than 0
	 */
	public void processKeyboard(float delta) {
		boolean keyUp = Keyboard.isKeyDown(Keyboard.KEY_UP)
				|| Keyboard.isKeyDown(Keyboard.KEY_W);
		boolean keyDown = Keyboard.isKeyDown(Keyboard.KEY_DOWN)
				|| Keyboard.isKeyDown(Keyboard.KEY_S);
		boolean keyLeft = Keyboard.isKeyDown(Keyboard.KEY_LEFT)
				|| Keyboard.isKeyDown(Keyboard.KEY_A);
		boolean keyRight = Keyboard.isKeyDown(Keyboard.KEY_RIGHT)
				|| Keyboard.isKeyDown(Keyboard.KEY_D);
		boolean flyUp = Keyboard.isKeyDown(Keyboard.KEY_SPACE);
		boolean flyDown = Keyboard.isKeyDown(Keyboard.KEY_LSHIFT);
		boolean moveFaster = Keyboard.isKeyDown(Keyboard.KEY_LCONTROL);
		boolean moveSlower = Keyboard.isKeyDown(Keyboard.KEY_TAB);

		if (moveFaster && !moveSlower) {
			walkingSpeed *= 4f;
		}
		if (moveSlower && !moveFaster) {
			walkingSpeed /= 10f;
		}

		if (keyUp && keyRight && !keyLeft && !keyDown) {
			float angle = rotation.y + 45;
			Vector3f newPosition = new Vector3f(position);
			float hypotenuse = (walkingSpeed * 0.0002f) * delta;
			float adjacent = hypotenuse
					* (float) Math.cos(Math.toRadians(angle));
			float opposite = (float) (Math.sin(Math.toRadians(angle)) * hypotenuse);
			newPosition.z += adjacent;
			newPosition.x -= opposite;
			position.z = newPosition.z;
			position.x = newPosition.x;
		}
		if (keyUp && keyLeft && !keyRight && !keyDown) {
			float angle = rotation.y - 45;
			Vector3f newPosition = new Vector3f(position);
			float hypotenuse = (walkingSpeed * 0.0002f) * delta;
			float adjacent = hypotenuse
					* (float) Math.cos(Math.toRadians(angle));
			float opposite = (float) (Math.sin(Math.toRadians(angle)) * hypotenuse);
			newPosition.z += adjacent;
			newPosition.x -= opposite;
			position.z = newPosition.z;
			position.x = newPosition.x;
		}
		if (keyUp && !keyLeft && !keyRight && !keyDown) {
			float angle = rotation.y;
			Vector3f newPosition = new Vector3f(position);
			float hypotenuse = (walkingSpeed * 0.0002f) * delta;
			float adjacent = hypotenuse
					* (float) Math.cos(Math.toRadians(angle));
			float opposite = (float) (Math.sin(Math.toRadians(angle)) * hypotenuse);
			newPosition.z += adjacent;
			newPosition.x -= opposite;
			position.z = newPosition.z;
			position.x = newPosition.x;
		}
		if (keyDown && keyLeft && !keyRight && !keyUp) {
			float angle = rotation.y - 135;
			Vector3f newPosition = new Vector3f(position);
			float hypotenuse = (walkingSpeed * 0.0002f) * delta;
			float adjacent = hypotenuse
					* (float) Math.cos(Math.toRadians(angle));
			float opposite = (float) (Math.sin(Math.toRadians(angle)) * hypotenuse);
			newPosition.z += adjacent;
			newPosition.x -= opposite;
			position.z = newPosition.z;
			position.x = newPosition.x;
		}
		if (keyDown && keyRight && !keyLeft && !keyUp) {
			float angle = rotation.y + 135;
			Vector3f newPosition = new Vector3f(position);
			float hypotenuse = (walkingSpeed * 0.0002f) * delta;
			float adjacent = hypotenuse
					* (float) Math.cos(Math.toRadians(angle));
			float opposite = (float) (Math.sin(Math.toRadians(angle)) * hypotenuse);
			newPosition.z += adjacent;
			newPosition.x -= opposite;
			position.z = newPosition.z;
			position.x = newPosition.x;
		}
		if (keyDown && !keyUp && !keyLeft && !keyRight) {
			float angle = rotation.y;
			Vector3f newPosition = new Vector3f(position);
			float hypotenuse = -(walkingSpeed * 0.0002f) * delta;
			float adjacent = hypotenuse
					* (float) Math.cos(Math.toRadians(angle));
			float opposite = (float) (Math.sin(Math.toRadians(angle)) * hypotenuse);
			newPosition.z += adjacent;
			newPosition.x -= opposite;
			position.z = newPosition.z;
			position.x = newPosition.x;
		}
		if (keyLeft && !keyRight && !keyUp && !keyDown) {
			float angle = rotation.y - 90;
			Vector3f newPosition = new Vector3f(position);
			float hypotenuse = (walkingSpeed * 0.0002f) * delta;
			float adjacent = hypotenuse
					* (float) Math.cos(Math.toRadians(angle));
			float opposite = (float) (Math.sin(Math.toRadians(angle)) * hypotenuse);
			newPosition.z += adjacent;
			newPosition.x -= opposite;
			position.z = newPosition.z;
			position.x = newPosition.x;
		}
		if (keyRight && !keyLeft && !keyUp && !keyDown) {
			float angle = rotation.y + 90;
			Vector3f newPosition = new Vector3f(position);
			float hypotenuse = (walkingSpeed * 0.0002f) * delta;
			float adjacent = hypotenuse
					* (float) Math.cos(Math.toRadians(angle));
			float opposite = (float) (Math.sin(Math.toRadians(angle)) * hypotenuse);
			newPosition.z += adjacent;
			newPosition.x -= opposite;
			position.z = newPosition.z;
			position.x = newPosition.x;
		}
		if (flyUp && !flyDown) {
			double newPositionY = (walkingSpeed * 0.0002) * delta;
			position.y -= newPositionY;
		}
		if (flyDown && !flyUp) {
			double newPositionY = (walkingSpeed * 0.0002) * delta;
			position.y += newPositionY;
		}
		if (moveFaster && !moveSlower) {
			walkingSpeed /= 4f;
		}
		if (moveSlower && !moveFaster) {
			walkingSpeed *= 10f;
		}
	}

	/**
	 * Move in the direction you're looking. That is, this method assumes a new
	 * coordinate system where the axis you're looking down is the z-axis, the
	 * axis to your left is the x-axis, and the upward axis is the y-axis.
	 * 
	 * @param dx
	 *            the movement along the x-axis
	 * @param dy
	 *            the movement along the y-axis
	 * @param dz
	 *            the movement along the z-axis
	 */
	public void moveFromLook(float dx, float dy, float dz) {

	}

	/**
	 * Sets the position of the camera.
	 * 
	 * @param x
	 *            the x-coordinate of the camera
	 * @param y
	 *            the y-coordinate of the camera
	 * @param z
	 *            the z-coordinate of the camera
	 */
	public void setPosition(float x, float y, float z) {
		GLViewPort.position.x = x;
		GLViewPort.position.y = y;
		GLViewPort.position.z = z;
	}

	/**
	 * Sets GL_PROJECTION to an orthographic projection matrix. The matrix mode
	 * will be returned it its previous value after execution.
	 */
	public void applyOrthographicMatrix() {
		GL11.glPushAttrib(GL11.GL_TRANSFORM_BIT);
		GL11.glMatrixMode(GL11.GL_PROJECTION);
		GL11.glLoadIdentity();
		GL11.glOrtho(-aspectRatio, aspectRatio, -1, 1, 0, zFar);
		GL11.glPopAttrib();
	}

	/**
	 * Enables or disables OpenGL states that will enhance the camera
	 * appearance. Enable GL_DEPTH_CLAMP if ARB_depth_clamp is supported
	 */
	public void applyOptimalStates() {
		if (GLContext.getCapabilities().GL_ARB_depth_clamp) {
			GL11.glEnable(GL11.GL_DEPTH_TEST);
		}
	}

	/**
	 * Sets GL_PROJECTION to an perspective projection matrix. The matrix mode
	 * will be returned it its previous value after execution.
	 */
	public void applyPerspectiveMatrix() {
		GL11.glPushAttrib(GL11.GL_TRANSFORM_BIT);
		GL11.glMatrixMode(GL11.GL_PROJECTION);
		GL11.glLoadIdentity();
		GLU.gluPerspective(fov, aspectRatio, zNear, zFar);
		GL11.glPopAttrib();
	}

	public static void startGLMode2D() {
		GL11.glMatrixMode(GL11.GL_PROJECTION);
		GL11.glLoadMatrix(Game.orthographicProjectrionMatrix);
		GL11.glMatrixMode(GL11.GL_MODELVIEW);
		GL11.glPushMatrix();
		GL11.glLoadIdentity();
		GL11.glEnable(GL11.GL_BLEND);
		GL11.glDisable(GL11.GL_DEPTH_TEST);
		GL11.glDisable(GL11.GL_LIGHTING);
		GL11.glDisable(GL11.GL_LIGHT0);
		GL11.glDisable(GL11.GL_CULL_FACE);

	}

	public static void endGLMode2D() {
		GL11.glPopMatrix();
		GL11.glMatrixMode(GL11.GL_PROJECTION);
		GL11.glLoadMatrix(Game.perspectiveProjectionMatrix);
		GLU.gluPerspective(fov, aspectRatio, zNear, zFar);
		GL11.glMatrixMode(GL11.GL_MODELVIEW);
		GL11.glEnable(GL11.GL_DEPTH_TEST);
		GL11.glEnable(GL11.GL_CULL_FACE);
		GL11.glEnable(GL11.GL_LIGHTING);
		GL11.glEnable(GL11.GL_LIGHT0);
		GL11.glDisable(GL11.GL_BLEND);
	}

	/** Applies the camera translations and rotations to GL_MODELVIEW. */
	public void applyTranslations() {
		GL11.glLoadIdentity();
		GL11.glRotatef(rotation.x, 1, 0, 0);
		GL11.glRotatef(rotation.y, 0, 1, 0);
		GL11.glRotatef(rotation.z, 0, 0, 1);
		GL11.glTranslatef(position.x, position.y, position.z);
	}

	/**
	 * Sets the rotation of the camera.
	 * 
	 * @param pitch
	 *            the rotation around the x-axis in degrees
	 * @param yaw
	 *            the rotation around the y-axis in degrees
	 * @param roll
	 *            the rotation around the z-axis in degrees
	 */
	public void setRotation(float pitch, float yaw, float roll) {
		GLViewPort.rotation.x = pitch;
		GLViewPort.rotation.y = yaw;
		GLViewPort.rotation.z = roll;
	}

	/** @return the x-coordinate of the camera */
	public float x() {
		return position.x;
	}

	/** @return y the y-coordinate of the camera */
	public float y() {
		return position.y;
	}

	/** @return the z-coordinate of the camera */
	public float z() {
		return position.z;
	}

	/** @return the pitch of the camera in degrees */
	public float pitch() {
		return rotation.x;
	}

	/** @return the yaw of the camera in degrees */
	public float yaw() {
		return rotation.y;
	}

	/** @return the roll of the camera in degrees */
	public float roll() {
		return rotation.z;
	}

	/** @return the fov of the camera in degrees in the y direction */
	public float fieldOfView() {
		return fov;
	}

	/**
	 * Sets the field of view angle in degrees in the y direction. Note that
	 * this.applyPerspectiveMatrix() must be applied in order to see any
	 * difference.
	 * 
	 * @param fov
	 *            the field of view angle in degrees in the y direction
	 */
	public void setFieldOfView(float fov) {
		GLViewPort.fov = fov;
	}

	@Override
	public void setAspectRatio(float aspectRatio) {
		if (aspectRatio <= 0) {
			throw new IllegalArgumentException("aspectRatio " + aspectRatio
					+ " is 0 or less");
		}
		GLViewPort.aspectRatio = aspectRatio;
	}

	/** @return the aspect ratio of the camera */
	public float aspectRatio() {
		return aspectRatio;
	}

	/** @return the distance from the camera to the near clipping pane */
	public float nearClippingPane() {
		return zNear;
	}

	/** @return the distance from the camera to the far clipping pane */
	public float farClippingPane() {
		return zFar;
	}

	@Override
	public String toString() {
		return "Camera [x=" + position.x + ", y=" + position.y + ", z="
				+ position.z + ", pitch=" + rotation.x + ", yaw=" + rotation.y
				+ ", " + "roll=" + rotation.z + ", fov=" + fov
				+ ", aspectRatio=" + aspectRatio + ", zNear=" + zNear + ", "
				+ "zFar=" + zFar + "]";
	}

	/** A builder helper class for the EulerCamera class. */
	public static class Builder {

		private float aspectRatio = 1;
		private float x = 0, y = 0, z = 0, pitch = 0, yaw = 0, roll = 0;
		private float zNear = 0.3f;
		private float zFar = 100;
		private float fov = 90;

		public Builder() {

		}

		/**
		 * Sets the aspect ratio of the camera.
		 * 
		 * @param aspectRatio
		 *            the aspect ratio of the camera (window width / window
		 *            height)
		 * 
		 * @return this
		 */
		public Builder setAspectRatio(float aspectRatio) {
			if (aspectRatio <= 0) {
				throw new IllegalArgumentException("aspectRatio " + aspectRatio
						+ " was 0 or was smaller than 0");
			}
			this.aspectRatio = aspectRatio;
			return this;
		}

		/**
		 * Sets the distance from the camera to the near clipping pane.
		 * 
		 * @param nearClippingPane
		 *            the distance from the camera to the near clipping pane
		 * 
		 * @return this
		 * 
		 * @throws IllegalArgumentException
		 *             if nearClippingPane is 0 or less
		 */
		public Builder setNearClippingPane(float nearClippingPane) {
			if (nearClippingPane <= 0) {
				throw new IllegalArgumentException("nearClippingPane "
						+ nearClippingPane + " is 0 or less");
			}
			this.zNear = nearClippingPane;
			return this;
		}

		/**
		 * Sets the distance from the camera to the far clipping pane.
		 * 
		 * @param farClippingPane
		 *            the distance from the camera to the far clipping pane
		 * 
		 * @return this
		 * 
		 * @throws IllegalArgumentException
		 *             if farClippingPane is 0 or less
		 */
		public Builder setFarClippingPane(float farClippingPane) {
			if (farClippingPane <= 0) {
				throw new IllegalArgumentException("farClippingPane "
						+ farClippingPane + " is 0 or less");
			}
			this.zFar = farClippingPane;
			return this;
		}

		/**
		 * Sets the field of view angle in degrees in the y direction.
		 * 
		 * @param fov
		 *            the field of view angle in degrees in the y direction
		 * 
		 * @return this
		 */
		public Builder setFieldOfView(float fov) {
			this.fov = fov;
			return this;
		}

		/**
		 * Sets the position of the camera.
		 * 
		 * @param x
		 *            the x-coordinate of the camera
		 * @param y
		 *            the y-coordinate of the camera
		 * @param z
		 *            the z-coordinate of the camera
		 * 
		 * @return this
		 */
		public Builder setPosition(float x, float y, float z) {
			this.x = x;
			this.y = y;
			this.z = z;
			return this;
		}

		/**
		 * Sets the rotation of the camera.
		 * 
		 * @param pitch
		 *            the rotation around the x-axis in degrees
		 * @param yaw
		 *            the rotation around the y-axis in degrees
		 * @param roll
		 *            the rotation around the z-axis in degrees
		 */
		public Builder setRotation(float pitch, float yaw, float roll) {
			this.pitch = pitch;
			this.yaw = yaw;
			this.roll = roll;
			return this;
		}

		/**
		 * Constructs an instance of EulerCamera from this builder helper class.
		 * 
		 * @return an instance of EulerCamera
		 * 
		 * @throws IllegalArgumentException
		 *             if farClippingPane is the same or less than
		 *             nearClippingPane
		 */
		public GLViewPort build() {
			try {
				setupGLDisplay();
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if (zFar <= zNear) {
				throw new IllegalArgumentException("farClippingPane " + zFar
						+ " is the same or less than " + "nearClippingPane "
						+ zNear);
			}
			return new GLViewPort(this);
		}
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseMoved(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean isEnabled() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isDeleted() {
		// TODO Auto-generated method stub
		return false;
	}

	/**
	 * @return the mouseSpeed
	 */
	public static float getMouseSpeed() {
		return mouseSpeed;
	}

	/**
	 * @param mouseSpeed
	 *            the mouseSpeed to set
	 */
	public static void setMouseSpeed(float mouseSpeed) {
		GLViewPort.mouseSpeed = mouseSpeed;
	}

	public static void setupGLDisplay() throws FileNotFoundException,
			IOException {
		/*
		 * Set Display Modes avabile. 0 - Small Screenwith border 1 - Full
		 * Screen with border 2 - Full Screen without border
		 */

		try {
			Game.SCREEN_WIDTH = Toolkit.getDefaultToolkit().getScreenSize().width;
			Game.SCREEN_HEIGHT = Toolkit.getDefaultToolkit().getScreenSize().height;
			if (Game.currentDisplayMode == 2) {
				System.setProperty("org.lwjgl.opengl.Window.undecorated",
						"true");
				Display.setDisplayMode(new DisplayMode(Game.SCREEN_WIDTH,
						Game.SCREEN_HEIGHT));

				Display.setFullscreen(true);
			} else if (Game.currentDisplayMode == 1) {
				Display.setDisplayMode(new DisplayMode(Game.SCREEN_WIDTH,
						Game.SCREEN_HEIGHT));
			} else {
				Display.setDisplayMode(new DisplayMode(640, 480));
				Game.SCREEN_WIDTH = 640;
				Game.SCREEN_HEIGHT = 480;
			}

			Display.setTitle("Coffea Economy Simulator");

			ByteBuffer[] icons = new ByteBuffer[1];
			icons[0] = getGameIcon("resources/icon_16.png", 16, 16);
			Display.setIcon(icons);

			Display.create();
		} catch (LWJGLException e) {
			e.printStackTrace();
			System.exit(0);
		}

		/*
		 * Set up 2D and 3D viewports!
		 */
		Display.setVSyncEnabled(Game.vSync);
		GL11.glGetFloat(GL11.GL_PROJECTION_MATRIX,
				Game.perspectiveProjectionMatrix);
		GL11.glMatrixMode(GL11.GL_PROJECTION);
		//GL11.glPolygonMode(GL11.GL_FRONT_AND_BACK, GL11.GL_LINE);
		GLU.gluPerspective(45.0f,
				(float) Display.getWidth() / Display.getHeight(), 0.01f, 100f);
		GL11.glEnable(GL11.GL_DEPTH_TEST);
		//GL11.glEnable(GL11.GL_CULL_FACE);
		GL11.glPolygonMode(GL11.GL_FRONT_AND_BACK, 6914);
		GL11.glCullFace(GL11.GL_BACK);
		GL11.glEnable(GL11.GL_LIGHTING);
		GL11.glEnable(GL11.GL_LIGHT0);
		GL11.glLoadIdentity();
		GL11.glOrtho(0, Display.getWidth(), Display.getHeight(), 0, 1, -1);

		GL11.glGetFloat(GL11.GL_PROJECTION_MATRIX,
				Game.orthographicProjectrionMatrix);
		GL11.glLoadMatrix(Game.perspectiveProjectionMatrix);
		GL11.glMatrixMode(GL11.GL_MODELVIEW_MATRIX);

		Game.fbo = GL30.glGenFramebuffers();
		GL30.glBindFramebuffer(GL30.GL_FRAMEBUFFER, Game.fbo);

		Game.rbo = GL30.glGenRenderbuffers();
		GL30.glBindRenderbuffer(GL30.GL_RENDERBUFFER, Game.rbo);
		GL30.glRenderbufferStorage(GL30.GL_RENDERBUFFER, GL11.GL_RGBA8, 640,
				480);
		GL30.glFramebufferRenderbuffer(GL30.GL_FRAMEBUFFER,
				GL30.GL_COLOR_ATTACHMENT0, GL30.GL_RENDERBUFFER, Game.rbo);

		assert (GL30.glCheckFramebufferStatus(GL30.GL_FRAMEBUFFER) == GL30.GL_FRAMEBUFFER_COMPLETE);

		GL30.glBindFramebuffer(GL30.GL_DRAW_FRAMEBUFFER, Game.fbo);
		GL20.glDrawBuffers(GL30.GL_COLOR_ATTACHMENT0);
		GL11.glClear(GL11.GL_COLOR_BUFFER_BIT);

	}

	public static ByteBuffer getGameIcon(String filename, int width, int height) {
		BufferedImage image = null;
		try {
			image = ImageIO.read(ResourceLoader
					.getResourceAsStream(filename));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} // load image

		// convert image to byte array
		byte[] imageBytes = new byte[width * height * 4];
		for (int i = 0; i < height; i++) {
			for (int j = 0; j < width; j++) {
				int pixel = image.getRGB(j, i);
				for (int k = 0; k < 3; k++)
					// red, green, blue
					imageBytes[(i * 16 + j) * 4 + k] = (byte) (((pixel >> (2 - k) * 8)) & 255);
				imageBytes[(i * 16 + j) * 4 + 3] = (byte) (((pixel >> (3) * 8)) & 255); // alpha
			}
		}
		return ByteBuffer.wrap(imageBytes);
	}

	@Override
	public boolean isHover() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean setHover(boolean state) {
		return false;
	}

	@Override
	public boolean inBounds(MouseEvent e) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void keyPressed(KeyEvent key) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyReleased(KeyEvent key) {
		// TODO Auto-generated method stub
		
	}
}