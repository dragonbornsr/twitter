/*
	Copyright (c) 2014, Coffea Games
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	1. Redistributions of source code must retain the above copyright
	   notice, this list of conditions and the following disclaimer.
	2. Redistributions in binary form must reproduce the above copyright
	   notice, this list of conditions and the following disclaimer in the
	   documentation and/or other materials provided with the distribution.
	3. All advertising materials mentioning features or use of this software
	   must display the following acknowledgement:
	   This product includes software developed by the Coffea Games.
	4. Neither the name of the Coffea Games nor the
	   names of its contributors may be used to endorse or promote products
	   derived from this software without specific prior written permission.
	
	THIS SOFTWARE IS PROVIDED BY COFFEA GAMES ''AS IS'' AND ANY
	EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
	WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL COFFEA GAMES BE LIABLE FOR ANY
	DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
	(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
	LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
	ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
	(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
package com.coffeagame.Engine.Variables;

import java.nio.FloatBuffer;

import org.lwjgl.BufferUtils;

/**
 * Class holding and managing special FloatBuffer for GL objects.
 * @author ROG
 *
 */
public class GLFloat {
	
	FloatBuffer buffer;
	
	
	public GLFloat() {
		this.buffer = asFloatBuffer(new float[]{0f, 0f, 0f, 1.0f});
	}
	
	public GLFloat(float arg1) {
		this.buffer = asFloatBuffer(new float[]{arg1});
	}
	
	public GLFloat(float arg1, float arg2) {
		this.buffer = asFloatBuffer(new float[]{arg1, arg2});
	}
	
	public GLFloat(float arg1, float arg2, float arg3) {
		this.buffer = asFloatBuffer(new float[]{arg1, arg2, arg3});
	}
	
	public GLFloat(float arg1, float arg2, float arg3, float arg4) {
		this.buffer = asFloatBuffer(new float[]{arg1, arg2, arg3, arg4});
	}
	
	public void setFloat(float red, float green, float blue, float alpha) {
		this.buffer = asFloatBuffer(new float[]{green, red, blue, alpha});
	}
	
	public GLFloat(float[] args) {
		this.buffer = asFloatBuffer(args);
	}
	
	/**
	 * Get FloatBuffer.
	 * @return
	 */
	public FloatBuffer getValue() {
		return this.buffer;
	}
	
	private FloatBuffer asFloatBuffer(float[] f) {
		FloatBuffer buffer = BufferUtils.createFloatBuffer(f.length);
		buffer.put(f);
		buffer.flip();
		return buffer;
	}
}
