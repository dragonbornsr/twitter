/*
	Copyright (c) 2014, Coffea Games
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	1. Redistributions of source code must retain the above copyright
	   notice, this list of conditions and the following disclaimer.
	2. Redistributions in binary form must reproduce the above copyright
	   notice, this list of conditions and the following disclaimer in the
	   documentation and/or other materials provided with the distribution.
	3. All advertising materials mentioning features or use of this software
	   must display the following acknowledgement:
	   This product includes software developed by the Coffea Games.
	4. Neither the name of the Coffea Games nor the
	   names of its contributors may be used to endorse or promote products
	   derived from this software without specific prior written permission.
	
	THIS SOFTWARE IS PROVIDED BY COFFEA GAMES ''AS IS'' AND ANY
	EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
	WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL COFFEA GAMES BE LIABLE FOR ANY
	DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
	(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
	LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
	ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
	(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
package com.coffeagame.Engine.core;

import com.coffeagame.Engine.Game;

/**
 * Core object. Do not worry about it :)
 * 
 * @since 740769d
 */
public class TickHandler {

	protected long now, lastTime = System.nanoTime();
	protected int frames = 0;
	protected double process = 0.0;
	protected long frameTime = System.currentTimeMillis();
	protected double NSperTick = 1000000000.0 / 60.0;
	public int tickSpeed = 60;
	protected int TPS = 60;

	public TickHandler() {
		lastTime = System.nanoTime();
	}

	/**
	 * set Maximum FPS or TPS (depends on context)
	 * 
	 * @param
	 */
	public void setMaxTPS(int tickSpeed) {
		this.tickSpeed = tickSpeed;
		TPS = tickSpeed;
		NSperTick = 1000000000.0 / tickSpeed;
	}

	/**
	 * 
	 * @return current FPS or TPS
	 */
	public int getLastTPS() {
		return TPS;
	}

	/**
	 * Delta is a speed constant. 60 frames per second will be constant 1. if
	 * FPS is 120, every movement per frame will be multiplied with 0.5 to
	 * achieve same object speed.
	 * 
	 * @return
	 */
	public float getDelta() {
		return (float) 60 / TPS;
	}

	public void handle(Game g) throws InterruptedException {
		now = System.nanoTime();
		process += (now - lastTime) / NSperTick;
		lastTime = now;
		boolean ticked = false;
		while (process >= 1) {
			process -= 1;
			ticked = true;
		}
		if (ticked) {
			g.updateLoop();
			g.drawLoop();
			frames++;
		} else {
			Thread.sleep(0, 100);

		}

		if (frameTime <= System.currentTimeMillis() - 1000) {
			TPS = frames;
			frameTime += 1000;
			frames = 0;
		}
	}
}
