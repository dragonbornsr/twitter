package com.coffeagame.Engine.event.types;

import java.awt.Toolkit;

import org.lwjgl.input.Keyboard;

public class KeyEvent {
	private int key;
	private int character;
	private char modifiedKey;
	
	//List all modifiers!!!
	private boolean shift;
	private boolean control;
	private boolean alt;
	private boolean function; //Win or MAC
	private boolean caps_lock;
	
	public KeyEvent(int key) {
		this(key, true);
	}
	public KeyEvent(int key, boolean getModifiers) {
		this.key = key;
		
		shift = Keyboard.isKeyDown(Keyboard.KEY_LSHIFT | Keyboard.KEY_RSHIFT);
		control = Keyboard.isKeyDown(Keyboard.KEY_RCONTROL | Keyboard.KEY_RCONTROL);
		alt = Keyboard.isKeyDown(Keyboard.KEY_LMENU| Keyboard.KEY_RMENU);
		function = Keyboard.isKeyDown(Keyboard.KEY_FUNCTION);
		//caps_lock = Toolkit.getDefaultToolkit().getLockingKeyState(Keyboard.KEY_CAPITAL);
		character = Keyboard.getEventCharacter();
		modifiedKey = Keyboard.getEventCharacter();
	}
	
	public int getKey() {
		return key;
	}
	
	public int getModifiedKey() {
		return modifiedKey;
	}
	
	public String getModifiedKeyAsString() {
		char[] c = {modifiedKey};
		return String.copyValueOf(c);
	}
	
	public String getKeyAsString() {
		return "NULL";
	}
}
