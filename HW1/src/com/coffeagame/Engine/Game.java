/*
	Copyright (c) 2014, Coffea Games
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	1. Redistributions of source code must retain the above copyright
	   notice, this list of conditions and the following disclaimer.
	2. Redistributions in binary form must reproduce the above copyright
	   notice, this list of conditions and the following disclaimer in the
	   documentation and/or other materials provided with the distribution.
	3. All advertising materials mentioning features or use of this software
	   must display the following acknowledgement:
	   This product includes software developed by the Coffea Games.
	4. Neither the name of the Coffea Games nor the
	   names of its contributors may be used to endorse or promote products
	   derived from this software without specific prior written permission.
	
	THIS SOFTWARE IS PROVIDED BY COFFEA GAMES ''AS IS'' AND ANY
	EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
	WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL COFFEA GAMES BE LIABLE FOR ANY
	DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
	(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
	LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
	ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
	(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

package com.coffeagame.Engine;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.util.ArrayList;
import java.util.List;

import org.lwjgl.opengl.Display;

import com.coffeagame.Engine.Objects.*;
import com.coffeagame.Engine.Utility.BufferTools;
import com.coffeagame.Engine.core.NativesLoader;
import com.coffeagame.Engine.core.TickHandler;
import com.coffeagame.Engine.event.GameEventController;
import com.coffeagame.Engine.event.KeyController;
import com.coffeagame.Engine.event.MouseController;
import com.coffeagame.Engine.scene.GLViewPort;

/**
 * 
 * @since dcaf132
 */
public class Game {

	public static TickHandler th = new TickHandler();

	public static List<DrawableGameObject> listOfDrawableGameObjects = new ArrayList<DrawableGameObject>();
	public static List<GameObject> listOfGameObjects = new ArrayList<GameObject>();

	public static List<DrawableGameObject> listOfDeletedDrawableGameObjects = new ArrayList<DrawableGameObject>();
	public static List<GameObject> listOfDeletedGameObjects = new ArrayList<GameObject>();

	public static List<CoreObject> listOfCoreObjects = new ArrayList<CoreObject>();

	public static int currentDisplayMode = 0;
	public static int SCREEN_WIDTH, SCREEN_HEIGHT;

	public static KeyController keyController = new KeyController();
	public static MouseController mouseController = new MouseController();
	public static GameEventController gameEventController = new GameEventController();

	public static ByteBuffer buffer = ByteBuffer.allocateDirect(64).order(
			ByteOrder.nativeOrder());
	public static FloatBuffer perspectiveProjectionMatrix = (BufferTools
			.reserveData(16));
	public static FloatBuffer orthographicProjectrionMatrix = (BufferTools
			.reserveData(16));

	public static GLViewPort playerViewPort;
	public static Cursor cursor;

	public static boolean vSync = true;
	public static int rbo;
	public static int fbo;
	
	/**
	 * Currently focused Drawable game object. used to look up for
	 * last selected GUI or other item.
	 */
	public static DrawableGameObject focused = null;

	public Game() {
		NativesLoader.load();
	}

	public void Run() throws InterruptedException {
		preInit();
		init();
		postInit();

		while (!Display.isCloseRequested()) {
			th.handle(this);
		}
		Display.destroy();
	}

	/**
	 * Please do not call or Override this function this is meant only for tick
	 * handler to call out!
	 */
	public void updateLoop() {
		for (DrawableGameObject o : listOfDeletedDrawableGameObjects) {
			listOfDrawableGameObjects.remove(o);
		}
		listOfDeletedDrawableGameObjects.clear();
		for (GameObject o : listOfDeletedGameObjects) {
			listOfDeletedGameObjects.remove(o);
		}
		listOfDeletedGameObjects.clear();

		for (CoreObject co : listOfCoreObjects) {
			co.update(th.getDelta());
		}
		for (GameObject go : listOfGameObjects) {
			if (go.enabled)
				go.update(th.getDelta());
		}
		for (DrawableGameObject dgo : listOfDrawableGameObjects) {
			if (dgo.enabled)
				dgo.update(th.getDelta());
		}
		/** Finally **/
		if (Display.isCloseRequested()) {
			exitGame();
		}
	}

	/**
	 * Method to call to exit the game. Will call ut finalize() function for all objects to
	 * allow safe saving.
	 */
	public static void exitGame() {
		for (DrawableGameObject dgo : listOfDrawableGameObjects) {
			dgo.finalize();
		}
		for (GameObject go : listOfGameObjects) {
			go.finalize();
		}
		Display.destroy();
		System.exit(0);
	}

	/**
	 * Please do not call or Override this function this is meant only for tick
	 * handler to call out!
	 */
	public void drawLoop() {
		for (DrawableGameObject dgo : listOfDrawableGameObjects) {
			if (dgo.enabled && dgo.visible)
				dgo.draw(th.getDelta());
		}
		/* Finnally */
		Display.update();
	}

	/**
	 * Used to set up Initial game parameters, before doing ANYTHIG with them.
	 * For example, set currentDisplayMode etc.
	 */
	protected void preInit() {
		
	}
	
	protected void init() {

	}

	protected void postInit() {
	}
	/**
	 * Start drawing in 2D perspective.
	 */
	public static void startGLMode2D() {
		GLViewPort.startGLMode2D();
	}
	/**
	 * Finish drawing in 2D perspective. Continues 
	 * the 3D perspective.
	 */
	public static void endGLMode2D() {
		GLViewPort.endGLMode2D();
	}
	
	public static boolean isFocused(DrawableGameObject o) {
		if (o == focused)
			return true;
		return false;
	}

}
