/*
	Copyright (c) 2014, Coffea Games
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	1. Redistributions of source code must retain the above copyright
	   notice, this list of conditions and the following disclaimer.
	2. Redistributions in binary form must reproduce the above copyright
	   notice, this list of conditions and the following disclaimer in the
	   documentation and/or other materials provided with the distribution.
	3. All advertising materials mentioning features or use of this software
	   must display the following acknowledgement:
	   This product includes software developed by the Coffea Games.
	4. Neither the name of the Coffea Games nor the
	   names of its contributors may be used to endorse or promote products
	   derived from this software without specific prior written permission.
	
	THIS SOFTWARE IS PROVIDED BY COFFEA GAMES ''AS IS'' AND ANY
	EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
	WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL COFFEA GAMES BE LIABLE FOR ANY
	DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
	(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
	LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
	ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
	(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
package com.coffeagame.Engine.gui;

import org.newdawn.slick.opengl.Texture;

import com.coffeagame.Engine.Objects.DrawableGameObject;
import com.coffeagame.Engine.event.interfaces.IMouseEvent;
import com.coffeagame.Engine.event.types.MouseEvent;
import com.coffeagame.Engine.media.ResourceManager;

/**
 * Tile is a button like object that has custom rendering instead on certain
 * background. May contain buttons and other types of gadgets too.
 * 
 * @author ROG
 * 
 */
public class GuiTile extends DrawableGameObject implements IMouseEvent {
	protected Gui parent;
	protected int posX = 0, posY = 0;
	protected int width = 0;
	protected int height = 0;
	protected int tileID = 0;
	protected Texture[] texture = null;
	protected int texOffX, texOffY;
	public boolean hover = false;

	/**
	 * 
	 * @param parent
	 * @param tileID
	 */
	public GuiTile() {
		
	}
	public GuiTile(Gui parent, int tileID) {
		this.parent = parent;
		parent.listOfGuiTiles.add(this);
		register(parent.priority + 1);
		this.tileID = tileID;
	}

	public GuiTile(Gui parent, int tileID, int posX, int posY, int width,
			int height, String[] textures) {
		this(parent, tileID);
		this.posX = posX;
		this.posY = posY;
		this.width = width;
		this.height = height;
		for (String s : textures) {
			texture[texture.length] = ResourceManager.getTexture(s);
		}
	}

	@Override
	public void update(float delta) {
		if (!this.parent.visible && this.visible) {
			this.visible = false;
		}
		if (this.parent.visible && !this.visible) {
			this.visible = true;
		}
	}

	/**
	 * Draw the tile. Remember, tiles are not rendered automatically. Every tile will have
	 * its own rendering.
	 */
	@Override
	public void draw(float delta) {
		
	}

	protected void setPosition(int x, int y) {
		this.posX = x;
		this.posY = y;
	}

	protected void setSize(int width, int height) {
		this.width = width;
		this.height = height;
	}

	/**
	 * Set Tile size and position relative to its parent GUI.
	 * @param x
	 * @param y
	 * @param width
	 * @param height
	 */
	public void setSizeAndPosition(int x, int y, int width, int height) {
		this.setPosition(x, y);
		this.setSize(width, height);
	}

	@Override
	public boolean isEnabled() {
		return parent.isVisible;
	}
	/**
	 * Returns if point on monitor is in bounds of the tile!
	 * @param e
	 * @return
	 */
	@Override
	public boolean inBounds(MouseEvent e) {
		if (parent == null)
			return false;
		if ((posX + parent.posX <= e.posX) && (posY + parent.posY <= e.posY)) // Left
																				// lower?
			if ((posX + parent.posX + width >= e.posX)
					&& (posY + height + parent.posY >= e.posY))
				return true;
		return false;
	}

	@Override
	public boolean isDeleted() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void mousePressed(MouseEvent e) {
		if (inBounds(e)) {
			if (parent == null)
				System.out.println("No parent found");
			parent.tilePressed(this.tileID);
		}
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseMoved(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean isHover() {
		return this.hover;
	}

	@Override
	public boolean setHover(boolean state) {
		this.hover = state;	
		return true;
	}
}
