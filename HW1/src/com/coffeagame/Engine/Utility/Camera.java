/*
	Copyright (c) 2014, Coffea Games
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	1. Redistributions of source code must retain the above copyright
	   notice, this list of conditions and the following disclaimer.
	2. Redistributions in binary form must reproduce the above copyright
	   notice, this list of conditions and the following disclaimer in the
	   documentation and/or other materials provided with the distribution.
	3. All advertising materials mentioning features or use of this software
	   must display the following acknowledgement:
	   This product includes software developed by the Coffea Games.
	4. Neither the name of the Coffea Games nor the
	   names of its contributors may be used to endorse or promote products
	   derived from this software without specific prior written permission.
	
	THIS SOFTWARE IS PROVIDED BY COFFEA GAMES ''AS IS'' AND ANY
	EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
	WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL COFFEA GAMES BE LIABLE FOR ANY
	DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
	(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
	LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
	ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
	(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
package com.coffeagame.Engine.Utility;

public interface Camera {

	/**
	 * Translate mouse movement into camera movement
	 */
	public void processMouse(float delta);

	/**
	 * Translate keyboard input into camera movement
	 */
	public void processKeyboard(float delta);

	/**
	 * Move in the direction you are looking.
	 * 
	 * @param dx
	 * @param dy
	 * @param dz
	 */
	public void moveFromLook(float dx, float dy, float dz);

	/**
	 * Sets the position of the camera.
	 * 
	 * @param x
	 * @param y
	 * @param z
	 */
	public void setPosition(float x, float y, float z);

	/**
	 * Sets GL_PROJECTION to an orthographic projection matrix. The matrix mode
	 * will be returned it its previous value after execution.
	 */
	public void applyOrthographicMatrix();

	/**
	 * Enables or disables OpenGL states that will enhance the camera
	 * appearance.
	 */
	public void applyOptimalStates();

	/**
	 * Sets GL_PROJECTION to an perspective projection matrix. The matrix mode
	 * will be returned it its previous value after execution.
	 */
	public void applyPerspectiveMatrix();

	/** Applies the camera translations and rotations to GL_MODELVIEW. */
	public void applyTranslations();

	/**
	 * Sets the rotation of the camera.
	 * 
	 * @param pitch
	 *            the rotation around the x-axis in degrees
	 * @param yaw
	 *            the rotation around the y-axis in degrees
	 * @param roll
	 *            the rotation around the z-axis in degrees
	 */
	public void setRotation(float pitch, float yaw, float roll);

	/** @return the x-coordinate of the camera */
	public float x();

	/** @return y the y-coordinate of the camera */
	public float y();

	/** @return the z-coordinate of the camera */
	public float z();

	/** @return the pitch of the camera in degrees */
	public float pitch();

	/** @return the yaw of the camera in degrees */
	public float yaw();

	/** @return the roll of the camera in degrees */
	public float roll();

	/** @return the fov of the camera in degrees in the y direction */
	public float fieldOfView();

	/**
	 * Sets the field of view angle in degrees in the y direction. Note that
	 * this.applyPerspectiveMatrix() must be applied in order to see any
	 * difference.
	 * 
	 * @param fov
	 *            the field of view angle in degrees in the y direction
	 */
	public void setFieldOfView(float fov);

	/**
	 * Sets the aspect ratio of the camera. Note that, to see any effect, you
	 * must call applyPerspectiveMatrix or applyOrthographicMatrix.
	 * 
	 * @param aspectRatio
	 *            the aspect ratio of the camera
	 * 
	 * @throws IllegalArgumentException
	 *             if aspectRatio is 0 or less
	 */
	public void setAspectRatio(float aspectRatio);

	/** @return the aspect ratio of the camera */
	public float aspectRatio();

	/** @return the distance from the camera to the near clipping pane */
	public float nearClippingPane();

	/** @return the distance from the camera to the far clipping pane */
	public float farClippingPane();
}