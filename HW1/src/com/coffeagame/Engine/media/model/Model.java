/*
	Copyright (c) 2014, Coffea Games
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	1. Redistributions of source code must retain the above copyright
	   notice, this list of conditions and the following disclaimer.
	2. Redistributions in binary form must reproduce the above copyright
	   notice, this list of conditions and the following disclaimer in the
	   documentation and/or other materials provided with the distribution.
	3. All advertising materials mentioning features or use of this software
	   must display the following acknowledgement:
	   This product includes software developed by the Coffea Games.
	4. Neither the name of the Coffea Games nor the
	   names of its contributors may be used to endorse or promote products
	   derived from this software without specific prior written permission.
	
	THIS SOFTWARE IS PROVIDED BY COFFEA GAMES ''AS IS'' AND ANY
	EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
	WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL COFFEA GAMES BE LIABLE FOR ANY
	DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
	(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
	LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
	ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
	(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
package com.coffeagame.Engine.media.model;

import java.util.ArrayList;
import java.util.List;

import org.lwjgl.opengl.GL11;
import org.lwjgl.util.vector.Vector3f;

/**
 * Model is a 3D object
 * @author ROG
 *
 */
public class Model {
	public List<Vector3f> vertices = new ArrayList<Vector3f>();
	public List<Vector3f> normals = new ArrayList<Vector3f>();
	public List<Face> faces = new ArrayList<Face>();

	public Vector3f rotation = new Vector3f(0, 0, 0);

	public Model() {

	}

	public void render(float x, float y, float z, float offX, float offY,
			float offZ, float scale) {
		for (Face face : this.faces) {
			Vector3f normal1 = this.normals.get((int) face.normal.x - 1);
			GL11.glNormal3f(x + (normal1.x + offX) * scale, y
					+ (normal1.y + offY) * scale, z + (normal1.z + offZ)
					* scale);
			Vector3f vertex1 = this.vertices.get((int) face.vertex.x - 1);
			GL11.glVertex3f(x + (vertex1.x + offX) * scale, y
					+ (vertex1.y + offY) * scale, z + (vertex1.z + offZ)
					* scale);

			Vector3f normal2 = this.normals.get((int) face.normal.y - 1);
			GL11.glNormal3f(x + (normal2.x + offX) * scale, y
					+ (normal2.y + offY) * scale, z + (normal2.z + offZ)
					* scale);
			Vector3f vertex2 = this.vertices.get((int) face.vertex.y - 1);
			GL11.glVertex3f(x + (vertex2.x + offX) * scale, y
					+ (vertex2.y + offY) * scale, z + (vertex2.z + offZ)
					* scale);

			Vector3f normal3 = this.normals.get((int) face.normal.z - 1);
			GL11.glNormal3f(x + (normal3.x + offX) * scale, y
					+ (normal3.y + offY) * scale, z + (normal3.z + offZ)
					* scale);
			Vector3f vertex3 = this.vertices.get((int) face.vertex.z - 1);
			GL11.glVertex3f(x + (vertex3.x + offX) * scale, y
					+ (vertex3.y + offY) * scale, z + (vertex3.z + offZ)
					* scale);
		}

	}
}
