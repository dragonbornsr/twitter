/*
	Copyright (c) 2014, Coffea Games
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	1. Redistributions of source code must retain the above copyright
	   notice, this list of conditions and the following disclaimer.
	2. Redistributions in binary form must reproduce the above copyright
	   notice, this list of conditions and the following disclaimer in the
	   documentation and/or other materials provided with the distribution.
	3. All advertising materials mentioning features or use of this software
	   must display the following acknowledgement:
	   This product includes software developed by the Coffea Games.
	4. Neither the name of the Coffea Games nor the
	   names of its contributors may be used to endorse or promote products
	   derived from this software without specific prior written permission.
	
	THIS SOFTWARE IS PROVIDED BY COFFEA GAMES ''AS IS'' AND ANY
	EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
	WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL COFFEA GAMES BE LIABLE FOR ANY
	DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
	(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
	LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
	ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
	(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
package com.coffeagame.Engine.media;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.newdawn.slick.opengl.Texture;
import org.newdawn.slick.opengl.TextureLoader;
import org.newdawn.slick.util.ResourceLoader;

import com.coffeagame.Engine.Objects.CoreObject;
import com.coffeagame.Engine.media.model.Model;
import com.coffeagame.Engine.media.model.ModelLoader;

import java.awt.Font;
import java.awt.FontFormatException;

public class ResourceManager extends CoreObject {

	protected static HashMap<String, Texture> TEXTURES = new HashMap<String, Texture>();
	protected static Texture defaultTexture;
	protected static HashMap<String, Font> FONTS = new HashMap<String, Font>();
	public static Font defaultFont;
	protected static HashMap<String, Model> MODELS = new HashMap<String, Model>();
	protected static Model defaultModel = null;
	public static String lastLoadingItem;

	protected List<String> listOfResources = new ArrayList<String>();
	public static boolean load = true;
	public boolean unload = false;
	protected int pointer = 0;

	/**
	 * Class to load external files to memory.
	 */
	public ResourceManager() {
		register();
		try {
			defaultTexture = TextureLoader
					.getTexture("PNG", ResourceLoader
							.getResourceAsStream("resources/default.png"));
			defaultFont = Font.createFont(Font.TRUETYPE_FONT, ResourceLoader
					.getResourceAsStream("resources/TimesNewRoman.ttf"));
		} catch (IOException e) {
			e.printStackTrace();
		} catch (FontFormatException e) {
			e.printStackTrace();
		}

		getResourceNames("resources");
	}

	@Override
	public void update(float delta) {
		if (load) {
			try {
				try {
					load(listOfResources.get(pointer));
				} catch (FontFormatException e) {
					e.printStackTrace();
				}
			} catch (IOException e) {
				e.printStackTrace();
				System.exit(0);
			}
			pointer++;
			if (pointer >= listOfResources.size())
				load = false;
		}

	}
	/**
	 * Load a resource
	 * @param resStr
	 * @throws IOException
	 * @throws FontFormatException
	 */
	protected void load(String resStr) throws IOException, FontFormatException {
		if (resStr.length() > 0) {
			String resIndexName = null;
			String resExtentionName = null;
			String[] fileparts = resStr.split("\\\\");
			resIndexName = fileparts[fileparts.length - 1];
			int pos = resIndexName.lastIndexOf(".");
			if (pos > 0) {
				resExtentionName = resIndexName.substring(pos + 1,
						resIndexName.length());
				resIndexName = resIndexName.substring(0, pos);
			}
			lastLoadingItem = resIndexName;
			if (resExtentionName.equals("png"))
				TEXTURES.put(
						resIndexName,
						TextureLoader.getTexture("PNG",
								ResourceLoader.getResourceAsStream(resStr)));
			else if (resExtentionName.equals("ttf"))
				FONTS.put(
						resIndexName,
						Font.createFont(Font.TRUETYPE_FONT,
								ResourceLoader.getResourceAsStream(resStr)));
			else if (resExtentionName.equals("obj"))
				MODELS.put(resIndexName,
						ModelLoader.loadModel(new File(resStr)));
		}
	}

	/**
	 * Get all resources recursively in the directory.
	 * @param sDir
	 */
	public void getResourceNames(String sDir) {
		File[] faFiles = new File(sDir).listFiles();
		for (File file : faFiles) {
			if (file.getName().matches("^(.*?)")) {
				if (!file.isDirectory())
					listOfResources.add(file.getAbsolutePath());
			}
			if (file.isDirectory()) {
				getResourceNames(file.getAbsolutePath());
			}
		}
	}

	/**
	 * Method to get a Texture from memory
	 * @param tex
	 * @return
	 */
	public static Texture getTexture(String tex) {
		if (TEXTURES.get(tex) != null) {
			return TEXTURES.get(tex);
		}
		return defaultTexture;
	}
	/**
	 * Method to get font from memory.
	 * @param f
	 * @return
	 */
	public static Font getFont(String f) {
		if (FONTS.get(f) != null) {
			return FONTS.get(f);
		}
		return defaultFont;
	}
	/**
	 * Method to get a model from memory
	 * @param f
	 * @return
	 */
	public static Model getModel(String f) {
		if (MODELS.get(f) != null) {
			return MODELS.get(f);
		}
		return defaultModel;
	}

	public static int getTexturesCount() {
		return TEXTURES.size();
	}

	public static int getFontsCount() {
		return FONTS.size();
	}

	public static int getModelsCount() {
		return MODELS.size();
	}
}
