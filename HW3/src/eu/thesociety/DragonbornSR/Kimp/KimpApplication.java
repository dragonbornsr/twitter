package eu.thesociety.DragonbornSR.Kimp;

import org.newdawn.slick.opengl.Texture;
import com.coffeagame.Engine.Game;
import com.coffeagame.Engine.media.ResourceManager;
import eu.thesociety.DragonbornSR.Kimp.Elements.GameCursor;
import eu.thesociety.DragonbornSR.Kimp.Elements.GameStateManager;
import eu.thesociety.DragonbornSR.Kimp.Elements.ViewPort;
import eu.thesociety.DragonbornSR.Kimp.controllers.KimpMouseController;

public class KimpApplication extends Game {

	/** resource manager. */
	public static ResourceManager resourceManager;
	/** Game cursor. */
	static GameCursor cursor;
	
	public static Texture defaultPNG;
	
	/*public static Color fontcolor = new Color(50,50,50);
	public static TrueTypeFont font;
	public static Font awtFont = null;*/
	
	@Override
	protected void preInit() {
		Game.SCREEN_WIDTH = 1280;
		Game.SCREEN_HEIGHT = 720;
		Game.MIN_SCREEN_HEIGHT = 100;
		super.init();
		th.setMaxTPS(60);
		
	}
	
	@Override
	protected void init() {
		super.init();
		/*Start Here*/
		listOfCoreObjects.add(new GameStateManager());
		playerViewPort =  new ViewPort();
		playerViewPort.setAspectRatio(Game.SCREEN_HEIGHT/Game.SCREEN_HEIGHT);
		listOfCoreObjects.add(keyController);
		mouseController = new KimpMouseController();
		listOfCoreObjects.add(mouseController);
		listOfCoreObjects.add(gameEventController);
		resourceManager = new ResourceManager();
	}
	
	@Override
	protected void postInit() {
		super.postInit();
		/*Start Here*/
	}
}