package eu.thesociety.DragonbornSR.Kimp.controllers;

import com.coffeagame.Engine.Objects.GameObject;

import paulscode.sound.SoundSystem;
import paulscode.sound.SoundSystemConfig;
import paulscode.sound.SoundSystemException;
import paulscode.sound.codecs.CodecJOgg;
import paulscode.sound.libraries.LibraryLWJGLOpenAL;

public class SoundController extends GameObject {

	public static SoundSystem soundSystem;

	public SoundController() {
		this.register(0);
		try {
			SoundSystemConfig.addLibrary(LibraryLWJGLOpenAL.class);
			SoundSystemConfig.setCodec("ogg", CodecJOgg.class);
		} catch (SoundSystemException e) {
			e.printStackTrace();
		}
		SoundSystemConfig
				.setSoundFilesPackage("resources/");
		soundSystem = new SoundSystem();
		soundSystem.backgroundMusic("bg", "theme.ogg", true);
		soundSystem.setMasterVolume(0.1f);
	}

	@Override
	public void update(float delta) {

	}

	public void playSound(String source, String soundName) {
		soundSystem.backgroundMusic(source, soundName, true);
	}

	public void stopSound(String source) {
		soundSystem.stop(source);
	}


	@Override
	public void finalize() {
		soundSystem.cleanup();
	}
}
