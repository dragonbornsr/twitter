package eu.thesociety.DragonbornSR.Kimp.controllers;

import java.util.ArrayList;

import org.newdawn.slick.Color;

import com.coffeagame.Engine.Objects.*;
import com.coffeagame.Engine.Variables.Point4i;
import com.coffeagame.Engine.event.interfaces.IKeyEvent;
import com.coffeagame.Engine.event.types.Event;
import com.coffeagame.Engine.event.types.EventTextBoxEdited;
import com.coffeagame.Engine.event.types.KeyEvent;

import eu.thesociety.DragonbornSR.Kimp.Components.*;
import eu.thesociety.DragonbornSR.Kimp.Elements.GameStateManager;
import eu.thesociety.DragonbornSR.Kimp.Elements.GameStateManager.GameState;
public class DrawController extends GameObject implements IKeyEvent {
	
	private static ArrayList<DrawableComponent> listOfDrawnObjects = new ArrayList<DrawableComponent>();
	public static Class<? extends DrawableComponent> currentType = DrawableFreehand.class;
	public static DrawableComponent currentObject = null;
	
	//Global settings
	public static float global_borderWidth = 10f;
	public static boolean isFilled = true;
	private static Point4i c = new Point4i(0,0,0,255);
	private static Point4i c2 = new Point4i(0,0,0,255);
	public static Color global_color = new Color(c.x, c.y, c.z, c.a);
	public static Color global_border_color = new Color(c2.x, c2.y, c2.z, c2.a);
	//End of Globals
	
	public DrawController() {
		this.register(0);
		add(currentType);
	}
	
	@Override
	public void update(float delta) {

	}

	@Override
	public void finalize() {
		
	}
	
	public static void add(Class<? extends DrawableComponent> c)  {
		if (GameStateManager.gameState == GameState.PAUSED)
			return;
		try {
			listOfDrawnObjects.add((DrawableComponent)c.newInstance());
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		currentObject = listOfDrawnObjects.get(listOfDrawnObjects.size()-1);
		currentObject.register( listOfDrawnObjects.indexOf(currentObject) + 0xFAA );
		currentObject.id = listOfDrawnObjects.indexOf(currentObject);
	}
	public static void undo() {
		if (GameStateManager.gameState == GameState.PAUSED)
			return;
		listOfDrawnObjects.get(listOfDrawnObjects.size()-1).enabled = false;
		listOfDrawnObjects.get(listOfDrawnObjects.size()-1).visible = false;
		for (int i = listOfDrawnObjects.size() - 1; i >= 0; i--) {
			if (!listOfDrawnObjects.get(i).isFinished) {

			}
			else if (listOfDrawnObjects.get(i).enabled) {
				listOfDrawnObjects.get(i).enabled = false;
				listOfDrawnObjects.get(i).visible = false;
				listOfDrawnObjects.get(i).isFinished = true;
				break;
			}
		}
		add(currentType);
	}
	
	public static void redo() {
		if (GameStateManager.gameState == GameState.PAUSED)
			return;
		boolean f = false;
		for (int i = listOfDrawnObjects.size() - 1; i >= 0; i--) {
			if (listOfDrawnObjects.get(i).isFinished) {
				if (listOfDrawnObjects.get(i).enabled == true && listOfDrawnObjects.get(i).visible == true) {
					for (int j = i; j <= listOfDrawnObjects.size() - 1; j++) {
						if (!listOfDrawnObjects.get(j).enabled && listOfDrawnObjects.get(j).isFinished) {
							listOfDrawnObjects.get(j).enabled = true;
							listOfDrawnObjects.get(j).visible = true;
							f = true;
							break;
						}
					}
					f = true;
					break;
				}
			}
		}
		if (!f)
			for (int j = 0; j <= listOfDrawnObjects.size() - 1; j++) {
				if (!listOfDrawnObjects.get(j).enabled && listOfDrawnObjects.get(j).isFinished) {
					listOfDrawnObjects.get(j).enabled = true;
					listOfDrawnObjects.get(j).visible = true;
					break;
				}
			}
	}
	
	public static boolean isCurrent(int i) {
		if ((listOfDrawnObjects.size() - 1)  == i) return true;
		return false;
	}
	
	public static void next() {
		add(currentType);
	}
	
	public static void set(Class<? extends DrawableComponent> c) {
		if (GameStateManager.gameState == GameState.PAUSED)
			return;
		listOfDrawnObjects.remove(listOfDrawnObjects.size() - 1).isFinished = true;
		currentType = c;
		add(c);
	}

	@Override
	public boolean isEnabled() {
		return true;
	}

	@Override
	public boolean isDeleted() {
		return false;
	}

	@Override
	public void keyPressed(KeyEvent key) {
		if (key.getModifiedKey() == 26)
			undo();
		if (key.getModifiedKey() == 24)
			redo();
	}

	@Override
	public void keyReleased(KeyEvent key) {
		
	}
	
	public static DrawableComponent getLast() {
		if (!listOfDrawnObjects.isEmpty())
			return listOfDrawnObjects.get(listOfDrawnObjects.size() - 1);
		return null;
	}

	public static void onGuiElementEvent(Event e) {
		if (e instanceof EventTextBoxEdited) {
			if (((EventTextBoxEdited)e).getTextBoxString().length() > 0)
				switch (((EventTextBoxEdited)e).getID()) {
				case 0:
					global_borderWidth = Integer.parseInt(((EventTextBoxEdited)e).getTextBoxString());
					break;
				case 1:
					c.x = Integer.parseInt(((EventTextBoxEdited)e).getTextBoxString());
					global_color = new Color(c.x, c.y, c.z, c.a);
					break;
				case 2:
					c.y = Integer.parseInt(((EventTextBoxEdited)e).getTextBoxString());
					global_color = new Color(c.x, c.y, c.z, c.a);
					break;
				case 3:
					c.z = Integer.parseInt(((EventTextBoxEdited)e).getTextBoxString());
					global_color = new Color(c.x, c.y, c.z, c.a);
					break;
				case 4:
					c.a = Integer.parseInt(((EventTextBoxEdited)e).getTextBoxString());
					global_color = new Color(c.x, c.y, c.z, c.a);
					break;
				case 5:
					c2.x = Integer.parseInt(((EventTextBoxEdited)e).getTextBoxString());
					global_border_color = new Color(c2.x, c2.y, c2.z, c2.a);
					break;
				case 6:
					c2.y = Integer.parseInt(((EventTextBoxEdited)e).getTextBoxString());
					global_border_color = new Color(c2.x, c2.y, c2.z, c2.a);
					break;
				case 7:
					c2.z = Integer.parseInt(((EventTextBoxEdited)e).getTextBoxString());
					global_border_color = new Color(c2.x, c2.y, c2.z, c2.a);
					break;
				case 8:
					c2.a = Integer.parseInt(((EventTextBoxEdited)e).getTextBoxString());
					global_border_color = new Color(c2.x, c2.y, c2.z, c2.a);
					break;
			}
		}
	}
}