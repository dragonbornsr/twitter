package eu.thesociety.DragonbornSR.Kimp.Components;

import org.lwjgl.opengl.GL11;
import org.newdawn.slick.Color;
import org.newdawn.slick.opengl.TextureImpl;

import com.coffeagame.Engine.Game;
import com.coffeagame.Engine.Variables.Point2i;
import com.coffeagame.Engine.event.MouseController;
import com.coffeagame.Engine.event.interfaces.IMouseEvent;
import com.coffeagame.Engine.event.types.MouseEvent;

import eu.thesociety.DragonbornSR.Kimp.Elements.GameStateManager;
import eu.thesociety.DragonbornSR.Kimp.Elements.GameStateManager.GameState;
import eu.thesociety.DragonbornSR.Kimp.controllers.DrawController;

public class DrawableLine extends DrawableComponent implements IMouseEvent {
	
	public Point2i p1, p2;
	int i = 0;
	Color color = Color.black;
	float borderWidth = 2f;

	@Override
	public void draw(float delta) {
		float s = (isFinished)?this.borderWidth:DrawController.global_borderWidth;
		Color c = (isFinished)?color:DrawController.global_color;
		if (p1 != null) {
			TextureImpl.bindNone();
			Game.startGLMode2D();
			GL11.glPushMatrix();
				c.bind();
				GL11.glLineWidth(s);
				GL11.glBegin(GL11.GL_LINE_STRIP);
				GL11.glVertex2f(p1.x, p1.y);
				GL11.glVertex2f(p2.x, p2.y);
				GL11.glEnd();
				TextureImpl.bindNone();
			GL11.glPopMatrix();
			TextureImpl.bindNone();
			Game.endGLMode2D();
			TextureImpl.bindNone();
		}
	}

	@Override
	public void onDisplayResize(int x, int y) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void update(float delta) {		
	}

	@Override
	public void finalize() {}
	
	@Override
	public void finish() {
		DrawController.next();
		this.isFinished = true;
		MouseController.listOfDeleted.add(this);
		this.borderWidth = DrawController.global_borderWidth;
		this.color = new Color(
				DrawController.global_color.getRed(),
				DrawController.global_color.getGreen(),
				DrawController.global_color.getBlue(),
				DrawController.global_color.getAlpha()
				);
	}

	@Override
	public boolean isEnabled() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isDeleted() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isHover() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean setHover(boolean state) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void mousePressed(MouseEvent e) {
		if (!this.isFinished && (GameStateManager.gameState == GameState.PLAYING))
			if (p1 == null) {
				p1 = new Point2i(e.posX, e.posY);
				p2 = new Point2i(e.posX, e.posY);
			}
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		if (!this.isFinished && (GameStateManager.gameState == GameState.PLAYING))
			p2 = new Point2i(e.posX, e.posY);
		else {
			this.finish();
			DrawController.next();
			((IMouseEvent)DrawController.getLast()).mousePressed(e);
		}
		this.finish();
	}

	@Override
	public void mouseMoved(MouseEvent e) {
		if (!this.isFinished && DrawController.isCurrent(id) && (GameStateManager.gameState == GameState.PLAYING)) {
			p2 = new Point2i(e.posX, e.posY);
		}
	}

	@Override
	public boolean inBounds(MouseEvent e) {
		return super.inBounds(e);
	}
}
