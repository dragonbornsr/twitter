package eu.thesociety.DragonbornSR.Kimp.Components;

import org.lwjgl.opengl.GL11;
import org.newdawn.slick.Color;
import org.newdawn.slick.opengl.TextureImpl;

import com.coffeagame.Engine.Game;
import com.coffeagame.Engine.Variables.Point2i;
import com.coffeagame.Engine.event.MouseController;
import com.coffeagame.Engine.event.interfaces.IMouseEvent;
import com.coffeagame.Engine.event.types.MouseEvent;

import eu.thesociety.DragonbornSR.Kimp.Elements.GameStateManager;
import eu.thesociety.DragonbornSR.Kimp.Elements.GameStateManager.GameState;
import eu.thesociety.DragonbornSR.Kimp.controllers.DrawController;

public class DrawableRectangle extends DrawableComponent implements IMouseEvent {
	
	private Point2i ul = null;
	private Point2i lr = null;
	Point2i t1 = null;
	Point2i t2 = null;
	private boolean started = false;
	
	private float borderWidth = DrawController.global_borderWidth;
	private boolean isFilled = DrawController.isFilled;
	private Color color = Color.black;
	private Color borderColor = Color.black;
	private float hbw = DrawController.global_borderWidth/2;
	@Override
	public void draw(float delta) {
		if ((ul != null) && (lr != null)) {
			if ((isFinished)?this.isFilled:DrawController.isFilled) {
				TextureImpl.bindNone();
				Game.startGLMode2D();
				GL11.glPushMatrix();
				Color c = (isFinished)?this.color:DrawController.global_border_color;
				c.bind();
				GL11.glBegin(GL11.GL_QUADS);
				{
					GL11.glVertex2f(ul.x, ul.y);
					GL11.glVertex2f(lr.x, ul.y);
					GL11.glVertex2f(lr.x, lr.y);
					GL11.glVertex2f(ul.x, lr.y);
				}
				GL11.glEnd();
				GL11.glPopMatrix();
			}
			//Draw border
			float b = (isFinished)?this.borderWidth:DrawController.global_borderWidth;
			if (b > 0) {
				Color c = (isFinished)?this.borderColor:DrawController.global_color;
				c.bind();
				GL11.glLineWidth(b);
				GL11.glBegin(GL11.GL_LINES);
				{
					GL11.glVertex2f(t1.x - 2*hbw, t1.y  - hbw);
					GL11.glVertex2f(t2.x + 2*hbw, t1.y - hbw);
					GL11.glVertex2f(t2.x + hbw, t1.y);
					GL11.glVertex2f(t2.x + hbw, t2.y + 2*hbw);
					GL11.glVertex2f(t2.x, t2.y + hbw);
					GL11.glVertex2f(t1.x - 2*hbw, t2.y +hbw);
					GL11.glVertex2f(t1.x - hbw, t2.y);
					GL11.glVertex2f(t1.x - hbw, t1.y);
				}
				GL11.glEnd();
				TextureImpl.bindNone();
				GL11.glPopMatrix();
				Game.endGLMode2D();
			}
		}
	}

	@Override
	public void onDisplayResize(int x, int y) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void update(float delta) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void finalize() {

	}
	
	@Override
	public void finish() {
		DrawController.next();
		this.isFinished = true;
		MouseController.listOfDeleted.add(this);
		color = DrawController.global_border_color;
		borderColor = DrawController.global_color;
		hbw = DrawController.global_borderWidth/2;
		//if ()
	}

	@Override
	public boolean isEnabled() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isDeleted() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isHover() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean setHover(boolean state) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void mousePressed(MouseEvent e) {
		if (!isFinished && (GameStateManager.gameState == GameState.PLAYING)) {
			this.borderWidth = DrawController.global_borderWidth;
			ul = new Point2i(e.posX, e.posY);
			lr = new Point2i(e.posX, e.posY);
			arrange();
		}
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		if (started  && (GameStateManager.gameState == GameState.PLAYING)) {
			lr = new Point2i(e.posX, e.posY);
			arrange();
		}
		this.finish();
	}

	@Override
	public void mouseMoved(MouseEvent e) {
		if (!isFinished  && (GameStateManager.gameState == GameState.PLAYING)) {
			lr = new Point2i(e.posX, e.posY);
			arrange();
		}
	}

	@Override
	public boolean inBounds(MouseEvent e) {
		return true;
	}
	
	private void arrange() {
		if (ul != null) {
			t1 = new Point2i(cmp(ul.x,lr.x).y,cmp(ul.y,lr.y).y);
			t2 = new Point2i(cmp(ul.x,lr.x).x,cmp(ul.y,lr.y).x);
		}
	}
	
	private Point2i cmp(int a, int b) {
		if (a >= b)
			return new Point2i(a,b);
		return new Point2i(b,a);
	}
}
