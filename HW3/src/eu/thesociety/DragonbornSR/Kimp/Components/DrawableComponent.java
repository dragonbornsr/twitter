package eu.thesociety.DragonbornSR.Kimp.Components;

import com.coffeagame.Engine.Objects.DrawableGameObject;
import com.coffeagame.Engine.event.types.MouseEvent;

import eu.thesociety.DragonbornSR.Kimp.controllers.DrawController;

public abstract class DrawableComponent extends DrawableGameObject {
	
	public boolean isFinished = false;
	public int id = 0;

	@Override
	public abstract void draw(float delta);

	@Override
	public abstract void onDisplayResize(int x, int y);

	@Override
	public abstract void update(float delta);

	@Override
	public abstract void finalize();
	
	public abstract void finish();
	
	public boolean inBounds(MouseEvent e) {
		if (e.posX > 65 && e.posY > 45)
			return true;
		return false;
	}
	
	public boolean inSelectableArea() {
		return (false);
	}
	
	public boolean isLast() {
		if (DrawController.isCurrent(id))
			return true;
		return false;
	}
	
}
