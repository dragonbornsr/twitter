package eu.thesociety.DragonbornSR.Kimp.Components;

import java.util.ArrayList;

import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.GL11;
import org.newdawn.slick.Color;
import org.newdawn.slick.opengl.TextureImpl;

import com.coffeagame.Engine.Game;
import com.coffeagame.Engine.Variables.Point2i;
import com.coffeagame.Engine.event.MouseController;
import com.coffeagame.Engine.event.interfaces.IMouseEvent;
import com.coffeagame.Engine.event.types.MouseEvent;

import eu.thesociety.DragonbornSR.Kimp.Elements.GameStateManager;
import eu.thesociety.DragonbornSR.Kimp.Elements.GameStateManager.GameState;
import eu.thesociety.DragonbornSR.Kimp.controllers.DrawController;

public class DrawableFreehand extends DrawableComponent implements IMouseEvent {
	
	public ArrayList<Point2i> path = new ArrayList<Point2i>();
	public Point2i point;
	int i = 0;
	Color color = Color.black;
	float borderWidth = 2f;

	@Override
	public void draw(float delta) {
		float s = (isFinished)?this.borderWidth:DrawController.global_borderWidth;
		Color c = (isFinished)?color:DrawController.global_color;
		if (c.a == 1) {
			TextureImpl.bindNone();
			Game.startGLMode2D();
			GL11.glPushMatrix();
				c.bind();
				GL11.glPointSize(s);
				GL11.glBegin(GL11.GL_POINTS);
				for (Point2i p : path)
					GL11.glVertex2f(p.x, p.y);
				GL11.glEnd();
				TextureImpl.bindNone();
			GL11.glPopMatrix();
			Game.endGLMode2D();
		}
		if (path.size() > 1) {
			TextureImpl.bindNone();
			Game.startGLMode2D();
			GL11.glPushMatrix();
				c.bind();
				GL11.glLineWidth(s);
				GL11.glBegin(GL11.GL_LINE_STRIP);
				for (Point2i p : path)
					GL11.glVertex2f(p.x, p.y);
				GL11.glEnd();
				TextureImpl.bindNone();
			GL11.glPopMatrix();
			TextureImpl.bindNone();
			Game.endGLMode2D();
			TextureImpl.bindNone();
		}
	}

	@Override
	public void onDisplayResize(int x, int y) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void update(float delta) {		
	}

	@Override
	public void finalize() {}
	
	@Override
	public void finish() {
		DrawController.next();
		this.isFinished = true;
		MouseController.listOfDeleted.add(this);
		this.borderWidth = DrawController.global_borderWidth;
		this.color = new Color(
				DrawController.global_color.getRed(),
				DrawController.global_color.getGreen(),
				DrawController.global_color.getBlue(),
				DrawController.global_color.getAlpha()
				);
	}

	@Override
	public boolean isEnabled() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isDeleted() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isHover() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean setHover(boolean state) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void mousePressed(MouseEvent e) {
		if (this.path.isEmpty() && (GameStateManager.gameState == GameState.PLAYING)) {
			if (!this.isFinished && DrawController.isCurrent(id))
				path.add(new Point2i(e.posX, e.posY));
		}
		
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		if (!this.isFinished && (GameStateManager.gameState == GameState.PLAYING))
			this.finish();
	}

	@Override
	public void mouseMoved(MouseEvent e) {
		if (!this.isFinished && DrawController.isCurrent(id) && (GameStateManager.gameState == GameState.PLAYING)) {
			if (Mouse.isButtonDown(0)) {
				int last = (path.size() - 1);
				if (path.isEmpty())
					path.add(new Point2i(e.posX, e.posY));
				else if (e.posX != path.get(last).x || e.posY != path.get(last).y) {
					path.add(new Point2i(e.posX, e.posY));
				}
			}
		}
	}

	@Override
	public boolean inBounds(MouseEvent e) {
		return super.inBounds(e);
	}
}
