package eu.thesociety.DragonbornSR.Kimp.Gui;

import java.awt.Font;

import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.GL11;
import org.lwjgl.util.vector.Vector2f;
import org.newdawn.slick.Color;
import org.newdawn.slick.TrueTypeFont;
import org.newdawn.slick.opengl.Texture;
import org.newdawn.slick.opengl.TextureImpl;

import com.coffeagame.Engine.Game;
import com.coffeagame.Engine.Utility.TextureTools;
import com.coffeagame.Engine.event.interfaces.IGuiElementEvent;
import com.coffeagame.Engine.event.interfaces.IKeyEvent;
import com.coffeagame.Engine.event.types.Event;
import com.coffeagame.Engine.event.types.KeyEvent;
import com.coffeagame.Engine.gui.Gui;
import com.coffeagame.Engine.gui.GuiButton;
import com.coffeagame.Engine.gui.GuiScroller;
import com.coffeagame.Engine.gui.GuiTextBox;
import com.coffeagame.Engine.media.ResourceManager;

import eu.thesociety.DragonbornSR.Kimp.controllers.DrawController;

public class SideBarTop extends Gui implements IKeyEvent, IGuiElementEvent {
	
	public static Color fontcolor = new Color(0,0,0);
	public static TrueTypeFont font;
	public static Font awtFont = null;
	
	Texture bg;
	Vector2f c1,c2,c3,c4;
	public static GuiTextBox lineWidth;
	public static GuiTextBox red, red2;
	public static GuiTextBox green, green2;
	public static GuiTextBox blue, blue2;
	public static GuiTextBox alpha, alpha2;
	
	public SideBarTop() {
		this.register(Integer.MAX_VALUE - 10);
		
		awtFont = ResourceManager.getFont("DroidSans");
		awtFont = awtFont.deriveFont(Font.PLAIN, 20.0f);
		font = new TrueTypeFont(awtFont, true);
		
		height = 50;
		bg = ResourceManager.getTexture("tsb");
		c1 = TextureTools.getTextureCorner(bg,0,0);
		c2 = TextureTools.getTextureCorner(bg,Game.SCREEN_WIDTH,0);
		c3 = TextureTools.getTextureCorner(bg,Game.SCREEN_WIDTH,height);
		c4 = TextureTools.getTextureCorner(bg,0,height);
		
		lineWidth = new GuiTextBox(this, 0);
		lineWidth.setBackground(ResourceManager.getTexture("tsbfb"))
				.setDefaultString("10")
				.setPositionAndSize(50, 5, 30, 20)
				.setFontSize(18f)
				.setRegex("[0-9]")
				.setMaxTextLenght(2, true);
		
		red = new GuiTextBox(this, 1);
		red.setBackground(ResourceManager.getTexture("tsbfb"))
				.setDefaultString("0")
				.setPositionAndSize(100, 5, 40, 20)
				.setFontSize(18f)
				.setRegex("[0-9]")
				.setMaxTextLenght(3, true);
		
		green = new GuiTextBox(this, 2);
		green.setBackground(ResourceManager.getTexture("tsbfb"))
				.setDefaultString("0")
				.setPositionAndSize(140, 5, 40, 20)
				.setFontSize(18f)
				.setRegex("[0-9]")
				.setMaxTextLenght(3, true);
		
		blue = new GuiTextBox(this, 3);
		blue.setBackground(ResourceManager.getTexture("tsbfb"))
				.setDefaultString("0")
				.setPositionAndSize(180, 5, 40, 20)
				.setFontSize(18f)
				.setRegex("[0-9]")
				.setMaxTextLenght(3, true);
		
		alpha = new GuiTextBox(this, 4);
		alpha.setBackground(ResourceManager.getTexture("tsbfb"))
				.setDefaultString("255")
				.setPositionAndSize(220, 5, 40, 20)
				.setFontSize(18f)
				.setRegex("[0-9]")
				.setMaxTextLenght(3, true);
		
		red2 = new GuiTextBox(this, 5);
		red2.setBackground(ResourceManager.getTexture("tsbfb"))
				.setDefaultString("0")
				.setPositionAndSize(100, 25, 40, 20)
				.setFontSize(18f)
				.setRegex("[0-9]")
				.setMaxTextLenght(3, true);
		
		green2 = new GuiTextBox(this, 6);
		green2.setBackground(ResourceManager.getTexture("tsbfb"))
				.setDefaultString("0")
				.setPositionAndSize(140, 25, 40, 20)
				.setFontSize(18f)
				.setRegex("[0-9]")
				.setMaxTextLenght(3, true);
		
		blue2 = new GuiTextBox(this, 7);
		blue2.setBackground(ResourceManager.getTexture("tsbfb"))
				.setDefaultString("0")
				.setRegex("[0-9]")
				.setMaxTextLenght(3, true);
		
		alpha2 = new GuiTextBox(this, 8);
		alpha2.setBackground(ResourceManager.getTexture("tsbfb"))
				.setDefaultString("255")
				.setPositionAndSize(220, 25, 40, 20)
				.setFontSize(18f)
				.setRegex("[0-9]")
				.setMaxTextLenght(3, true);
		
		GuiScroller b = new GuiScroller(this, 0)
				.setTexture(ResourceManager.getTexture("KimpButtons"))
				.setbuttonBackground(ResourceManager.getTexture("asd4"))
				.setSizeAndPosition(35, 10, 25, 25, 0, 25);
	}
	
	
	
	@Override
	public void buttonPressed(int buttonID) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void tilePressed(int tileID) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void draw(float delta) {
		Color.white.bind();
		Game.startGLMode2D();
		GL11.glPushMatrix();
			TextureImpl.bindNone();
			GL11.glBlendFunc(GL11.GL_SRC_ALPHA, 
					GL11.GL_ONE_MINUS_SRC_ALPHA);
			GL11.glDisable(GL11.GL_BLEND);
			bg.bind();
			GL11.glBegin(GL11.GL_QUADS);
				GL11.glTexCoord2f(0,0);
				GL11.glVertex2f(0, 0);
				GL11.glTexCoord2f(c2.x,0);
				GL11.glVertex2f(Game.SCREEN_WIDTH, 0);
				GL11.glTexCoord2f(c3.x,c3.y);
				GL11.glVertex2f(Game.SCREEN_WIDTH, height);
				GL11.glTexCoord2f(0,c4.y);
				GL11.glVertex2f(0, height);
			GL11.glEnd();
		GL11.glPopMatrix();
		//Draw some cool text now :D
		
		GL11.glPushMatrix();
		GL11.glEnable(GL11.GL_BLEND);
		Color.white.bind();
		font.drawString(this.posX ,this.posY - 40, 
				"Test String adkljadsjsadjsjksdsd" ,fontcolor);
		GL11.glPopMatrix();
		
		// Draw Border line color indicator
		GL11.glPushMatrix();
		TextureImpl.bindNone();
		GL11.glBlendFunc(GL11.GL_SRC_ALPHA, 
				GL11.GL_ONE_MINUS_SRC_ALPHA);
		GL11.glDisable(GL11.GL_BLEND);
		DrawController.global_border_color.bind();
		GL11.glBegin(GL11.GL_QUADS);
			GL11.glTexCoord2f(0,0);
			GL11.glVertex2f(260, 25);
			GL11.glTexCoord2f(1,0);
			GL11.glVertex2f(280, 25);
			GL11.glTexCoord2f(1,1);
			GL11.glVertex2f(280, 45);
			GL11.glTexCoord2f(0,1);
			GL11.glVertex2f(260, 45);
		GL11.glEnd();
		GL11.glPopMatrix();
		
		// Draw Border line color indicator
		GL11.glPushMatrix();
		TextureImpl.bindNone();
		GL11.glBlendFunc(GL11.GL_SRC_ALPHA, 
				GL11.GL_ONE_MINUS_SRC_ALPHA);
		GL11.glDisable(GL11.GL_BLEND);
		DrawController.global_color.bind();
		GL11.glBegin(GL11.GL_QUADS);
			GL11.glTexCoord2f(0,0);
			GL11.glVertex2f(260, 5);
			GL11.glTexCoord2f(1,0);
			GL11.glVertex2f(280, 5);
			GL11.glTexCoord2f(1,1);
			GL11.glVertex2f(280, 25);
			GL11.glTexCoord2f(0,1);
			GL11.glVertex2f(260, 25);
		GL11.glEnd();
		GL11.glPopMatrix();
		Game.endGLMode2D();
		
	}

	@Override
	public void update(float delta) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onDisplayResize(int x, int y) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void finalize() {
		// TODO Auto-generated method stub
		
	}



	@Override
	public boolean isEnabled() {
		// TODO Auto-generated method stub
		return false;
	}



	@Override
	public boolean isDeleted() {
		// TODO Auto-generated method stub
		return false;
	}



	@Override
	public void keyPressed(KeyEvent key) {

	}



	@Override
	public void keyReleased(KeyEvent key) {
		// TODO Auto-generated method stub
		
	}



	@Override
	public void onGuiElementEvent(Event e) {
		DrawController.onGuiElementEvent(e);
	}



	@Override
	public void scrollerMoved(int scrollerID, GuiScroller scroller) {
		// TODO Auto-generated method stub
		
	}

}
