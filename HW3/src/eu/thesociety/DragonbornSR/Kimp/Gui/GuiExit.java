package eu.thesociety.DragonbornSR.Kimp.Gui;

import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.GL11;
import org.lwjgl.util.vector.Vector2f;
import org.newdawn.slick.Color;
import org.newdawn.slick.opengl.Texture;
import org.newdawn.slick.opengl.TextureImpl;

import com.coffeagame.Engine.Game;
import com.coffeagame.Engine.Utility.TextureTools;
import com.coffeagame.Engine.Variables.Point2i;
import com.coffeagame.Engine.event.interfaces.IKeyEvent;
import com.coffeagame.Engine.event.types.KeyEvent;
import com.coffeagame.Engine.gui.Gui;
import com.coffeagame.Engine.gui.GuiButton;
import com.coffeagame.Engine.gui.GuiScroller;
import com.coffeagame.Engine.media.ResourceManager;

import eu.thesociety.DragonbornSR.Kimp.Elements.GameStateManager;
import eu.thesociety.DragonbornSR.Kimp.Elements.GameStateManager.GameState;

public class GuiExit extends Gui implements IKeyEvent{
	
	private Texture bg;
	private Point2i g1;
	private Point2i g2;
	Vector2f c1,c2,c3,c4;
	
	public GuiExit() {
		this.isVisible = false;
		this.visible = false;
		width = 300;
		height = 100;
		g1 = new Point2i(Game.SCREEN_WIDTH/2 - width/2,Game.SCREEN_HEIGHT/2 - height/2);
		g2 = new Point2i(Game.SCREEN_WIDTH/2 + width/2,Game.SCREEN_HEIGHT/2 + height/2);
		this.posX = g1.x;
		this.posY = g1.y;
		bg = ResourceManager.getTexture("exitGui");
		this.register(Integer.MAX_VALUE - 3);
		updateTextureCorners();
		

		GuiButton b = new GuiButton(this, 0);
		b.setTexture(ResourceManager.getTexture("KimpButtons"));
		b.setSizeAndPosition(20, 50, 125, 45, 0, 75);
		
		GuiButton c = new GuiButton(this, 1);
		c.setTexture(ResourceManager.getTexture("KimpButtons"));
		c.setSizeAndPosition(155, 50, 125, 45, 0, 120);
	}

	@Override
	public void buttonPressed(int buttonID) {
		if (buttonID == 0) {
			Game.exitGame();
		} else {
			this.isVisible = false;
			this.visible = false;
			GameStateManager.gameState = GameState.PLAYING;
		}
	}

	@Override
	public void tilePressed(int tileID) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void draw(float delta) {
		Color.white.bind();
		Game.startGLMode2D();
		GL11.glPushMatrix();
			TextureImpl.bindNone();
			GL11.glBlendFunc(GL11.GL_SRC_ALPHA, 
					GL11.GL_ONE_MINUS_SRC_ALPHA);
			GL11.glDisable(GL11.GL_BLEND);
			bg.bind();
			GL11.glBegin(GL11.GL_QUADS);
				GL11.glTexCoord2f(c1.x,c1.y);
				GL11.glVertex2f(g1.x, g1.y);
				GL11.glTexCoord2f(c2.x,c2.y);
				GL11.glVertex2f(g2.x, g1.y);
				GL11.glTexCoord2f(c3.x,c3.y);
				GL11.glVertex2f(g2.x,g2.y);
				GL11.glTexCoord2f(c4.x,c4.y);
				GL11.glVertex2f(g1.x, g2.y);
			GL11.glEnd();
		GL11.glPopMatrix();		
		Game.endGLMode2D();
	}

	@Override
	public void update(float delta) {
		
	}

	@Override
	public void onDisplayResize(int x, int y) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void finalize() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean isEnabled() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isDeleted() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void keyPressed(KeyEvent key) {
		if (key.getKey() == Keyboard.KEY_ESCAPE) {
			GameStateManager.gameState = (GameStateManager.gameState == GameState.PLAYING)?
					GameState.PAUSED:GameState.PLAYING;
			if (this.isVisible == false) {
				this.isVisible = true;
				this.visible = true;
			} else {
				this.isVisible = false;
				this.visible = false;
			}
			
		}
	}

	@Override
	public void keyReleased(KeyEvent key) {
		// TODO Auto-generated method stub
		
	}
	
	public void updateTextureCorners() {
		c1 = TextureTools.getTextureCorner(this.bg, 0, 0);
		c2 = TextureTools.getTextureCorner(this.bg, width ,0);
		c3 = TextureTools.getTextureCorner(this.bg, width, height);
		c4 = TextureTools.getTextureCorner(this.bg, 0,height);
	}

	@Override
	public void scrollerMoved(int scrollerID, GuiScroller scroller) {
		// TODO Auto-generated method stub
		
	}

}
