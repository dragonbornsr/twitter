package eu.thesociety.DragonbornSR.Kimp.Gui;

import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.GL11;
import org.lwjgl.util.vector.Vector2f;
import org.newdawn.slick.Color;
import org.newdawn.slick.opengl.Texture;
import org.newdawn.slick.opengl.TextureImpl;

import com.coffeagame.Engine.Game;
import com.coffeagame.Engine.Utility.TextureTools;
import com.coffeagame.Engine.gui.Gui;
import com.coffeagame.Engine.gui.GuiScroller;
import com.coffeagame.Engine.media.ResourceManager;

public class GuiBackground extends Gui {
	
	Texture bg, logo;
	Vector2f c1,c2,c3,c4;
	
	public GuiBackground() {
		this.register(0);
		bg = ResourceManager.getTexture("transparent");
		logo = ResourceManager.getTexture("logo");
		c1 = TextureTools.getTextureCorner(bg,0,0);
		c2 = TextureTools.getTextureCorner(bg,Game.SCREEN_WIDTH,0);
		c3 = TextureTools.getTextureCorner(bg,Game.SCREEN_WIDTH,Game.SCREEN_HEIGHT);
		c4 = TextureTools.getTextureCorner(bg,0,Game.SCREEN_HEIGHT);
		this.height = Display.getHeight();
		this.width = Display.getWidth();
	}
	
	@Override
	public void buttonPressed(int buttonID) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void tilePressed(int tileID) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void draw(float delta) {
		Color.white.bind();
		Game.startGLMode2D();
		GL11.glPushMatrix();
			TextureImpl.bindNone();
			GL11.glBlendFunc(GL11.GL_SRC_ALPHA, 
					GL11.GL_ONE_MINUS_SRC_ALPHA);
			GL11.glDisable(GL11.GL_BLEND);
			bg.bind();
			GL11.glBegin(GL11.GL_QUADS);
				GL11.glTexCoord2f(0,0);
				GL11.glVertex2f(0, 0);
				GL11.glTexCoord2f(c2.x,0);
				GL11.glVertex2f(Game.SCREEN_WIDTH, 0);
				GL11.glTexCoord2f(c3.x,c3.y);
				GL11.glVertex2f(Game.SCREEN_WIDTH, Game.SCREEN_HEIGHT);
				GL11.glTexCoord2f(0,c4.y);
				GL11.glVertex2f(0, Game.SCREEN_HEIGHT);
			GL11.glEnd();
		GL11.glPopMatrix();
		Game.endGLMode2D();
	}

	@Override
	public void update(float delta) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onDisplayResize(int x, int y) {
		c1 = TextureTools.getTextureCorner(bg,0,0);
		c2 = TextureTools.getTextureCorner(bg,x,0);
		c3 = TextureTools.getTextureCorner(bg,x,y);
		c4 = TextureTools.getTextureCorner(bg,0,y);
		System.out.println();
		System.out.println(x + " " + y);
		System.out.println(Game.SCREEN_WIDTH + " " + Game.SCREEN_HEIGHT);
		
		
	}

	@Override
	public void finalize() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void scrollerMoved(int scrollerID, GuiScroller scroller) {
		// TODO Auto-generated method stub
		
	}

}
