package eu.thesociety.DragonbornSR.Kimp.Gui;

import org.lwjgl.opengl.GL11;
import org.lwjgl.util.vector.Vector2f;
import org.newdawn.slick.Color;
import org.newdawn.slick.opengl.Texture;
import org.newdawn.slick.opengl.TextureImpl;

import com.coffeagame.Engine.Game;
import com.coffeagame.Engine.Utility.TextureTools;
import com.coffeagame.Engine.gui.Gui;
import com.coffeagame.Engine.gui.GuiButton;
import com.coffeagame.Engine.gui.GuiScroller;
import com.coffeagame.Engine.media.ResourceManager;

import eu.thesociety.DragonbornSR.Kimp.Components.DrawableFreehand;
import eu.thesociety.DragonbornSR.Kimp.Components.DrawableLine;
import eu.thesociety.DragonbornSR.Kimp.Components.DrawableRectangle;
import eu.thesociety.DragonbornSR.Kimp.controllers.DrawController;

public class SideBarLeft extends Gui {
	
	Texture bg;
	Vector2f c1,c2,c3,c4;
	
	public SideBarLeft() {
		this.register(Integer.MAX_VALUE - 10);
		this.setPosition(0, 50);
		this.isVisible = true;
		width = 65;
		bg = ResourceManager.getTexture("lsb");
		c1 = TextureTools.getTextureCorner(bg,0,0);
		c2 = TextureTools.getTextureCorner(bg,width,0);
		c3 = TextureTools.getTextureCorner(bg,width,Game.SCREEN_HEIGHT - this.posY);
		c4 = TextureTools.getTextureCorner(bg,0,Game.SCREEN_HEIGHT - this.posY);
		
		GuiButton freehandModeButton = new GuiButton(this, 0);
		freehandModeButton.setTexture(ResourceManager.getTexture("KimpButtons"));
		freehandModeButton.setSizeAndPosition(5, 10, 25, 25, 0, 0);
		
		GuiButton b = new GuiButton(this, 1);
		b.setTexture(ResourceManager.getTexture("KimpButtons"));
		b.setSizeAndPosition(35, 10, 25, 25, 0, 25);
		
		GuiButton c = new GuiButton(this, 2);
		c.setTexture(ResourceManager.getTexture("KimpButtons"));
		c.setSizeAndPosition(5, 40, 25, 25, 0, 50);
	}
	
	@Override
	public void buttonPressed(int buttonID) {
		if (buttonID == 0)
			DrawController.set(DrawableFreehand.class);
		if (buttonID == 1)
			DrawController.set(DrawableRectangle.class);
		if (buttonID == 2)
			DrawController.set(DrawableLine.class);
	}

	@Override
	public void tilePressed(int tileID) {
		// TODO Auto-generated method stub
	}

	@Override
	public void draw(float delta) {
		Color.white.bind();
		Game.startGLMode2D();
		GL11.glPushMatrix();
			TextureImpl.bindNone();
			GL11.glBlendFunc(GL11.GL_SRC_ALPHA, 
					GL11.GL_ONE_MINUS_SRC_ALPHA);
			GL11.glDisable(GL11.GL_BLEND);
			bg.bind();
			GL11.glBegin(GL11.GL_QUADS);
				GL11.glTexCoord2f(0,0);
				GL11.glVertex2f(0, 0 + this.posY);
				GL11.glTexCoord2f(c2.x,0);
				GL11.glVertex2f(width, 0 + this.posY);
				GL11.glTexCoord2f(c3.x,c3.y );
				GL11.glVertex2f(width, Game.SCREEN_HEIGHT + this.posY);
				GL11.glTexCoord2f(0,c4.y);
				GL11.glVertex2f(0, Game.SCREEN_HEIGHT + this.posY);
			GL11.glEnd();
		GL11.glPopMatrix();
		Game.endGLMode2D();
	}

	@Override
	public void update(float delta) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onDisplayResize(int x, int y) {
		// TODO Auto-generated method stub
	}

	@Override
	public void finalize() {
		// TODO Auto-generated method stub
	}

	@Override
	public void scrollerMoved(int scrollerID, GuiScroller scroller) {
		// TODO Auto-generated method stub
		
	}
}
