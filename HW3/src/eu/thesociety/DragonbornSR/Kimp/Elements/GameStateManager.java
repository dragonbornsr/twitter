package eu.thesociety.DragonbornSR.Kimp.Elements;


import com.coffeagame.Engine.Objects.CoreObject;
import com.coffeagame.Engine.media.ResourceManager;

import eu.thesociety.DragonbornSR.Kimp.Gui.*;
import eu.thesociety.DragonbornSR.Kimp.controllers.DrawController;
import eu.thesociety.DragonbornSR.Kimp.controllers.SoundController;

public class GameStateManager extends CoreObject  {
	
	/**
	 * Enum that holds different states.
	 * @author ROG
	 *
	 */
	public static enum GameState {
		/** Initializing state. */
		INITIALIZING (0),
		/** Loading State. */
		LOADING (1),
		/** Playing State. */
		PLAYING(2),
		/** Game State. */
		PAUSED(3);
		/** Game State. */
		private final int state;
		
		/**
		 * Constructor.
		 * @param state2 GameState.
		 */
		GameState(int state) {
			this.state = state;
		}
		/**
		 * Method to get game State.
		 * @return game state.
		 */
		public int getState() {
			return state;
		}
	}
	/**
	 * Current game State.
	 */
	public static GameState gameState = GameState.INITIALIZING;
	
	/**
	 * Construcor. Registers gameStateManager to GameObjects list.
	 */
	public GameStateManager() {
		gameState = GameState.LOADING;
	}
	
	@Override
	public void update(float delta) {
		if ((!ResourceManager.load) 
				&& (gameState == GameState.LOADING)) {
			gameState = GameState.PLAYING;
			new GuiBackground();
			//new Background();
			new GameCursor();
			new DrawController();
			new SideBarLeft();
			new SideBarTop();
			new SoundController();
			new GuiExit();
		}
	}

	@Override
	public void finalize() {
		
	}
}
