package eu.thesociety.DragonbornSR.TwitterGUI.GUI;

import java.util.ArrayList;
import java.util.List;

import org.lwjgl.input.Keyboard;
import org.newdawn.slick.Color;

import com.coffeagame.Engine.Game;
import com.coffeagame.Engine.event.interfaces.IKeyEvent;
import com.coffeagame.Engine.event.interfaces.ITask;
import com.coffeagame.Engine.event.types.KeyEvent;
import com.coffeagame.Engine.gui.Gui;
import com.coffeagame.Engine.gui.GuiButton;
import com.coffeagame.Engine.gui.GuiTextBox;
import com.coffeagame.Engine.media.ResourceManager;

import eu.thesociety.DragonbornSR.Twitter.Tweet;
import eu.thesociety.DragonbornSR.TwitterGUI.TwitterGui;
import eu.thesociety.DragonbornSR.TwitterGUI.objects.Page;
import eu.thesociety.DragonbornSR.TwitterGUI.objects.TweetTile;


public class GuiSearch extends Gui implements ITask, IKeyEvent {
	
	public List<Tweet> lastTweets = new ArrayList<Tweet>();
	public boolean hasNewResults = false;
	private GuiTextBox keywordBox;
	private GuiTextBox locationBox;
	private GuiTextBox distanceBox;
	private GuiTextBox countBox;
	private GuiButton sortButton;
	private int sortState = 0; // 0 No sorting, 1 date 2 date desc, 3 username 4 username desc 5 content 6 content desc
	private String[] sortTexture = {"s0", "s1", "s2", "s3", "s4", "s5", "s6"};
	
	public GuiSearch() {
		this.register(100);
		this.isVisible = true;
		
		this.width = 1280 - 64;
		this.height = 200;
		this.posX = 64;
		this.posY = 0;
		Color fontcolor = new Color(1,1,1f,0.5f);
		
		GuiButton search = new GuiButton(this, 0);
		search.setTexture(ResourceManager.getTexture("search"));
		search.setSizeAndPosition(this.width - 90, 20, 50, 50, 0, 0);
		this.listOfGuiButtons.add(search);
		
		keywordBox = new GuiTextBox(this, 0)
				.setPositionAndSize(10, 10, 400, 30)
				.setBackground(ResourceManager.getTexture("trans"))
				.setDefaultString("Keyword")
				.setFontName("DroidSans")
				.setFontColor(fontcolor);
		
		locationBox = new GuiTextBox(this, 0)
				.setPositionAndSize(420, 10, 200, 30)
				.setBackground(ResourceManager.getTexture("trans"))
				.setDefaultString("Location")
				.setFontName("DroidSans")
				.setFontColor(fontcolor)
				.setRegex("^[A-Za-z0-9, ��������]*$");
		
		distanceBox = new GuiTextBox(this, 0)
				.setPositionAndSize(630, 10, 60, 30)
				.setBackground(ResourceManager.getTexture("trans")).
				setDefaultString("km")
				.setFontName("DroidSans")
				.setFontColor(fontcolor)
				.setMaxTextLenght(4, true)
				.setRegex("^[0-9]*$");

		countBox = new GuiTextBox(this, 0)
				.setPositionAndSize(700, 10, 35, 30)
				.setBackground(ResourceManager.getTexture("trans"))
				.setDefaultString("81")
				.setFontName("DroidSans")
				.setFontColor(fontcolor)
				.setCursorEnabled(false)
				.setMaxTextLenght(2, true)
				.setRegex("^[0-9]*$");
		
		sortButton = new GuiButton(this, 1);
		sortButton.setTexture(ResourceManager.getTexture("s0"));
		sortButton.setSizeAndPosition(745, 10, 124, 30, 0, 0);
		this.listOfGuiButtons.add(sortButton);
			
	}

	@Override
	public void buttonPressed(int id) {
		if (id == 0) {
			search();
		}
		
		if (id == 1) {
			sortState++;
			if (sortState > 6) {
				sortState = 0;
			}
			this.sortButton.setTexture(ResourceManager.getTexture(this.sortTexture[sortState]));
		}
	}
	
	private void search() {
		String spar1 = "";
		String spar2 = "";
		String spar3 = "";
		if (this.sortState != 0) {
			spar1 = "-sort";
			switch (sortState) {
				case 1:
					spar2 = "date";
					spar3 = "";
					break;
				case 2:
					spar2 = "date";
					spar3 = "desc";
					break;
				case 3:
					spar2 = "author";
					spar3 = "";
					break;
				case 4:
					spar2 = "author";
					spar3 = "desc";	
					break;
				case 5:
					spar2 = "content";
					spar3 = "";
					break;
				case 6:
					spar2 = "content";
					spar3 = "desc";
					break;
			}
		}
		
		if (locationBox.isEmty() && keywordBox.isEmty())
			keywordBox.setText("Twitter");
		if (!locationBox.isEmty() && distanceBox.isEmty())
			distanceBox.setText("Auto");
		TwitterGui.tweetsGui.currentPage = 0;
		for (Page p : TwitterGui.tweetsGui.pages) {
			for (TweetTile t : p.getTiles()) {
				Game.listOfDrawableGameObjects.remove(t);
			}
		}
		String[] args= {keywordBox.isEmty()?"":"-search" , keywordBox.isEmty()?"":keywordBox.getText(),
				locationBox.isEmty()?"":"-location" , locationBox.isEmty()?"":(locationBox.getText().equals("Auto")?"0":locationBox.getText()),
				(distanceBox.isEmty() || locationBox.isEmty())?"":distanceBox.getText(),
				"-c" ,countBox.isEmty()?"81":countBox.getText(), spar1, spar2, spar3
				};
		Thread t = new Thread(TwitterGui.searchObject);
		TwitterGui.searchObject.setArgs(args);
		t.start();
	}

	@Override
	public boolean isEnabled() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isDeleted() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void keyPressed(KeyEvent key) {
		if (key.getKey() == Keyboard.KEY_RETURN) {
			search();
		} else if (key.getKey() == Keyboard.KEY_TAB) {
			if (Game.isFocused(keywordBox)) {
				locationBox.setFocused();
				keywordBox.updateDisplayString();
			} else if (Game.isFocused(locationBox)) {
				distanceBox.setFocused();
				locationBox.updateDisplayString();
			}
			else if (Game.isFocused(distanceBox)) {
				countBox.setFocused();
				distanceBox.updateDisplayString();
			}
			else if (Game.isFocused(countBox)) {
				keywordBox.setFocused();
				countBox.updateDisplayString();
			}
		}
		
	}

	@Override
	public void keyReleased(KeyEvent key) {
		// TODO Auto-generated method stub
		
	}
}
