package eu.thesociety.DragonbornSR.TwitterGUI.GUI;

import com.coffeagame.Engine.gui.Gui;
import com.coffeagame.Engine.gui.GuiButton;
import com.coffeagame.Engine.media.ResourceManager;

import eu.thesociety.DragonbornSR.TwitterGUI.TwitterGui;

public class GuiNavigation extends Gui {
	
	public GuiNavigation() {
		this.register(500);
		this.isVisible = true;
		this.width = 115;
		this.height = 50;
		this.posX = 1280 - width - 40;
		this.posY = 720 -height - 13;
		GuiButton back = new GuiButton(this, 0);
		back.setTexture(ResourceManager.getTexture("ForwardBack"));
		back.setSizeAndPosition(0, 0, 50, 50, 0, 50);
		this.listOfGuiButtons.add(back);
		
		GuiButton forward = new GuiButton(this, 1);
		forward.setTexture(ResourceManager.getTexture("ForwardBack"));
		forward.setSizeAndPosition(65, 0, 50, 50, 0, 0);
		this.listOfGuiButtons.add(forward);
	}
	
	@Override
	public void draw(float delta) {
		
	}
	
	@Override
	public void buttonPressed(int buttonID) {
		if (buttonID == 0) {
			TwitterGui.tweetsGui.displayPreviousPage();
		}
		else if (buttonID == 1) {
			TwitterGui.tweetsGui.displayNextPage();
		}
	}
}
