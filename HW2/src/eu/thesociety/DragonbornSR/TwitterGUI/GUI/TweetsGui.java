package eu.thesociety.DragonbornSR.TwitterGUI.GUI;

import java.awt.datatransfer.*;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.util.ArrayList;
import java.util.List;

import org.newdawn.slick.opengl.Texture;

import com.coffeagame.Engine.gui.Gui;

import eu.thesociety.DragonbornSR.Twitter.Tweet;
import eu.thesociety.DragonbornSR.TwitterGUI.TwitterGui;
import eu.thesociety.DragonbornSR.TwitterGUI.objects.Page;
import eu.thesociety.DragonbornSR.TwitterGUI.objects.TweetTile;

public class TweetsGui extends Gui {

	private Texture bg;
	public List<Page> pages = new ArrayList<Page>();
	public int currentPage = 0;
	
	public int tileWidth = 400;
	public int tileHeight = 170;
	public TweetsGui() {
		this.isVisible = true;
		this.register(100);
		this.posX = 10;
		this.posY = 104;
		this.width = 1280 - 20;
		this.height = 720 - 114;
		this.refreshContent(TwitterGui.guiSearch.lastTweets);
	}
	
	@Override
	public void update(float delta) {
		if (TwitterGui.guiSearch.hasNewResults) {
			TwitterGui.guiSearch.hasNewResults = false;
			this.refreshContent(TwitterGui.guiSearch.lastTweets);
		}
	}
	
	@Override
	public void draw(float delta) {
		
	}
	
	@Override
	public void tilePressed(int tileID) {
	}
	
	public void displayNextPage() {
		if (hasNextPage()) {
			pages.get(this.currentPage).setVisible(false);
			pages.get(this.currentPage).isCurrentPage = false;
			pages.get(this.currentPage + 1).setVisible(true);
			pages.get(this.currentPage + 1).isCurrentPage = true;
			this.currentPage += 1;
		}
	}
	
	public void displayPreviousPage() {
		if (hasPreviousPage()) {
			pages.get(this.currentPage).setVisible(false);
			pages.get(this.currentPage).isCurrentPage = false;
			pages.get(this.currentPage - 1).setVisible(true);
			pages.get(this.currentPage - 1).isCurrentPage = true;
			this.currentPage -= 1;
		}
	}
	
	public boolean hasNextPage() {
		if (this.pages.size() - 1 > this.currentPage)
			return true;
		return false;
	}
	
	public boolean hasPreviousPage() {
		if (this.currentPage > 0)
			return true;
		return false;
	}
	
	@Deprecated
	public void refreshContent(List<Tweet> tweets) {
		for (Page p : this.pages) {
			p.delete();
		}
		this.pages.clear();
		
		int row = 0;
		int column = 0;
		List<TweetTile> temp = new ArrayList<TweetTile>();
		for (Tweet tweet : tweets) {;
			TweetTile t = new TweetTile(this, column * 3 + row);
			t.setParentPage(new Page());
			t.setTweet(tweet);
			t.setSizeAndPosition(10 + row*tileWidth + row*10,10 + column*tileHeight + column*5, tileWidth, tileHeight);
			t.visible = true;
			temp.add(t);
			row++;
			if (row > 2) {
				row = 0;
				column++;
				if (column > 2) {
					column = 0;
					Page pg = new Page(temp);
					this.pages.add(pg);
					temp = new ArrayList<TweetTile>();
				}
			}
			
			
		}
		
		if (temp.size() > 0) {
			this.pages.add(new Page(temp));
		}
		if (pages.size() > 0) {
			pages.get(0).isCurrentPage = true;
			pages.get(0).setVisible(true);
		}
	}
}
