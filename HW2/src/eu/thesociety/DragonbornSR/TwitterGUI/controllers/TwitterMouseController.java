package eu.thesociety.DragonbornSR.TwitterGUI.controllers;

import java.util.List;

import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;

import com.coffeagame.Engine.Objects.DrawableGameObject;
import com.coffeagame.Engine.Objects.GameObject;
import com.coffeagame.Engine.annotations.Placeholder;
import com.coffeagame.Engine.event.MouseController;
import com.coffeagame.Engine.event.interfaces.IMouseEvent;
import com.coffeagame.Engine.event.types.MouseEvent;

public class TwitterMouseController extends MouseController {
	@Override
	public void update(float delta) {
		/*
		 * Avoid Concurrent Exceptions on removing while in the loop!
		 */
		for (GameObject o : MouseController.getListOfMouseEventListeners())
			if (((com.coffeagame.Engine.event.interfaces.IMouseEvent) o)
					.isDeleted()) {
				MouseController.listOfDeleted.add(o);
			}
		for (GameObject o : MouseController.listOfDeleted)
			if (((com.coffeagame.Engine.event.interfaces.IMouseEvent) o)
					.isDeleted()) {
				MouseController.getListOfMouseEventListeners().remove(o);
			}
		MouseController.listOfDeleted.clear();
		MouseEvent e;
		while (Mouse.next()) {
			if (Mouse.getEventButton() == -1)
				continue;
			e = new MouseEvent(Mouse.getX(), Display.getHeight()-Mouse.getY(), Mouse.getEventButton());
			if (Mouse.getEventButtonState()) {
				forwardMousePressed(e);
			} else {
				forwardMouseReleased(e);
			}
		}

		boolean foundFirst = false;
		for (GameObject o : getListOfMouseEventListeners()) {
			if (((IMouseEvent)o).inBounds(new MouseEvent(Mouse.getX(), Display.getHeight()-Mouse.getY())) && (!foundFirst) && (o.enabled)) {
				if (o instanceof DrawableGameObject) {
					if (((DrawableGameObject)o).visible) {
						foundFirst = ((IMouseEvent) o).setHover(true);
					}
				} else
					foundFirst = ((IMouseEvent) o).setHover(true);
			} else {
				((IMouseEvent) o).setHover(false);
			}
		}
	}

	@Override
	protected void forwardMousePressed(MouseEvent e) {
		for (GameObject o : getListOfMouseEventListeners()) {
			if (((IMouseEvent) o).inBounds(e) && (o.enabled)) {
				if (o instanceof DrawableGameObject) {
					if (((DrawableGameObject)o).visible) {
						((com.coffeagame.Engine.event.interfaces.IMouseEvent) o)
						.mousePressed(e);
						return;
					}
				} else {
				((com.coffeagame.Engine.event.interfaces.IMouseEvent) o)
						.mousePressed(e);
					return;
				}
			}
		}
	}
	@Override
	protected void forwardMouseReleased(MouseEvent e) {
		for (Object o : getListOfMouseEventListeners())
			if (((IMouseEvent) o).inBounds(e) && (((GameObject) o).enabled)) {
				if (o instanceof DrawableGameObject) {
					if (((DrawableGameObject)o).visible) {
						((com.coffeagame.Engine.event.interfaces.IMouseEvent) o)
						.mouseReleased(e);
						return;
					}
				} else {
				((com.coffeagame.Engine.event.interfaces.IMouseEvent) o)
						.mouseReleased(e);
					return;
				}
			}
	}
	
	/**
	 * Register a GameObject to be a IMouseEvent Listener.  Make sure that
	 * GameObject implements IMouseEvent.
	 * @param listener
	 * @param priority
	 */
	@Placeholder
	public void registerMouseEventListener(GameObject listener, int priority) {
		boolean added = false;
		if (listOfMouseEventListeners.isEmpty()) {
			listOfMouseEventListeners.add(0, listener);
			added = true;
		} else {
			for (GameObject mel : listOfMouseEventListeners) {
				if (mel.priority <= priority) {
					int index = listOfMouseEventListeners.indexOf(mel);
					listOfMouseEventListeners.add(index, listener);
					added = true;
					break;
				}
			}
			if (!added) {
				listOfMouseEventListeners.add(listener);
			}
		}
	}
	
	/**
	 * Change priority of a button. Use this when bringing a GUI/Button forward relative to any other.
	 * @param listener
	 * @param priority
	 */
	public void changePriority(GameObject listener, int priority) {
		listOfMouseEventListeners.remove(listener);
		registerMouseEventListener(listener, priority);
	}

	public static List<GameObject> getListOfMouseEventListeners() {
		return listOfMouseEventListeners;
	}
}
