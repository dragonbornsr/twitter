package eu.thesociety.DragonbornSR.TwitterGUI;

import java.awt.Font;

import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;
import org.newdawn.slick.Color;
import org.newdawn.slick.TrueTypeFont;
import org.newdawn.slick.opengl.Texture;

import com.coffeagame.Engine.Game;
import com.coffeagame.Engine.media.ResourceManager;
import com.coffeagame.Engine.scene.GLViewPort;

import eu.thesociety.DragonbornSR.TwitterGUI.GUI.GuiNavigation;
import eu.thesociety.DragonbornSR.TwitterGUI.GUI.GuiSearch;
import eu.thesociety.DragonbornSR.TwitterGUI.GUI.TweetsGui;
import eu.thesociety.DragonbornSR.TwitterGUI.controllers.TwitterMouseController;
import eu.thesociety.DragonbornSR.TwitterGUI.objects.SearchObject;

public class TwitterGui extends Game {

	/** resource manager. */
	public static ResourceManager resourceManager;
	/** Game cursor. */
	static GameCursor cursor;
	
	/** Player viewport (2D). */
	ViewPort vp;

	public static GuiSearch guiSearch;
	public static TweetsGui tweetsGui;
	public static GuiNavigation guiNavigation;
	public static SearchObject searchObject;
	
	public static Texture defaultPNG;
	
	public static Color fontcolor = new Color(50,50,50);
	public static TrueTypeFont font;
	public static Font awtFont = null;
	
	public static Color tweetfontcolor = new Color(40,40,50, 0.5f);
	public static TrueTypeFont tweetfont;
	public static Font tweetawtFont = null;
	
	public static Color extrafontcolor = new Color(40,40,50, 0.3f);
	public static TrueTypeFont extrafont;
	public static Font extraawtFont = null;
	
	@Override
	protected void preInit() {
		super.init();
		
		th.setMaxTPS(60);
		currentDisplayMode = 0;
	}
	
	@Override
	protected void init() {
		super.init();
		/*Start Here*/
		new GameStateManager();
		vp =  new ViewPort();
		vp.setAspectRatio(1.0f);
		listOfCoreObjects.add(keyController);
		mouseController = new TwitterMouseController();
		listOfCoreObjects.add(mouseController);
		listOfCoreObjects.add(gameEventController);
		resourceManager = new ResourceManager();
		searchObject = new SearchObject();
		listOfCoreObjects.add(searchObject);
	}
	
	@Override
	protected void postInit() {
		super.postInit();
		/*Start Here*/
	}
	

	@Override 
	public void drawLoop() {
		/*
		 * Clear OpenGL window before drawing all objects.
		 */
		GL11.glClear(GL11.GL_COLOR_BUFFER_BIT 
				| GL11.GL_DEPTH_BUFFER_BIT);
		super.drawLoop();
		
		GL30.glBindFramebuffer(GL30.GL_READ_FRAMEBUFFER, fbo);
		GL11.glReadBuffer(GL30.GL_COLOR_ATTACHMENT0);
		GL30.glBindFramebuffer(GL30.GL_DRAW_FRAMEBUFFER, 0);
		GL20.glDrawBuffers(GL11.GL_BACK_LEFT);

		GL30.glBlitFramebuffer(0, 0, Display.getWidth(), 
				Display.getHeight(), 0, 0, Display.getWidth(), 
				Display.getHeight(), GL11.GL_COLOR_BUFFER_BIT, 
				GL11.GL_NEAREST);
		GL30.glBindFramebuffer(GL30.GL_FRAMEBUFFER, 0);
		
	}
	/**
	 * Start drawing in 2D.
	 */
	public static void startGLMode2D() { GLViewPort.startGLMode2D(); }
	/**
	 * End drawing in 2D.
	 */
	public static void endGLMode2D() { GLViewPort.endGLMode2D(); }
	
}