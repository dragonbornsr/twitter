package eu.thesociety.DragonbornSR.TwitterGUI;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;
import java.net.HttpURLConnection;
import javax.imageio.ImageIO;

import eu.thesociety.DragonbornSR.Twitter.Interfaces.AsyncImageCallback;


public class AsyncImageLoader extends Thread {
    
    /** @brief Start an asynchronous image fetch operation.
      * @param url The URL of the remote picture.
      * @param cb The AsyncImageCallback object you want to be notified the operation completes.
      */
    public AsyncImageLoader(String url, AsyncImageCallback cb) {
        super();
        mURL=url;
        mCallback=cb;
        start();
    }
    public void run() {
        mCallback.onImageReceived(mURL,getImageFromURL(mURL));
    }
    private String mURL;
    private AsyncImageCallback mCallback;
    
    public static synchronized BufferedImage getImageFromURL(String url) {
    	HttpURLConnection conn = null;
		try {
			conn = (HttpURLConnection)(new URL(url)).openConnection();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        conn.setDoInput(true);
        try {
			conn.connect();
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
        try {
			return ImageIO.read(conn.getInputStream());
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
    }
}