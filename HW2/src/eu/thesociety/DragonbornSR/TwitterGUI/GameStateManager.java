package eu.thesociety.DragonbornSR.TwitterGUI;

import java.awt.Font;

import org.newdawn.slick.TrueTypeFont;

import com.coffeagame.Engine.Objects.GameObject;
import com.coffeagame.Engine.media.ResourceManager;

import eu.thesociety.DragonbornSR.TwitterGUI.GUI.GuiNavigation;
import eu.thesociety.DragonbornSR.TwitterGUI.GUI.GuiSearch;
import eu.thesociety.DragonbornSR.TwitterGUI.GUI.TweetsGui;
import eu.thesociety.DragonbornSR.TwitterGUI.objects.Background;

public class GameStateManager extends GameObject  {
	
	/**
	 * Enum that holds different states.
	 * @author ROG
	 *
	 */
	public static enum GameState {
		/** Initializing state. */
		INITIALIZING (0),
		/** Loading State. */
		LOADING (1),
		/** Playing State. */
		PLAYING(2);
		/** Game State. */
		private final int state;
		
		/**
		 * Constructor.
		 * @param state2 GameState.
		 */
		GameState(int state) {
			this.state = state;
		}
		/**
		 * Method to get game State.
		 * @return game state.
		 */
		public int getState() {
			return state;
		}
	}
	/**
	 * Current game State.
	 */
	public GameState gameState = GameState.INITIALIZING;
	
	/**
	 * Construcor. Registers gameStateManager to GameObjects list.
	 */
	public GameStateManager() {
		this.register(0);
	}
	
	@Override
	public void update(float delta) {
		if ((!ResourceManager.load) 
				&& (this.gameState == GameState.INITIALIZING)) {
			this.gameState = GameState.PLAYING;
			TwitterGui.guiSearch = new GuiSearch();
			new Background();
			new GameCursor();
			TwitterGui.tweetsGui = new TweetsGui();
			TwitterGui.guiNavigation = new GuiNavigation();
			
			TwitterGui.awtFont = TwitterGui.resourceManager.getFont("DroidSans");
			TwitterGui.awtFont = TwitterGui.awtFont.deriveFont(Font.PLAIN, 20.0f);
			TwitterGui.font = new TrueTypeFont(TwitterGui.awtFont, true);
			
			TwitterGui.tweetawtFont = TwitterGui.resourceManager.getFont("DroidSans");
			TwitterGui.tweetawtFont = TwitterGui.tweetawtFont.deriveFont(Font.PLAIN, 15.0f);
			TwitterGui.tweetfont = new TrueTypeFont(TwitterGui.tweetawtFont, true);
			
			TwitterGui.extraawtFont = TwitterGui.resourceManager.getFont("DroidSans");
			TwitterGui.extraawtFont = TwitterGui.extraawtFont.deriveFont(Font.PLAIN, 14.0f);
			TwitterGui.extrafont = new TrueTypeFont(TwitterGui.extraawtFont, true);
			
			TwitterGui.defaultPNG = TwitterGui.resourceManager.getTexture("defaultTweet");
		}
	}
}
