package eu.thesociety.DragonbornSR.TwitterGUI.objects;

import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.StringTokenizer;

import javax.imageio.ImageIO;

import org.lwjgl.opengl.GL11;
import org.lwjgl.util.vector.Vector2f;
import org.newdawn.slick.Color;
import org.newdawn.slick.opengl.Texture;
import org.newdawn.slick.opengl.TextureImpl;
import org.newdawn.slick.opengl.TextureLoader;

import com.coffeagame.Engine.Game;
import com.coffeagame.Engine.Utility.TextureTools;
import com.coffeagame.Engine.event.types.MouseEvent;
import com.coffeagame.Engine.gui.Gui;
import com.coffeagame.Engine.gui.GuiTile;
import com.coffeagame.Engine.media.ResourceManager;

import eu.thesociety.DragonbornSR.Twitter.Tweet;
import eu.thesociety.DragonbornSR.Twitter.Interfaces.AsyncImageCallback;
import eu.thesociety.DragonbornSR.TwitterGUI.AsyncImageLoader;
import eu.thesociety.DragonbornSR.TwitterGUI.TwitterGui;

public class TweetTile extends GuiTile implements AsyncImageCallback {
	
	private Texture bg;
	private Page page;
	public Tweet tweet;
	private Texture img, imgoverlay;
	int texwidth = 64;
	int texheight = 64;
	BufferedImage bi = null;
	boolean recievedTexture = false;
	private float dateOffset = 0f;
	
	protected Vector2f c1 = new Vector2f(0, 0), // Corner 1 is upper left,
			 c2 = new Vector2f(0, 0), // following ones are clockwise.
			 c3 = new Vector2f(0, 0),
			 c4 = new Vector2f(0, 0);
	

	public TweetTile(Gui parent, int tileID) {
		this.parent = parent;
		parent.listOfGuiTiles.add(this);
		//register(parent.priority + 1);
		this.tileID = tileID;
		this.enabled = false;
		bg = ResourceManager.getTexture("tilebg");
		imgoverlay = ResourceManager.getTexture("profileOverlay");
		
		
	}
	
	@Override
	public void update(float delta) {
		if (recievedTexture && (bi != null)) {
			recievedTexture = false;
			try {
				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				ImageIO.write( bi, "png", baos );
				byte[] imageInByte = baos.toByteArray();
				img = TextureLoader.getTexture("jpeg", new ByteArrayInputStream(imageInByte));
				this.updateTextureCorners(48);
				
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	@Override
	public void mousePressed(MouseEvent e) {
		System.out.println("copied tweet to clipboard");
		Toolkit toolkit = Toolkit.getDefaultToolkit();
		Clipboard clipboard = toolkit.getSystemClipboard();
		StringSelection strSel = new StringSelection(tweet.tweetText);
		clipboard.setContents(strSel, null);
		parent.tilePressed(this.tileID);
	}
	
	@Override
	public void draw(float delta) {
		if (this.page.isCurrentPage) {
			//Draw background
			Game.startGLMode2D();
				GL11.glPushMatrix();
				TextureImpl.bindNone();
				Color.white.bind();
				GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
				GL11.glDisable(GL11.GL_BLEND);
				bg.bind();
				GL11.glBegin(GL11.GL_QUADS);
					GL11.glTexCoord2f(0, 0);
					GL11.glVertex2f(parent.posX + posX + page.pageOffX,  parent.posY + posY);
					GL11.glTexCoord2f(1, 0);
					GL11.glVertex2f(parent.posX + posX + width + page.pageOffX,  parent.posY + posY);
					GL11.glTexCoord2f(1, 1);
					GL11.glVertex2f(parent.posX + posX + width + page.pageOffX,  parent.posY + posY + height);
					GL11.glTexCoord2f(0, 1);
					GL11.glVertex2f(parent.posX + posX + page.pageOffX,  parent.posY + posY + height);
				GL11.glEnd();

				GL11.glPushMatrix();
				TextureImpl.bindNone();
				Color.white.bind();
				GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
				GL11.glEnable(GL11.GL_BLEND);
				img.bind();
				GL11.glBegin(GL11.GL_QUADS);
					GL11.glTexCoord2f(c1.x, c1.y);
					GL11.glVertex2f(parent.posX + posX + page.pageOffX,  parent.posY + posY);
					GL11.glTexCoord2f(c2.x, c2.y);
					GL11.glVertex2f(parent.posX + posX + texwidth + page.pageOffX,  parent.posY + posY);
					GL11.glTexCoord2f(c3.x, c3.y);
					GL11.glVertex2f(parent.posX + posX + texwidth + page.pageOffX,  parent.posY + posY + texheight);
					GL11.glTexCoord2f(c4.x, c4.y);
					GL11.glVertex2f(parent.posX + posX + page.pageOffX,  parent.posY + posY + texheight);
				GL11.glEnd();

				GL11.glPushMatrix();
				TextureImpl.bindNone();
				Color.white.bind();
				GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
				GL11.glEnable(GL11.GL_BLEND);
				imgoverlay.bind();
				GL11.glBegin(GL11.GL_QUADS);
					GL11.glTexCoord2f(0, 0);
					GL11.glVertex2f(parent.posX + posX + page.pageOffX,  parent.posY + posY);
					GL11.glTexCoord2f(0, 1);
					GL11.glVertex2f(parent.posX + posX + texwidth + page.pageOffX,  parent.posY + posY);
					GL11.glTexCoord2f(1, 1);
					GL11.glVertex2f(parent.posX + posX + texwidth + page.pageOffX,  parent.posY + posY + texheight);
					GL11.glTexCoord2f(1, 0);
					GL11.glVertex2f(parent.posX + posX + page.pageOffX,  parent.posY + posY + texheight);
				GL11.glEnd();

				GL11.glPopMatrix();
				GL11.glPushMatrix();
				TextureImpl.bindNone();
				Color.white.bind();
				GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
				GL11.glEnable(GL11.GL_BLEND);
				TwitterGui.font.drawString(this.parent.posX + this.posX + 69,this.parent.posY + this.posY + 5,this.tweet.username, TwitterGui.fontcolor);
				TwitterGui.extrafont.drawString(this.parent.posX + this.posX + 69,this.parent.posY + this.posY + 30, 
							"Retweets: " + tweet.retweetCount + "   Favourites: " + tweet.favouriteCount, TwitterGui.extrafontcolor);
				TwitterGui.extrafont.drawString(this.parent.posX + this.posX + dateOffset,this.parent.posY + this.posY + this.height - 20, 
							tweet.date.toLocaleString() ,TwitterGui.extrafontcolor);
					int lineOffset = 15;
					for (String line : splitIntoLine(this.tweet.tweetText, 47)) {
						if (line.length() > 1) {
							TwitterGui.tweetfont.drawString(this.parent.posX + this.posX + 5,this.parent.posY + this.posY + 50 + lineOffset,line, TwitterGui.tweetfontcolor);
							lineOffset += 15;
						}
					}
					
				GL11.glDisable(GL11.GL_BLEND);
				GL11.glPopMatrix();
			Game.endGLMode2D();
		}
	}
	
	public void setTweet(Tweet t) {
		this.tweet = t;
		this.dateOffset = this.width-TwitterGui.extrafont.getWidth(t.date.toLocaleString()) - 6;
		img = TwitterGui.defaultPNG;
			new AsyncImageLoader(tweet.imageURL, this);
		this.updateTextureCorners(256);
	}
	
	public void setParentPage(Page p) {
		this.page = p;
	}
	
	@Override
	public void finalize() {
		Game.listOfDeletedDrawableGameObjects.add(this);
	}
	
	
	public void updateTextureCorners(int i) {
		c1 = TextureTools.getTextureCorner(this.img, 0, 0);
		c2 = TextureTools.getTextureCorner(this.img, i ,0);
		c3 = TextureTools.getTextureCorner(this.img, i, i);
		c4 = TextureTools.getTextureCorner(this.img, 0, i);
	}
	
	private String[] splitIntoLine(String input, int maxCharInLine){
		input = input.replace("\n", " ");
	    StringTokenizer tok = new StringTokenizer(input, " ");
	    StringBuilder output = new StringBuilder(input.length());
	    int lineLen = 0;
	    while (tok.hasMoreTokens()) {
	        String word = tok.nextToken();

	        while(word.length() > maxCharInLine){
	            output.append(word.substring(0, maxCharInLine-lineLen) + "\n");
	            word = word.substring(maxCharInLine-lineLen);
	            lineLen = 0;
	        }

	        if (lineLen + word.length() > maxCharInLine) {
	            output.append("\n");
	            lineLen = 0;
	        }
	        output.append(word + " ");

	        lineLen += word.length() + 1;
	    }
	    return output.toString().split("\n");
	}

	@Override
	public void onImageReceived(String url, BufferedImage i) {
		if (i != null) {
				this.bi = i;
				recievedTexture = true;
		}
	}
}
