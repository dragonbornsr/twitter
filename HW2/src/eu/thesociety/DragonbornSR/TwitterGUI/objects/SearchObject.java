package eu.thesociety.DragonbornSR.TwitterGUI.objects;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.coffeagame.Engine.Game;
import com.coffeagame.Engine.Objects.CoreObject;

import eu.thesociety.DragonbornSR.Twitter.ConfigurationManager;
import eu.thesociety.DragonbornSR.Twitter.Main;
import eu.thesociety.DragonbornSR.Twitter.Tweet;
import eu.thesociety.DragonbornSR.Twitter.TwitterApplication;
import eu.thesociety.DragonbornSR.TwitterGUI.TwitterGui;

public class SearchObject extends CoreObject implements Runnable {
	
	List<Page> pages = Collections.synchronizedList(new ArrayList<Page>());
	private static boolean taskDone = false;
	private List<Tweet> tweetBuffer;
	//private List<Page> pages = new ArrayList<Page>();
	private static String[] args;
	
	public ConfigurationManager c = new ConfigurationManager();
	public Main program = new Main(c);
	public TwitterApplication ta = new TwitterApplication();
	
	@Override
	public void update(float delta) {
		if (taskDone)
			upd(pages, 0, new Page());
	}
	
	synchronized void upd(List<Page> pg, int action, Page pag) {
		if (action == 0) {
			boolean pagetask = false;
			if (TwitterGui.tweetsGui.pages.size() == 0) {
				pagetask = true;
			}
			TwitterGui.tweetsGui.pages.addAll(pg);
			for (Page p : pg) {
				for (TweetTile t : p.tiles) {
					t.register(TwitterGui.tweetsGui.priority + 1);
				}
			}
			pages = new ArrayList<Page>();
			taskDone = false;
			
			if (pagetask && TwitterGui.tweetsGui.pages.size() > 0) {
				TwitterGui.tweetsGui.pages.get(0).isCurrentPage = true;
				TwitterGui.tweetsGui.pages.get(0).setVisible(true);
			}
		} else {
			this.pages.add(pag);
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	public void setArgs(String[] a) {
		this.args = a;
	}
	
	@Override
	public void run() { 

		ta.executeActions(ta.getActionsFromArguments(args));
		this.tweetBuffer = (List<Tweet>) ta.getTweets();
		for (Page p : TwitterGui.tweetsGui.pages) {
			p.delete();
		}
		TwitterGui.tweetsGui.pages.clear();
		
		int row = 0;
		int column = 0;
		List<TweetTile> temp = new ArrayList<TweetTile>();
		for (Tweet tweet : tweetBuffer) {
			TweetTile t = new TweetTile(TwitterGui.tweetsGui, column * 3 + row);
			t.setParentPage(new Page());
			t.setSizeAndPosition(10 + row*TwitterGui.tweetsGui.tileWidth + row*10,
					10 + column*TwitterGui.tweetsGui.tileHeight + column*5, 
					TwitterGui.tweetsGui.tileWidth, TwitterGui.tweetsGui.tileHeight);
			t.setTweet(tweet);
			t.visible = true;
			temp.add(t);
			row++;
			if (row > 2) {
				row = 0;
				column++;
				if (column > 2) {
					column = 0;
					Page pg = new Page(temp);
					upd(pages, 1, pg);
					this.taskDone = true;
					try {
						Thread.sleep(50);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					temp = new ArrayList<TweetTile>();
				}
			}
		}
		
		if (temp.size() > 0) {
			upd(pages, 1, new Page(temp));
			this.taskDone = true;
		}
	}
	
	public void refreshContent(List<Tweet> tweets) {
		for (Page p : this.pages) {
			p.delete();
		}
		this.pages.clear();
		
		int row = 0;
		int column = 0;
		List<TweetTile> temp = new ArrayList<TweetTile>();
		for (Tweet tweet : tweets) {
			TweetTile t = new TweetTile(TwitterGui.tweetsGui, column * 3 + row);
			t.setParentPage(new Page());
			t.setTweet(tweet);
			t.setSizeAndPosition(10 + row*TwitterGui.tweetsGui.tileWidth + row*10,
					10 + column*TwitterGui.tweetsGui.tileHeight + column*5, 
					TwitterGui.tweetsGui.tileWidth, TwitterGui.tweetsGui.tileHeight);
			t.visible = true;
			temp.add(t);
			row++;
			if (row > 2) {
				row = 0;
				column++;
				if (column > 2) {
					column = 0;
					Page pg = new Page(temp);
					this.pages.add(pg);
					temp = new ArrayList<TweetTile>();
				}
			}
		}
		
		if (temp.size() > 0) {
			this.pages.add(new Page(temp));
		}
	}
}
