package eu.thesociety.DragonbornSR.TwitterGUI.objects;

import java.util.ArrayList;
import java.util.List;

public class Page {
	
	public int pageOffX = 0;
	public boolean isCurrentPage = false;
	public List<TweetTile> tiles = new ArrayList<TweetTile>();
	public Page() {}
	
	public Page(List<TweetTile> tiles) {
		this.tiles = tiles;
		for (TweetTile t : tiles) {
			t.setParentPage(this);
			t.enabled = false;
		}
	}
	
	public Page createPage(List<TweetTile> tiles) {
		return new Page(tiles);
	}
	
	public List<TweetTile> getTiles() {
		return this.tiles;
	}
	
	public void setTiles(List<TweetTile> tiles) {
		this.tiles = tiles;
	}
	
	public void setVisible(boolean state) {
		for (TweetTile t : this.tiles) {
			t.enabled = state;
			t.visible = state;
		}
	}
	public void delete() {
		for (TweetTile t : tiles) {
			t.finalize();
		}
	}
}