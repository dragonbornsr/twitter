package eu.thesociety.DragonbornSR.TwitterGUI.objects;

import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.GL11;
import org.newdawn.slick.opengl.Texture;

import com.coffeagame.Engine.Game;
import com.coffeagame.Engine.Objects.DrawableGameObject;
import com.coffeagame.Engine.media.ResourceManager;

public class Background extends DrawableGameObject{
	private int height;
	private int width;
	Texture bg;
	Texture logo;
	int posX, posY = 0;
	public Background() {
		this.register(0);
		bg = ResourceManager.getTexture("bg");
		logo = ResourceManager.getTexture("logo");
		this.height = Display.getHeight();
		this.width = Display.getWidth();
	}
	
	
	@Override
	public void draw(float delta) {
		Game.startGLMode2D();
		GL11.glPushMatrix();
			GL11.glBlendFunc(GL11.GL_SRC_ALPHA, 
					GL11.GL_ONE_MINUS_SRC_ALPHA);
			GL11.glDisable(GL11.GL_BLEND);
			bg.bind();
			GL11.glBegin(GL11.GL_QUADS);
				GL11.glTexCoord2f(0,0);
				GL11.glVertex2f(posX, posY);
				GL11.glTexCoord2f(1,0);
				GL11.glVertex2f(posX + width, posY);
				GL11.glTexCoord2f(1,1);
				GL11.glVertex2f(posX + width, posY + height);
				GL11.glTexCoord2f(0,1);
				GL11.glVertex2f(posX, posY + height);
			GL11.glEnd();
		GL11.glPopMatrix();
		Game.endGLMode2D();
		
		Game.startGLMode2D();
		GL11.glPushMatrix();
			GL11.glBlendFunc(GL11.GL_SRC_ALPHA, 
					GL11.GL_ONE_MINUS_SRC_ALPHA);
			GL11.glEnable(GL11.GL_BLEND);
			logo.bind();
			GL11.glBegin(GL11.GL_QUADS);
				GL11.glTexCoord2f(0,0);
				GL11.glVertex2f(posX, posY);
				GL11.glTexCoord2f(1,0);
				GL11.glVertex2f(posX + 64, posY);
				GL11.glTexCoord2f(1,1);
				GL11.glVertex2f(posX + 64, posY + 64);
				GL11.glTexCoord2f(0,1);
				GL11.glVertex2f(posX, posY + 64);
			GL11.glEnd();
		GL11.glPopMatrix();
		Game.endGLMode2D();
	}
}
