package eu.thesociety.DragonbornSR.Twitter;

import twitter4j.Query;
import twitter4j.Query.Unit;
import eu.thesociety.DragonbornSR.Twitter.Actions.CountAction;
import eu.thesociety.DragonbornSR.Twitter.Actions.LocationAction;
import eu.thesociety.DragonbornSR.Twitter.Actions.LocationAction.Vector3f;
import eu.thesociety.DragonbornSR.Twitter.Actions.SortAction;
import eu.thesociety.DragonbornSR.Twitter.Interfaces.ITwitterQuery;

public class TwitterQuery implements ITwitterQuery {
	
	public Query query;
	
	private int ca = 50;
	private Vector3f la = null;
	private double radius = 10;
	private Query.Unit unit = Query.Unit.km;
	private String key = "";
	private int page = 1;
	private SortAction sortAction;
	
	public TwitterQuery() {
		
	}
	@Override
	public int getCount() {
		return ca;
	}
	@Override
	public void setCount(int i) {
		this.ca = i;
	}

	public Vector3f getLocation() {
		return la;
	}
	@Override
	public void setLocation(Vector3f vector3f) {
		this.la = vector3f.getValue();
	}

	@Override
	public void setRadius(double radius) {
		this.radius = radius;
	}
		
	@Override
	public double getRadius() {
		return radius;
	}
	@Override
	public void setRadius(LocationAction la) {
		this.radius = (double)la.getValue().r;
	}
	@Override
	public Unit getUnit() {
		return this.unit;
	}
	@Override
	public void setUnit(Unit u) {
		this.unit = u;
		
	}
	@Override
	public String getKey() {
		return key;
	}
	@Override
	public void setKey(String k) {
		this.key = k;
	}
	@Override
	public void setLocation(LocationAction la) {
		this.la = la.getValue();
		
	}
	@Override
	public void setCount(CountAction ca) {
		// TODO Auto-generated method stub
		
	}
	
	public String toString() {
		return "";
	}
	
	public void setPage(int page) {
		this.page = page;
	}
	
	public int getPage() {
		return this.page;
	}
	
	public SortAction getSorting() {
		return this.sortAction;
	}
	
	public void setSorting(SortAction a) {
		this.sortAction = a;
	}
}
