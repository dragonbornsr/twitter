package eu.thesociety.DragonbornSR.Twitter;

import eu.thesociety.DragonbornSR.Twitter.Interfaces.ITweet;
import eu.thesociety.DragonbornSR.TwitterGUI.TwitterGui;

public class Main {

	/**
	 * Start the program execution and 
	 * parse the parameters. pass parameters
	 * to Twitter Object.
	 * 
	 * If parameters are given, program will execute as the
	 * parameters ask, if not, program will start in
	 * interactive mode
	 * @param args
	 */
	public static ConfigurationManager cm;
	
	public static void main(String[] args) {
		for (String arg : args) {
			if (arg.matches("-gui")) {
				TwitterGui game = new TwitterGui();
				try {
					game.Run();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				return;
			}
		}
		
		ConfigurationManager c = new ConfigurationManager();
		if (args.length == 0) {
			Main program = new Main(c);
			program.run();
		} else {
			Main program = new Main(c);
			TwitterApplication ta = new TwitterApplication();
			ta.executeActions(ta.getActionsFromArguments(args));

			for (ITweet t : ta.getTweets()) {
				t.printTweet();
			}
		}
	}
	
	public Main(ConfigurationManager cm) {
		this.cm = cm;
	}
	
	public void run() {

	}
	
}
