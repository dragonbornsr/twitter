package eu.thesociety.DragonbornSR.Twitter;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import twitter4j.GeoLocation;
import twitter4j.Query;
import twitter4j.QueryResult;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;
import eu.thesociety.DragonbornSR.Twitter.Interfaces.ITweet;
import eu.thesociety.DragonbornSR.Twitter.Interfaces.ITwitterQuery;
import eu.thesociety.DragonbornSR.Twitter.Interfaces.ITwitterSearch;

public class TwitterSearch implements ITwitterSearch {

	Twitter twitter;
	AccessToken accessToken = null;
	
	String[] languages = {"english"};

	public TwitterSearch() {
		twitter = TwitterFactory.getSingleton();
		accessToken = this.loadAccessToken();
		if (accessToken == null) {
			twitter.setOAuthConsumer(
					Main.cm.getConfigValue("TwitterAccess", "Consumer Key"),
					Main.cm.getConfigValue("TwitterAccess", "Consumer Secret"));
			RequestToken requestToken = null;
			try {
				requestToken = twitter.getOAuthRequestToken();
			} catch (TwitterException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			while (null == accessToken) {
				System.out.println("Please copy PIN from following URL:");
				System.out.println(requestToken.getAuthorizationURL());
				System.out.print("[PIN]:");
				BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
				String pin = null;
				try {
					pin = br.readLine();
				} catch (IOException e) {
					e.printStackTrace();
				}
				try {
					if (pin.length() > 0) {
						accessToken = twitter.getOAuthAccessToken(requestToken,
								pin);
					} else {
						accessToken = twitter.getOAuthAccessToken();
					}
				} catch (TwitterException te) {
					if (401 == te.getStatusCode()) {
						System.out.println("Unable to get the access token.");
					} else {
						te.printStackTrace();
					}
				}
			}
		}
		storeAccessToken(accessToken.getUserId(), accessToken);
	}

	private void storeAccessToken(long useId, AccessToken accessToken) {
		if (Main.cm.configExists("AccessTokens"))
			new File("AccessTokens.xml").delete();
		Properties p = new Properties();
		p.put("token", accessToken.getToken());
		p.put("tokenSecret", accessToken.getTokenSecret());
		Main.cm.mkConfig("AccessTokens", p);
	}

	private AccessToken loadAccessToken() {
		if (Main.cm.configExists("AccessTokens")) {
			String token = Main.cm.getConfigValue("AccessTokens", "token");
			String tokenSecret = Main.cm.getConfigValue("AccessTokens",
					"tokenSecret");
			return new AccessToken(token, tokenSecret);
		}
		return null;
	}

	@Override
	public List<? extends ITweet> getTweets(ITwitterQuery query) {
		Query q = new Query();
		QueryResult qr = null;
		q.count(query.getCount());
		if (query.getLocation() != null) {
			System.out.println("Radius: " + query.getRadius());
			q.setGeoCode(new GeoLocation(query.getLocation().x, query.getLocation().y), query.getRadius(), query.getUnit());
		}
		if (!query.getKey().equals(""))
			q.setQuery(query.getKey());
		TwitterFactory factory = new TwitterFactory();
	    Twitter twitter = factory.getInstance();
		twitter.setOAuthConsumer(
				Main.cm.getConfigValue("TwitterAccess", "Consumer Key"),
				Main.cm.getConfigValue("TwitterAccess", "Consumer Secret"));
	    twitter.setOAuthAccessToken(accessToken);
		try {
			twitter.setOAuthAccessToken(accessToken);
			qr = twitter.search(q);
		} catch (TwitterException e) {
			e.printStackTrace();
			return null;
		}
		List<ITweet> results = new ArrayList<ITweet>();
		for (Status st : qr.getTweets()) {
			results.add(new Tweet(st));
		}		
		return results;
	}

}
