package eu.thesociety.DragonbornSR.Twitter;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.InvalidPropertiesFormatException;
import java.util.Properties;

public class ConfigurationManager {
	
	private HashMap<String,Configuration> configs = new HashMap<String,Configuration>();
	
	public ConfigurationManager() {
		
		//Create new, or read TwitterAccess.xml configuration.
		Properties p =  new Properties();
		p.setProperty("Consumer Key", "none");
		p.setProperty("Consumer Secret", "none");
		p.setProperty("Owner", "none");
		p.setProperty("Owner ID", "none");
		p.setProperty("Callback URL", "none");
		p.setProperty("App-only authentication", "https://api.twitter.com/oauth2/token");
		p.setProperty("Request token URL", "https://api.twitter.com/oauth/request_token");
		p.setProperty("Authorize URL", "https://api.twitter.com/oauth/authorize");
		p.setProperty("Access token URL", "https://api.twitter.com/oauth/access_token");
		configs.put("TwitterAccess", new Configuration("TwitterAccess.xml",p));
		configs.put("AccessTokens", new Configuration("AccessTokens.xml"));
	}
	
	public void mkConfig(String name, Properties p) {
		configs.put(name, new Configuration(name + ".xml",p));
	}
	
	public boolean configExists(String name) {
			if (new File(name+".xml").exists())
				return true;
		return false;
	}
	
	public String getConfigValue(String config, String key) {
		Object res = (configs.get(config).properties.get(key));
		if (res == null)
			return "";
		return res.toString();
	}
	
	private class Configuration {
		public String configFile = "default.xml";
		public Properties properties = new Properties();
		
		public Configuration(String filename, Properties defaultProperties) {
			this.configFile = filename;
			File f = new File(configFile);
			if (f.exists()) {
				loadConfig();
			} else {
				OutputStream os;
				try {
					os = new FileOutputStream(f);
					defaultProperties.storeToXML(os, "");
					properties = defaultProperties;
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		
		public Configuration(String string) {
			this.configFile = string;
		}

		public void saveConfig() {
			File f = new File(configFile);
			OutputStream os;
			try {
				os = new FileOutputStream(f);
				properties.storeToXML(os, "");
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		public void loadConfig() {
			File f = new File(configFile);
			InputStream in;
			try {
				in = new FileInputStream(f);
				properties.loadFromXML(in);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (InvalidPropertiesFormatException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
		}
	}
}