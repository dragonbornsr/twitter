package eu.thesociety.DragonbornSR.Twitter.Actions;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.List;
import java.util.Vector;

import twitter4j.JSONException;
import twitter4j.JSONObject;
import eu.thesociety.DragonbornSR.Twitter.Actions.LocationAction.Vector3f;
import eu.thesociety.DragonbornSR.Twitter.Interfaces.IAction;

public class LocationAction implements IAction {
	private String locationName = "";
	private Vector3f location;
	private Float radius = 0f;
	
	public LocationAction(List<String> args){
		if (args.size() > 1) {
			if (args.get(args.size() - 1).matches("[0-9]*")) {
				radius = Float.parseFloat((args.get(args.size() - 1)));
				args.remove(args.get(args.size() - 1));
			}
		}
		locationName = String.join("+", args);
		locationName = locationName.replace(" ", "+").replace("++", "+");
		JSONObject json = null;
		try {
			json = readJsonFromUrl("http://maps.google.com/maps/api/geocode/json?address=" + locationName + "&sensor=false");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	    location = getLocationFromString(json.toString());
	    
	    
	}
	
	public Vector3f getLocationFromString(String json) {
		String[] parts = json.split("}");
		float lat1 = 0, lon1 = 0;
		for (String p : parts) {
			if (p.contains("bounds")) {
				p = p.replace(",\"bounds\":{\"southwest\":{\"lng\":", "");
				p = p.replace("\"lat\":", "");
				String[] prt = p.split(",");
				lat1= Float.parseFloat(prt[1]);
				lon1= Float.parseFloat(prt[0]);
			}
		}
		for (String p : parts) {
			if (p.contains("location")) {
				p = p.replace(",\"location\":{\"lng\":", "");
				p = p.replace("\"lat\":", "");
				String[] prt = p.split(",");
				return new Vector3f(Float.parseFloat(prt[1]), Float.parseFloat(prt[0]), (this.radius == 0f)? 
						LocationAction.getDistance(lat1, lon1, Float.parseFloat(prt[1]), Float.parseFloat(prt[0])):radius);
			}
		}
		
		System.out.println("Did not find requested location!");
		return new Vector3f(0,0); // welcome to hell
	}
	
	private String readAll(Reader rd) throws IOException {
		StringBuilder sb = new StringBuilder();
	    int cp;
	    while ((cp = rd.read()) != -1) {
	    	sb.append((char) cp);
	    }
	    return sb.toString();
	}

	  public JSONObject readJsonFromUrl(String url) throws IOException, JSONException {
		  InputStream is = new URL(url).openStream();
		  try {
			  BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
			  String jsonText = readAll(rd);
			  JSONObject json = new JSONObject(jsonText);
			  return json;
		  } finally {
			  is.close();
		  }
	}
	public class Vector3f {
		public float x;
		public float y;
		public float r;
		public Vector3f(float x, float y) {
			this.x = x;
			this.y = y;
		}
		public Vector3f(float x, float y, float r) {
			this.x = x;
			this.y = y;
			this.r = r;
		}
		public String toString() {
			return this.x + "," + this.y + "," + this.r;
		}
		public Vector3f getValue() {
			return this;
		}
	}
	@Override
	public String toString() {
		return location.toString();
	}

	@Override
	public String getAsQueryString() {
		// TODO Auto-generated method stub
		return null;
	}
	
	public Vector3f getValue() {
		return this.location;
	}
	
	public static float getDistance(float lat1, float lng1, float lat2, float lng2 ) 
	{
	    double earthRadius = 3958.75;
	    double dLat = Math.toRadians(lat2-lat1);
	    double dLng = Math.toRadians(lng2-lng1);
	    double a = Math.sin(dLat/2) * Math.sin(dLat/2) +
	    Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) *
	    Math.sin(dLng/2) * Math.sin(dLng/2);
	    double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
	    double dist = earthRadius * c;

	    int meterConversion = 1609;

	    return new Float(dist * meterConversion).floatValue()/1000;
	}
}
