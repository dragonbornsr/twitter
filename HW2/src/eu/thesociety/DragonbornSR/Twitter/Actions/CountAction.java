package eu.thesociety.DragonbornSR.Twitter.Actions;

import java.util.List;

import eu.thesociety.DragonbornSR.Twitter.Interfaces.IAction;

public class CountAction implements IAction {
	
	public int count = 10;
	
	public CountAction(List<String> args) {
		for (String a : args) {
			if (a.matches("[0-9]*")) {
				count = Integer.parseInt(a);
				return;
			}
		}
	}
	
	@Override
	public String toString() {
		return Integer.toString(count);
	}

	@Override
	public String getAsQueryString() {
		// TODO Auto-generated method stub
		return null;
	}
	
	public int getValue() {
		return count;
	}
}
