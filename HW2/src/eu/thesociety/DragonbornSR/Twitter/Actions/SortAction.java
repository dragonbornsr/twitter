package eu.thesociety.DragonbornSR.Twitter.Actions;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import eu.thesociety.DragonbornSR.Twitter.Tweet;
import eu.thesociety.DragonbornSR.Twitter.Interfaces.IAction;
import eu.thesociety.DragonbornSR.Twitter.Interfaces.ITweet;

public class SortAction implements IAction {

	private String cmd = null;
	private String arg = null;
	private int order = 1;
	
	public SortAction(ArrayList<String> args) {
		if (args.size() > 0) {
			cmd = args.get(0);
			if (args.size() > 1) {
				arg = args.get(1);
				if (arg.equals("desc")) {
					order = -1;
				}
			}
		}
	}

	public List<? extends ITweet> sort(List<? extends ITweet> list) {
		if (cmd == null)
			return list;
		if (cmd.equals("author")) {
			Collections.sort(list, byAuthor);
		}
		if (cmd.equals("content")) {
			Collections.sort(list, byContent);
		}
		if (cmd.equals("date")) {
			Collections.sort(list, byDate);
		}
		return list;
	}

	@Override
	public String getAsQueryString() {
		return null;
	}
	
	Comparator<ITweet> byAuthor = new Comparator<ITweet>() {
	    public int compare(ITweet left, ITweet right) {
	    	return order*(((Tweet)left).username.toLowerCase().compareTo(((Tweet)right).username.toLowerCase()));
	    }
	};
	
	Comparator<ITweet> byDate = new Comparator<ITweet>() {
	    public int compare(ITweet left, ITweet right) {
	    	return order*((((Tweet)left).date).compareTo((((Tweet)right).date)));
	    }
	};
	
	Comparator<ITweet> byContent = new Comparator<ITweet>() {
	    public int compare(ITweet left, ITweet right) {
	    	return order*(((Tweet)left).tweetText.toLowerCase().compareTo(((Tweet)right).tweetText.toLowerCase()));
	    }
	};

}
