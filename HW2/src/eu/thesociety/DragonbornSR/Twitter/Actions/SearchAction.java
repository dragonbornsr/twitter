package eu.thesociety.DragonbornSR.Twitter.Actions;

import java.util.ArrayList;

import eu.thesociety.DragonbornSR.Twitter.Interfaces.IAction;

public class SearchAction implements IAction {
	private String keyword;
	public SearchAction(ArrayList<String> args) {
		keyword = String.join(" ", args);
	}

	@Override
	public String getAsQueryString() {
		return keyword;
	}

}
