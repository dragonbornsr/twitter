package eu.thesociety.DragonbornSR.Twitter.Interfaces;

public interface ILocationSearch {

	ITwitterQuery getQueryFromLocation(String location);

}
