package eu.thesociety.DragonbornSR.Twitter.Interfaces;

import twitter4j.Query;
import eu.thesociety.DragonbornSR.Twitter.Actions.CountAction;
import eu.thesociety.DragonbornSR.Twitter.Actions.LocationAction;
import eu.thesociety.DragonbornSR.Twitter.Actions.LocationAction.Vector3f;

public interface ITwitterQuery {
	public int getCount();

	public void setCount(CountAction ca);

	public Vector3f getLocation();

	public void setLocation(LocationAction la);
	
	public void setRadius(LocationAction la);
	
	public double getRadius();

	public void setRadius(double radius);
	
	public Query.Unit getUnit();

	public void setUnit(Query.Unit u);
	
	public String getKey();

	public void setKey(String k);

	void setLocation(Vector3f vector3f);

	void setCount(int i);
}
