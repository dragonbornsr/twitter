package eu.thesociety.DragonbornSR.Twitter.Interfaces;

import java.util.List;

public interface ITwitterSearch {

	List<? extends ITweet> getTweets(ITwitterQuery query);

}
