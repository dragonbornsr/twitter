package eu.thesociety.DragonbornSR.Twitter.Interfaces;


/**
 * 
 * @author ROG
 *
 */
public interface IAction {
	public String toString();
	public String getAsQueryString();

}
