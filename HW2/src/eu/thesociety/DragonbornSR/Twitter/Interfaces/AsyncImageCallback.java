package eu.thesociety.DragonbornSR.Twitter.Interfaces;

import java.awt.image.BufferedImage;

public interface AsyncImageCallback {

	void onImageReceived(String url, BufferedImage bi);
    
}
