package eu.thesociety.DragonbornSR.Twitter;

import java.util.Date;

import twitter4j.GeoLocation;
import twitter4j.Status;
import eu.thesociety.DragonbornSR.Twitter.Interfaces.ITweet;

public class Tweet implements ITweet {
	
	public Status rawData;
	public String tweetText;
	public String username;
	public Date date;
	public int retweetCount;
	public long[] contributors;
	public int favouriteCount;
	public GeoLocation location;
	public String imageURL;
	public Tweet(Status st) {
		rawData = st;
		username = st.getUser().getScreenName();
		tweetText = st.getText();
		date = st.getCreatedAt();
		retweetCount = st.getRetweetCount();
		contributors = st.getContributors();
		favouriteCount = st.getFavoriteCount();
		location = st.getGeoLocation();
		imageURL = st.getUser().getProfileImageURL();
	}
	
	
	public void printTweet() {
		System.out.println("***************************************************************************************************************");
		System.out.println(username + " Retweets: " + this.retweetCount + " Favourites: " + this.favouriteCount);
		System.out.println();
		System.out.println(tweetText);
	}
}
