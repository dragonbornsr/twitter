package eu.thesociety.DragonbornSR.Twitter;

import java.util.ArrayList;
import java.util.List;

import eu.thesociety.DragonbornSR.Twitter.Actions.CountAction;
import eu.thesociety.DragonbornSR.Twitter.Actions.LocationAction;
import eu.thesociety.DragonbornSR.Twitter.Actions.SearchAction;
import eu.thesociety.DragonbornSR.Twitter.Actions.SortAction;
import eu.thesociety.DragonbornSR.Twitter.Actions.WriteAction;
import eu.thesociety.DragonbornSR.Twitter.Interfaces.IAction;
import eu.thesociety.DragonbornSR.Twitter.Interfaces.ICache;
import eu.thesociety.DragonbornSR.Twitter.Interfaces.ILocationSearch;
import eu.thesociety.DragonbornSR.Twitter.Interfaces.ITweet;
import eu.thesociety.DragonbornSR.Twitter.Interfaces.ITwitterApplication;
import eu.thesociety.DragonbornSR.Twitter.Interfaces.ITwitterSearch;

public class TwitterApplication implements ITwitterApplication {

	private TwitterQuery currentQuery = new TwitterQuery();
	TwitterSearch search;
	public TwitterApplication() {
		search = new TwitterSearch();
	}
	
	@Override
	public List<IAction> getActionsFromInput(String action) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<IAction> getActionsFromArguments(String[] args) {
		List<IAction> loa = new ArrayList<IAction>();
		currentQuery = new TwitterQuery();
		String command = "";
		ArrayList<String> commandArguments = new ArrayList<String>();
		for (String c : args) {
			if (c.startsWith("-")) { // -parameter arg arg arg -parameter2 arg arg
				if (!command.equals("")) {
					loa.add(addCommand(command, commandArguments));
					commandArguments.clear();
				}
				command = c;
			} else {
				commandArguments.add(c);
			}
		}
		loa.add(addCommand(command, commandArguments));
		return loa;
	}
	
	public IAction addCommand(String cmd, ArrayList<String> args) {
		if (cmd.equals("-location")) {
			return new LocationAction(args);
		}
		else if (cmd.equals("-c")) {
			return new CountAction(args);
		}
		else if (cmd.equals("-w")) {
			return new WriteAction(args);
		}
		else if (cmd.equals("-sort")) {
			return new SortAction(args);
		}
		else if (cmd.equals("-search") || cmd.equals("-query")) {
			return new SearchAction(args);
		}
		return null;
	}

	@Override
	public void executeAction(IAction action) {
		if (action instanceof SearchAction) {
			currentQuery.setKey(((SearchAction)action).getAsQueryString());
		}
		if (action instanceof LocationAction) {
			currentQuery.setLocation(((LocationAction)action).getValue());
			currentQuery.setRadius(((LocationAction)action).getValue().r);
		}
		if (action instanceof CountAction) {
			currentQuery.setCount(((CountAction)action).getValue());
		}
		if (action instanceof SortAction) {
			currentQuery.setSorting((SortAction)action);
		}
	}

	@Override
	public void setLocationSearch(ILocationSearch locationSearch) {
		// TODO Auto-generated method stub

	}

	@Override
	public ILocationSearch getLocationSearch() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setTwitterSearch(ITwitterSearch twitterSearch) {
		// TODO Auto-generated method stub

	}

	@Override
	public ITwitterSearch getTwitterSearch() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setCache(ICache cache) {
		// TODO Auto-generated method stub

	}

	@Override
	public ICache getCache() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setTweets(List<? extends ITweet> tweets) {

	}

	@Override
	public List<? extends ITweet> getTweets() {
		TwitterSearch search = new TwitterSearch();
		if (currentQuery.getSorting() != null) {
			return currentQuery.getSorting().sort(search.getTweets(this.currentQuery));
		}
		return search.getTweets(this.currentQuery);
	}
}
