/*
	Copyright (c) 2014, Coffea Games
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	1. Redistributions of source code must retain the above copyright
	   notice, this list of conditions and the following disclaimer.
	2. Redistributions in binary form must reproduce the above copyright
	   notice, this list of conditions and the following disclaimer in the
	   documentation and/or other materials provided with the distribution.
	3. All advertising materials mentioning features or use of this software
	   must display the following acknowledgement:
	   This product includes software developed by the Coffea Games.
	4. Neither the name of the Coffea Games nor the
	   names of its contributors may be used to endorse or promote products
	   derived from this software without specific prior written permission.
	
	THIS SOFTWARE IS PROVIDED BY COFFEA GAMES ''AS IS'' AND ANY
	EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
	WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL COFFEA GAMES BE LIABLE FOR ANY
	DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
	(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
	LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
	ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
	(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
package com.coffeagame.Engine.scene;

import org.lwjgl.opengl.GL11;

import com.coffeagame.Engine.Variables.GLFloat;

/**
 * Object that describes material of an object.
 * @author ROG
 *
 */
public class Material {
	
	/*
	 * float[] mat_ambient ={0.25f, 0.148f, 0.06475f, 1.0f  };
		float[] mat_diffuse ={0.4f, 0.2368f, 0.1036f, 1.0f };
		float[] mat_specular ={0.774597f, 0.458561f, 0.200621f, 1.0f };
		float shine =76.8f ; 
	 * 
	 */
	
	/**
	 * Material Specular represents materials feature
	 * to reflect different colors when light falls on it.
	 */
	public GLFloat Material_Ambient = new GLFloat( 0.25f, 0.148f, 0.06475f, 1.0f );
	/**
	 * Material Specular represents materials feature
	 * to reflect different colors when light falls on it.
	 */
	public GLFloat Material_Diffuse = new GLFloat( 0.4f, 0.2368f, 0.1036f, 1.0f );
	/**
	 * Material Specular represents materials feature
	 * to reflect different colors when light falls on it.
	 */
	public GLFloat Material_Specular = new GLFloat( 0.774597f, 0.458561f, 0.200621f, 1.0f );
	
	/**
	 * Materials feature to shine.
	 * Not reflective - 0f;
	 * Very reflective - 100f (mirror);
	 */
	public GLFloat Material_Shininess = new GLFloat(50f, 50f, 50f, 50f);
	
	/**
	 * ApplyEffects apply material features to GL object. To be called
	 * in Draw loop only!
	 */
	public void applyEffecs() {
		   GL11.glMaterial(GL11.GL_FRONT_AND_BACK, GL11.GL_AMBIENT, Material_Ambient.getValue());
		   GL11.glMaterial(GL11.GL_FRONT_AND_BACK, GL11.GL_DIFFUSE, Material_Diffuse.getValue());
		   GL11.glMaterial(GL11.GL_FRONT_AND_BACK, GL11.GL_SPECULAR, Material_Specular.getValue());
		   GL11.glMaterial(GL11.GL_FRONT_AND_BACK, GL11.GL_SHININESS, Material_Shininess.getValue());
		   
	}
	/**
	 * Set material Specular properties.
	 * @param r
	 * @param g
	 * @param b
	 * @param a
	 */
	public void setSpecular(float r, float g, float b, float a) {
		Material_Specular = new GLFloat(r,g,b,a);
	}
	
	/**
	 * Set material Shininess.
	 * @param r
	 * @param g
	 * @param b
	 * @param a
	 */
	public void setShininess(float r, float g, float b, float a) {
		Material_Shininess = new GLFloat(r,g,b,a);
	}
	
	public void setAmbient(float r, float g, float b, float a) {
		Material_Ambient = new GLFloat(r,g,b,a);
	}
	
	public void setDiffuse(float r, float g, float b, float a) {
		Material_Diffuse = new GLFloat(r,g,b,a);
	}
	
	
	
	
}
