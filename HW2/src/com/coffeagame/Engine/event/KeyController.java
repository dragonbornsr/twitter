/*
	Copyright (c) 2014, Coffea Games
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	1. Redistributions of source code must retain the above copyright
	   notice, this list of conditions and the following disclaimer.
	2. Redistributions in binary form must reproduce the above copyright
	   notice, this list of conditions and the following disclaimer in the
	   documentation and/or other materials provided with the distribution.
	3. All advertising materials mentioning features or use of this software
	   must display the following acknowledgement:
	   This product includes software developed by the Coffea Games.
	4. Neither the name of the Coffea Games nor the
	   names of its contributors may be used to endorse or promote products
	   derived from this software without specific prior written permission.
	
	THIS SOFTWARE IS PROVIDED BY COFFEA GAMES ''AS IS'' AND ANY
	EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
	WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL COFFEA GAMES BE LIABLE FOR ANY
	DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
	(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
	LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
	ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
	(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
package com.coffeagame.Engine.event;

import java.util.ArrayList;
import java.util.List;

import org.lwjgl.input.Keyboard;

import com.coffeagame.Engine.Objects.CoreObject;
import com.coffeagame.Engine.event.types.KeyEvent;

/**
 * Controller that forwards IKeyListeners keyboard events.
 * @author ROG
 *
 */
public class KeyController extends CoreObject {

	private static List<Object> listOfKeyEventListeners = new ArrayList<Object>();
	public static List<Object> listOfDeleted = new ArrayList<Object>();

	@Override
	public void update(float delta) {
		/*
		 * Avoid Cocurrent Exceptions on removing while in the loop!
		 */
		for (Object o : KeyController.getListOfKeyEventListeners())
			if (((com.coffeagame.Engine.event.interfaces.IKeyEvent) o)
					.isDeleted()) {
				KeyController.listOfDeleted.add(o);
			}
		for (Object o : KeyController.listOfDeleted)
			if (((com.coffeagame.Engine.event.interfaces.IKeyEvent) o)
					.isDeleted()) {
				KeyController.getListOfKeyEventListeners().remove(o);
			}
		KeyController.listOfDeleted.clear();

		KeyEvent key;
		while (Keyboard.next()) {
			key = new KeyEvent(Keyboard.getEventKey());
			if (Keyboard.getEventKeyState()) {
				forwardKeyPressed(key);
			} else {
				forwardKeyReleased(key);
			}
		}
	}

	protected void forwardKeyPressed(KeyEvent key) {
		for (Object o : getListOfKeyEventListeners())
			((com.coffeagame.Engine.event.interfaces.IKeyEvent) o)
					.keyPressed(key);
	}

	protected void forwardKeyReleased(KeyEvent key) {
		for (Object o : getListOfKeyEventListeners())
			((com.coffeagame.Engine.event.interfaces.IKeyEvent) o)
					.keyReleased(key);
	}

	public static List<Object> getListOfKeyEventListeners() {
		return listOfKeyEventListeners;
	}
}
