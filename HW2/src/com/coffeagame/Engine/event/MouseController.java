/*
	Copyright (c) 2014, Coffea Games
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	1. Redistributions of source code must retain the above copyright
	   notice, this list of conditions and the following disclaimer.
	2. Redistributions in binary form must reproduce the above copyright
	   notice, this list of conditions and the following disclaimer in the
	   documentation and/or other materials provided with the distribution.
	3. All advertising materials mentioning features or use of this software
	   must display the following acknowledgement:
	   This product includes software developed by the Coffea Games.
	4. Neither the name of the Coffea Games nor the
	   names of its contributors may be used to endorse or promote products
	   derived from this software without specific prior written permission.
	
	THIS SOFTWARE IS PROVIDED BY COFFEA GAMES ''AS IS'' AND ANY
	EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
	WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL COFFEA GAMES BE LIABLE FOR ANY
	DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
	(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
	LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
	ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
	(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
package com.coffeagame.Engine.event;

import java.util.ArrayList;
import java.util.List;

import org.lwjgl.input.Mouse;

import com.coffeagame.Engine.Objects.CoreObject;
import com.coffeagame.Engine.Objects.Cursor;
import com.coffeagame.Engine.Objects.DrawableGameObject;
import com.coffeagame.Engine.Objects.GameObject;
import com.coffeagame.Engine.annotations.Placeholder;
import com.coffeagame.Engine.event.interfaces.IMouseEvent;
import com.coffeagame.Engine.event.types.MouseEvent;

/**
 * Controller that forwards IMouseEvent listeners mouse events.
 * @author ROG
 *
 */
public class MouseController extends CoreObject {
	protected static List<GameObject> listOfMouseEventListeners = new ArrayList<GameObject>();
	public static List<GameObject> listOfDeleted = new ArrayList<GameObject>();

	@Override
	public void update(float delta) {
		if (Cursor.cursorMode > 0)
			return;
		/*
		 * Avoid Concurrent Exceptions on removing while in the loop!
		 */
		for (GameObject o : MouseController.getListOfMouseEventListeners())
			if (((com.coffeagame.Engine.event.interfaces.IMouseEvent) o)
					.isDeleted()) {
				MouseController.listOfDeleted.add(o);
			}
		for (GameObject o : MouseController.listOfDeleted)
			if (((com.coffeagame.Engine.event.interfaces.IMouseEvent) o)
					.isDeleted()) {
				MouseController.getListOfMouseEventListeners().remove(o);
			}
		MouseController.listOfDeleted.clear();
		MouseEvent e;
		while (Mouse.next()) {
			if (Mouse.getEventButton() == -1)
				continue;
			e = new MouseEvent(Cursor.x, Cursor.y, Mouse.getEventButton());
			if (Mouse.getEventButtonState()) {
				
				forwardMousePressed(e);
			} else {
				forwardMouseReleased(e);
			}
		}

		boolean foundFirst = false;
		for (GameObject o : getListOfMouseEventListeners()) {
			if (((IMouseEvent)o).inBounds(new MouseEvent(Cursor.x, Cursor.y)) && (!foundFirst) && (o.enabled)) {
				if (o instanceof DrawableGameObject) {
					if (((DrawableGameObject)o).visible) {
						foundFirst = ((IMouseEvent) o).setHover(true);
					}
				} else
					foundFirst = ((IMouseEvent) o).setHover(true);
			} else {
				((IMouseEvent) o).setHover(false);
			}
		}
	}

	protected void forwardMousePressed(MouseEvent e) {
		for (GameObject o : getListOfMouseEventListeners())
			if (((IMouseEvent) o).inBounds(e) && (o.enabled)) {
				if (o instanceof DrawableGameObject) {
					if (((DrawableGameObject)o).visible) {
						((com.coffeagame.Engine.event.interfaces.IMouseEvent) o)
						.mousePressed(e);
						return;
					}
				} else {
				((com.coffeagame.Engine.event.interfaces.IMouseEvent) o)
						.mousePressed(e);
					return;
				}
			}
	}

	protected void forwardMouseReleased(MouseEvent e) {
		for (Object o : getListOfMouseEventListeners())
			if (((IMouseEvent) o).inBounds(e) && (((GameObject) o).enabled)) {
				if (o instanceof DrawableGameObject) {
					if (((DrawableGameObject)o).visible) {
						((com.coffeagame.Engine.event.interfaces.IMouseEvent) o)
						.mouseReleased(e);
						return;
					}
				} else {
				((com.coffeagame.Engine.event.interfaces.IMouseEvent) o)
						.mouseReleased(e);
					return;
				}
			}
	}
	
	/**
	 * Register a GameObject to be a IMouseEvent Listener.  Make sure that
	 * GameObject implements IMouseEvent.
	 * @param listener
	 * @param priority
	 */
	@Placeholder
	public void registerMouseEventListener(GameObject listener, int priority) {
		boolean added = false;
		if (listOfMouseEventListeners.isEmpty()) {
			listOfMouseEventListeners.add(0, listener);
			added = true;
		} else {
			for (GameObject mel : listOfMouseEventListeners) {
				if (mel.priority <= priority) {
					int index = listOfMouseEventListeners.indexOf(mel);
					listOfMouseEventListeners.add(index, listener);
					added = true;
					break;
				}
			}
			if (!added) {
				listOfMouseEventListeners.add(listener);
			}
		}
	}
	
	/**
	 * Change priority of a button. Use this when bringing a GUI/Button forward relative to any other.
	 * @param listener
	 * @param priority
	 */
	public void changePriority(GameObject listener, int priority) {
		listOfMouseEventListeners.remove(listener);
		registerMouseEventListener(listener, priority);
	}

	public static List<GameObject> getListOfMouseEventListeners() {
		return listOfMouseEventListeners;
	}
}
