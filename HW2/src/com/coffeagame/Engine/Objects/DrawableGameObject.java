/*
	Copyright (c) 2014, Coffea Games
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	1. Redistributions of source code must retain the above copyright
	   notice, this list of conditions and the following disclaimer.
	2. Redistributions in binary form must reproduce the above copyright
	   notice, this list of conditions and the following disclaimer in the
	   documentation and/or other materials provided with the distribution.
	3. All advertising materials mentioning features or use of this software
	   must display the following acknowledgement:
	   This product includes software developed by the Coffea Games.
	4. Neither the name of the Coffea Games nor the
	   names of its contributors may be used to endorse or promote products
	   derived from this software without specific prior written permission.
	
	THIS SOFTWARE IS PROVIDED BY COFFEA GAMES ''AS IS'' AND ANY
	EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
	WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL COFFEA GAMES BE LIABLE FOR ANY
	DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
	(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
	LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
	ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
	(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
package com.coffeagame.Engine.Objects;

import com.coffeagame.Engine.Game;
import com.coffeagame.Engine.event.KeyController;
import com.coffeagame.Engine.event.MouseController;
import com.coffeagame.Engine.event.interfaces.IKeyEvent;
import com.coffeagame.Engine.event.interfaces.IMouseEvent;
import com.coffeagame.Engine.scene.Material;

/**
 * Object that can be drawn on the screen Could be anything: HUD, menu, dragon,
 * planet, spider or human.
 * 
 * @since 995995e
 */
public class DrawableGameObject extends GameObject {

	/**
	 * Object name
	 */
	public String unlocalizedName = this.getClass().getName();
	/**
	 * Set if object will be drawn on the screen.
	 */
	public boolean visible = true;
	
	/**
	 * 
	 */
	protected Material material = new Material();

	/**
	 * Draw with OpenGL NB! Please do not use this method to update any
	 * parameters anymore. Just plain drawing, because this might be
	 * multi-threaded and might cause synchronization problem.
	 */
	public void draw(float delta) {
	}

	/**
	 * Call this function to delete object next tick safely. This will call out
	 * finalize method that will save all nessecary data etc.
	 * 
	 */
	@Override
	public void unregister() {
		this.visible = false; // prevent draw on same tick!
		this.enabled = false;
		this.delete = true;
		finalize();
		Game.listOfDeletedDrawableGameObjects.add(this);
	}

	/**
	 * register Object to render/update loop with certain priority. Priority 0
	 * will be drawn first, but will stay under other layers!
	 */
	public void register(int priority) {
		this.priority = priority;
		int index = 0;
		boolean added = false;
		if (Game.listOfDrawableGameObjects.isEmpty()) {
			Game.listOfDrawableGameObjects.add(0, this);
			added = true;
		} else {
			for (DrawableGameObject dgo : Game.listOfDrawableGameObjects) {
				if (dgo.priority >= priority) {
					index = Game.listOfDrawableGameObjects.indexOf(dgo);
					Game.listOfDrawableGameObjects.add(index, this);
					added = true;
					break;
				}
			}
			if (!added) {
				Game.listOfDrawableGameObjects.add(this);
			}
		}
		/*
		 * Register all Event Handlers.
		 */
		if (this instanceof IKeyEvent) {
			KeyController.getListOfKeyEventListeners().add(this);
		}
		if (this instanceof IMouseEvent) {
			Game.mouseController.registerMouseEventListener(this, this.priority);
		}
	}
}
