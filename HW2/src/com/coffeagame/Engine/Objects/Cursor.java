/*
	Copyright (c) 2014, Coffea Games
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	1. Redistributions of source code must retain the above copyright
	   notice, this list of conditions and the following disclaimer.
	2. Redistributions in binary form must reproduce the above copyright
	   notice, this list of conditions and the following disclaimer in the
	   documentation and/or other materials provided with the distribution.
	3. All advertising materials mentioning features or use of this software
	   must display the following acknowledgement:
	   This product includes software developed by the Coffea Games.
	4. Neither the name of the Coffea Games nor the
	   names of its contributors may be used to endorse or promote products
	   derived from this software without specific prior written permission.
	
	THIS SOFTWARE IS PROVIDED BY COFFEA GAMES ''AS IS'' AND ANY
	EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
	WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL COFFEA GAMES BE LIABLE FOR ANY
	DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
	(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
	LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
	ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
	(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
package com.coffeagame.Engine.Objects;

import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.GL11;
import org.newdawn.slick.Color;
import org.newdawn.slick.opengl.Texture;
import org.newdawn.slick.opengl.TextureImpl;

import com.coffeagame.Engine.event.interfaces.IKeyEvent;
import com.coffeagame.Engine.event.types.KeyEvent;
import com.coffeagame.Engine.media.ResourceManager;
import com.coffeagame.Engine.scene.GLViewPort;

public class Cursor extends DrawableGameObject implements IKeyEvent {

	public static float x = (float) Display.getWidth() / 2;
	public static float y = (float) Display.getHeight() / 2;
	public static int cursorMode = 0; // 0 = Game cursor, 1 = No cursor, 2 =
										// Desctop Cursor

	public float size = 32;
	protected Texture cursor = null;

	public Cursor() {
		register(Integer.MAX_VALUE);
		cursor = ResourceManager.getTexture("Cursor");
	}

	@Override
	public void update(float delta) {

	}
	
	/**
	 * Method to update cursor position.
	 * @param mouseDX
	 * @param mouseDY
	 */
	public static void updateCursor(float mouseDX, float mouseDY) {
		if (cursorMode == 0) {
			Cursor.x += mouseDX;
			Cursor.y -= mouseDY;

			if (Cursor.x > Display.getWidth())
				Cursor.x = Display.getWidth();
			if (Cursor.x < 0)
				Cursor.x = 0;

			if (Cursor.y > Display.getHeight())
				Cursor.y = Display.getHeight();
			if (Cursor.y < 0)
				Cursor.y = 0;
		}
	}

	@Override
	public void draw(float delta) {
		GLViewPort.startGLMode2D();
			TextureImpl.bindNone();
			Color.white.bind();
			cursor.bind();
			GL11.glPushMatrix();
			GL11.glEnable(GL11.GL_BLEND);
			GL11.glBegin(GL11.GL_QUADS);
				GL11.glTexCoord2f(0, 0);
				GL11.glVertex2f(x, y);
				GL11.glTexCoord2f(1, 0);
				GL11.glVertex2f(x + size, y);
				GL11.glTexCoord2f(1, 1);
				GL11.glVertex2f(x + size, y + size);
				GL11.glTexCoord2f(0, 1);
				GL11.glVertex2f(x, y + size);
			GL11.glEnd();
			GL11.glDisable(GL11.GL_BLEND);
			GL11.glPopMatrix();
			TextureImpl.bindNone();
			
		GLViewPort.endGLMode2D();
	}

	@Override
	public boolean isEnabled() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isDeleted() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void keyPressed(KeyEvent key) {
		if (key.getKey() == Keyboard.KEY_F3) {
			cursorMode += 1;
			switch (cursorMode) {
			case 0:
				this.visible = true;
				Mouse.setGrabbed(true);
				break;
			case 1:
				this.visible = false;
				Mouse.setGrabbed(true);
				break;
			case 2:
				this.visible = false;
				Mouse.setGrabbed(false);
				break;
			default:
				cursorMode = 0;
				this.visible = true;
				Mouse.setGrabbed(true);
				break;
			}
		}
		
	}

	@Override
	public void keyReleased(KeyEvent key) {
		// TODO Auto-generated method stub
		
	}
}
