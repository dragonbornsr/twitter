/*
	Copyright (c) 2014, Coffea Games
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	1. Redistributions of source code must retain the above copyright
	   notice, this list of conditions and the following disclaimer.
	2. Redistributions in binary form must reproduce the above copyright
	   notice, this list of conditions and the following disclaimer in the
	   documentation and/or other materials provided with the distribution.
	3. All advertising materials mentioning features or use of this software
	   must display the following acknowledgement:
	   This product includes software developed by the Coffea Games.
	4. Neither the name of the Coffea Games nor the
	   names of its contributors may be used to endorse or promote products
	   derived from this software without specific prior written permission.
	
	THIS SOFTWARE IS PROVIDED BY COFFEA GAMES ''AS IS'' AND ANY
	EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
	WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL COFFEA GAMES BE LIABLE FOR ANY
	DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
	(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
	LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
	ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
	(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
package com.coffeagame.Engine.Objects;

import com.coffeagame.Engine.Game;
import com.coffeagame.Engine.event.interfaces.IKeyEvent;
import com.coffeagame.Engine.event.interfaces.IMouseEvent;

/**
 * Object that can be updated, but will not be drawn on screen Used to create
 * handlers for DrawableGameObjects (to collect and manage them) or anything
 * else that needs to be calculated every tick, or once in a while.
 * 
 * @since 995995e
 */
public class GameObject {

	/**
	 * Set if object will be updated
	 */
	public boolean enabled = true;

	/**
	 * Set if object will be deleted on next update. Do not edit this boolean
	 * unless you do not want to save the data. Use delete() method instead to
	 * also call finalize()
	 */
	public boolean delete = false;

	/**
	 * Set priority in order to define if it must be updated before or after
	 * something. Priority 0 is lowest possible. Will be drawn first, more
	 * important that will stay on top of screen will be drawn later!
	 */
	public int priority = 0;

	/**
	 * Update object parameters, handle something, etc. DO NOT RENDER
	 * ANYTHING!!!
	 */
	public void update(float delta) {
	}

	/**
	 * Function called to prepare object to be deleted. Save information to
	 * file, log anything if needed.
	 */
	public void finalize() {
	}

	public void unregister() {
		this.delete = true;
		finalize();
	}

	/**
	 * register Object to render/update loop with certain priority. Priority 0
	 * will be drawn first, but will stay under other layers!
	 */
	@SuppressWarnings("static-access")
	public void register(int priority) {
		this.priority = priority;
		int index = 0;
		boolean added = false;
		if (Game.listOfGameObjects.isEmpty()) {
			Game.listOfGameObjects.add(0, this);
			added = true;
		} else {
			for (GameObject dgo : Game.listOfGameObjects) {
				if (dgo.priority >= priority) {
					index = Game.listOfGameObjects.indexOf(dgo);
					Game.listOfGameObjects.add(index, this);
					added = true;
					break;
				}
			}
			if (!added) {
				Game.listOfGameObjects.add(index + 1, this);
			}
		}
		/*
		 * Register all Event Handlers.
		 */
		if (this instanceof IKeyEvent) {
			Game.keyController.getListOfKeyEventListeners().add(this);
		}
		if (this instanceof IMouseEvent) {
			Game.mouseController.registerMouseEventListener(this, this.priority);
		}
	}

}
