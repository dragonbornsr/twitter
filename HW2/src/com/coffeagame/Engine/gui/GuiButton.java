/*
	Copyright (c) 2014, Coffea Games
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	1. Redistributions of source code must retain the above copyright
	   notice, this list of conditions and the following disclaimer.
	2. Redistributions in binary form must reproduce the above copyright
	   notice, this list of conditions and the following disclaimer in the
	   documentation and/or other materials provided with the distribution.
	3. All advertising materials mentioning features or use of this software
	   must display the following acknowledgement:
	   This product includes software developed by the Coffea Games.
	4. Neither the name of the Coffea Games nor the
	   names of its contributors may be used to endorse or promote products
	   derived from this software without specific prior written permission.
	
	THIS SOFTWARE IS PROVIDED BY COFFEA GAMES ''AS IS'' AND ANY
	EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
	WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL COFFEA GAMES BE LIABLE FOR ANY
	DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
	(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
	LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
	ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
	(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
package com.coffeagame.Engine.gui;

import org.lwjgl.opengl.GL11;
import org.lwjgl.util.vector.Vector2f;
import org.newdawn.slick.Color;
import org.newdawn.slick.opengl.Texture;
import org.newdawn.slick.opengl.TextureImpl;

import com.coffeagame.Engine.Objects.DrawableGameObject;
import com.coffeagame.Engine.Utility.TextureTools;
import com.coffeagame.Engine.event.interfaces.IMouseEvent;
import com.coffeagame.Engine.event.types.MouseEvent;
import com.coffeagame.Engine.scene.GLViewPort;

public class GuiButton extends DrawableGameObject implements IMouseEvent {

	protected Gui parent;
	protected int posX = 0, posY = 0;
	protected int width = 0, height = 0;
	protected int texOffX = 0, texOffY = 0;
	protected Texture texture = null;
	protected boolean htm = false;

	/**
	 * parameters that describe texture corners.
	 */
	protected Vector2f c1 = new Vector2f(0, 0), // Corner 1 is upper left,
					 c2 = new Vector2f(0, 0), // following ones are clockwise.
					 c3 = new Vector2f(0, 0),
					 c4 = new Vector2f(0, 0);

	public int buttonID = 0;
	public boolean hover = false;

	/**
	 * Initialize gui button. REMEMBER, please also set size and position for the button!!!
	 * @param parent
	 * @param buttonID
	 */
	public GuiButton(Gui parent, int buttonID) {
		this.parent = parent;
		parent.listOfGuiButtons.add(this);
		register(parent.priority + 1);
		this.buttonID = buttonID;
	}

	@Override
	public void update(float delta) {
		if (!this.parent.visible && this.visible) {
			this.visible = false;
		}
		if (this.parent.visible && !this.visible) {
			this.visible = true;
		}

		if (this.hover) {
			if (!htm) {
				texOffX += width;
				updateTextureCorners();
				texOffX -= width;
				htm = true;
			}
		} else if (htm) {
			updateTextureCorners();
			htm = false;
		}
	}

	@Override
	public void draw(float delta) {
		if (parent.isVisible) {
			GLViewPort.startGLMode2D();
			GL11.glPushMatrix();
			texture.bind();
			Color.white.bind();
			GL11.glEnable(GL11.GL_BLEND);
			GL11.glBegin(GL11.GL_QUADS);
			// GL11.glColor4f(0.6f, 0.1f, 0.1f, 1.0f);
			GL11.glTexCoord2f(c1.x, c1.y);
			GL11.glVertex2f(posX + parent.posX, posY + parent.posY);
			GL11.glTexCoord2f(c2.x, c2.y);
			GL11.glVertex2f(posX + parent.posX + width, posY + parent.posY);
			GL11.glTexCoord2f(c3.x, c3.y);
			GL11.glVertex2f(posX + parent.posX + width, posY + height + parent.posY);
			GL11.glTexCoord2f(c4.x, c4.y);
			GL11.glVertex2f(posX + parent.posX, posY + height + parent.posY);
			GL11.glEnd();
			GL11.glPopMatrix();
			TextureImpl.bindNone();
			GLViewPort.endGLMode2D();
		}
	}

	protected void setPosition(int x, int y) {
		this.posX = x;
		this.posY = y;
	}

	protected void setSize(int width, int height) {
		this.width = width;
		this.height = height;
	}

	protected void setOffset(int x, int y) {
		this.texOffX = x;
		this.texOffY = y;
	}
	
	/**
	 * Set button size and position relative to its parent GUI.
	 * @param x
	 * @param y
	 * @param width
	 * @param height
	 * @param offX
	 * @param offY
	 */
	public void setSizeAndPosition(int x, int y, int width, int height,
			int offX, int offY) {
		this.setPosition(x, y);
		this.setSize(width, height);
		this.setOffset(offX, offY);
		this.updateTextureCorners();
	}

	@Override
	public boolean isEnabled() {
		return parent.isVisible;
	}

	/**
	 * Returns if point on monitor is in bounds of the button!
	 * @param e
	 * @return
	 */
	@Override
	public boolean inBounds(MouseEvent e) {
		if ((posX + parent.posX <= e.posX) && (posY + parent.posY <= e.posY)) // Left
																				// lower?
			if ((posX + parent.posX + width >= e.posX)
					&& (posY + height + parent.posY >= e.posY))
				return true;
		return false;
	}

	@Override
	public boolean isDeleted() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void mousePressed(MouseEvent e) {
		if (this.visible && parent.isVisible && inBounds(e)) {
			parent.buttonPressed(this.buttonID);
		}
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseMoved(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	public Texture getTexture() {
		return texture;
	}

	public void setTexture(Texture texture) {
		this.texture = texture;
	}
	
	/**
	 * Update texture corner positions to allow texture off-setting.
	 */
	public void updateTextureCorners() {
		c1 = TextureTools.getTextureCorner(this.texture, texOffX, texOffY);
		c2 = TextureTools.getTextureCorner(this.texture, texOffX + width ,texOffY);
		c3 = TextureTools.getTextureCorner(this.texture, texOffX + width, texOffY + height);
		c4 = TextureTools.getTextureCorner(this.texture, texOffX, texOffY + height);
	}

	@Override
	public boolean isHover() {
		return this.hover;
	}

	@Override
	public boolean setHover(boolean state) {
		this.hover = state;
		return true;
	}

}
