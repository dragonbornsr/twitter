/*
	Copyright (c) 2014, Coffea Games
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	1. Redistributions of source code must retain the above copyright
	   notice, this list of conditions and the following disclaimer.
	2. Redistributions in binary form must reproduce the above copyright
	   notice, this list of conditions and the following disclaimer in the
	   documentation and/or other materials provided with the distribution.
	3. All advertising materials mentioning features or use of this software
	   must display the following acknowledgement:
	   This product includes software developed by the Coffea Games.
	4. Neither the name of the Coffea Games nor the
	   names of its contributors may be used to endorse or promote products
	   derived from this software without specific prior written permission.
	
	THIS SOFTWARE IS PROVIDED BY COFFEA GAMES ''AS IS'' AND ANY
	EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
	WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL COFFEA GAMES BE LIABLE FOR ANY
	DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
	(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
	LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
	ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
	(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
package com.coffeagame.Engine.gui;

import java.util.ArrayList;
import java.util.List;

import com.coffeagame.Engine.Objects.DrawableGameObject;

public class Gui extends DrawableGameObject {

	public List<GuiButton> listOfGuiButtons = new ArrayList<GuiButton>();
	public List<GuiTile> listOfGuiTiles = new ArrayList<GuiTile>();
	public List<GuiTextArea> listOfGuiTextAreas = new ArrayList<GuiTextArea>();
	public List<GuiTextBox> listOfGuiTextBoxes = new ArrayList<GuiTextBox>();
	/**
	 * Boolean focus defines, if GUI is on top of others. This Must be set by
	 * GUI handler
	 */
	protected boolean isFocused = false;

	public int height, width, posX, posY, posZ;
	protected int defaultPosZ = 0;
	public boolean isVisible;
	protected float roll = 0.0f;

	public boolean getIsFocused() {
		return isFocused;
	}

	public void setFocused(boolean focus) {
		this.isFocused = focus;
	}

	/**
	 * set GUI size
	 * 
	 * @param height
	 * @param width
	 */
	public void setSize(int height, int width) {
		this.height = height;
		this.width = width;
	}

	/**
	 * Set GUI position
	 * 
	 * @param x
	 * @param y
	 */
	public void setPosition(int x, int y) {
		this.posX = x;
		this.posY = y;
		this.posZ = this.defaultPosZ;
	}

	/**
	 * Allows GUI to be drawn also with Z scale.
	 * 
	 * @param x
	 * @param y
	 * @param z
	 */
	public void setPosition(int x, int y, int z) {
		this.posX = x;
		this.posY = y;
		this.posZ = z;
	}

	public void setRoll(float roll) {
		this.roll = roll;
	}

	public void register(int priority) {
		GuiController.getListOfGuis().add(this);
		super.register(priority);
	}
	/**
	 * Method that is used by buttons to forward that they were pressed.
	 * @param buttonID
	 */
	public void buttonPressed(int buttonID) {

	}
	/**
	 * Method that is used by tiles to forward that they were pressed.
	 * @param tileID
	 */
	public void tilePressed(int tileID) {

	}
}
