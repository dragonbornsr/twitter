package com.coffeagame.Engine.gui;

import java.awt.Font;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.GL11;
import org.newdawn.slick.Color;
import org.newdawn.slick.TrueTypeFont;

import com.coffeagame.Engine.Objects.DrawableGameObject;
import com.coffeagame.Engine.event.interfaces.IKeyEvent;
import com.coffeagame.Engine.event.interfaces.IMouseEvent;
import com.coffeagame.Engine.event.types.KeyEvent;
import com.coffeagame.Engine.event.types.MouseEvent;
import com.coffeagame.Engine.media.ResourceManager;
import com.coffeagame.Engine.scene.GLViewPort;

public class GuiTextArea extends DrawableGameObject implements IMouseEvent, IKeyEvent{
	protected String text = "";
	protected String[] textInLines;
	protected int width = 0;
	protected int height = 0;
	
	protected Gui parent;
	protected boolean visible = false;
	protected int ID = 0;
	
	protected Font awtFont = null;
	protected Color fontcolor = Color.white;
	protected TrueTypeFont font;
	protected int characterSize = 0;
	
	protected List<int[]> indexes = new ArrayList<int[]>();
	protected List<String> lines = new ArrayList<String>();
	
	protected boolean caps = false;
	
	protected int currentFirstRow = 0;
	
	public GuiTextArea(Gui parent, int ID){
		this.parent = parent;
		this.ID = ID;
		parent.listOfGuiTextAreas.add(this);
		register(parent.priority + 1);
		
		awtFont = ResourceManager.getFont("TimesNewRoman");
		awtFont = awtFont.deriveFont(Font.BOLD, 20.0f);
		font = new TrueTypeFont(awtFont, true);
	}
	
	public void setText(String text){
		this.text = text;
		this.textInLines = this.splitIntoLines();
	}
	public void setDimensions(int width, int height){
		this.width = width;
		this.height = height;
	}
	
	private String[] splitIntoLines(){
		StringTokenizer tok = new StringTokenizer(this.text, " ");
		StringBuilder output = new StringBuilder(this.text.length());
		int lineLen = 0;

		int maxCharInLine = (int) (this.width*0.11);

		while(tok.hasMoreTokens()){
			String word = tok.nextToken();
			
			 while(word.length() > maxCharInLine){
				 	int endIndex = (maxCharInLine-lineLen<0)?maxCharInLine:maxCharInLine-lineLen;
				 	System.out.println("lineLen : "+lineLen);
		            output.append(word.substring(0, endIndex) + "\n");
		            word = word.substring(endIndex);
		            lineLen = 0;
		        }

		        if (lineLen + word.length() > maxCharInLine) {
		            output.append("\n");
		            lineLen = 0;
		        }
		        output.append(word + " ");

		        lineLen += word.length() + 1;
		}
		
		return output.toString().split("\n");
	}
	
	@Override
	public void draw(float delta) {
		GLViewPort.startGLMode2D();
		GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
		GL11.glEnable(GL11.GL_BLEND);
		
		try{
			for(int e=0;e<this.textInLines.length;e++){
				if(this.height<(e+1)*font.getLineHeight()){
					break;
				}
					/*font.drawString(this.parent.posX + 5, this.parent.posY + 5 + e*font.getLineHeight(), 
								this.text.substring(e*50, (e+1)*50-1), fontcolor);*/
				font.drawString(this.parent.posX + 5, this.parent.posY + 5 + e*font.getLineHeight(), 
						this.textInLines[e+this.currentFirstRow]);	
			}
		}catch(Exception ex){
			
		}
		
		GL11.glDisable(GL11.GL_BLEND);
		GL11.glPopMatrix();
		GLViewPort.endGLMode2D();
	}
	
	@Override
	public boolean isEnabled() {
		return parent.isVisible;
	}

	@Override
	public boolean isDeleted() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isHover() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean setHover(boolean state) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void mousePressed(MouseEvent e) {
		
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseMoved(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean inBounds(MouseEvent e) {
		// TODO Auto-generated method stub
		return false;
	}
	
	private void addCharacter(String c){
		if(c.equals("SPACE")){
			c = " ";
		}else if(c.equals("RETURN")){
			c = "\n";
		}
		this.text += (this.caps)?c.toUpperCase():c.toLowerCase();
		this.textInLines = this.splitIntoLines();
	}
	
	private void removeLastCharacter(){
		if(this.text.length()>0){
			this.text = this.text.substring(0,this.text.length()-1);
			this.textInLines = this.splitIntoLines();
		}
	}

	@Override
	public void keyPressed(KeyEvent key) {
		/*if(key==201){
			this.currentFirstRow -= (currentFirstRow-1>=0)?1:0;
		}else if(key==209){
			this.currentFirstRow += (currentFirstRow+1<this.textInLines.length)?1:0;
		}else if(key==Keyboard.KEY_CAPITAL){
			this.caps = !this.caps;
			System.out.println(this.caps);
		}else if(key==Keyboard.KEY_BACK){
			this.removeLastCharacter();
		}else{
		
			Pattern pattern = Pattern.compile("^(?:(^[a-zA-Z0-9]$|\\b(?:SPACE|RETURN)\\b))$");
			Matcher matcher = pattern.matcher(Keyboard.getKeyName(key));
			if(matcher.find()){
				System.out.println("Sa vajutasid sobiva t�he/numbri:"+Keyboard.getKeyName(key));
				this.addCharacter(Keyboard.getKeyName(key));
			}*/
		
		this.addCharacter(Keyboard.getKeyName(key.getModifiedKey()));
	}

	@Override
	public void keyReleased(KeyEvent key) {
		// TODO Auto-generated method stub
		
	}
}
