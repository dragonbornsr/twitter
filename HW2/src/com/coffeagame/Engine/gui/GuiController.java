/*
	Copyright (c) 2014, Coffea Games
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	1. Redistributions of source code must retain the above copyright
	   notice, this list of conditions and the following disclaimer.
	2. Redistributions in binary form must reproduce the above copyright
	   notice, this list of conditions and the following disclaimer in the
	   documentation and/or other materials provided with the distribution.
	3. All advertising materials mentioning features or use of this software
	   must display the following acknowledgement:
	   This product includes software developed by the Coffea Games.
	4. Neither the name of the Coffea Games nor the
	   names of its contributors may be used to endorse or promote products
	   derived from this software without specific prior written permission.
	
	THIS SOFTWARE IS PROVIDED BY COFFEA GAMES ''AS IS'' AND ANY
	EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
	WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL COFFEA GAMES BE LIABLE FOR ANY
	DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
	(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
	LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
	ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
	(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
package com.coffeagame.Engine.gui;

import java.util.ArrayList;
import java.util.List;

import com.coffeagame.Engine.Objects.CoreObject;
import com.coffeagame.Engine.event.KeyController;
import com.coffeagame.Engine.event.interfaces.IKeyEvent;
import com.coffeagame.Engine.event.types.KeyEvent;
/**
 * Class that handles all GUI's in the game! Tells them who is on top, who is
 * enabled etc.
 * 
 * @author ROG
 * 
 */
public class GuiController extends CoreObject implements IKeyEvent {

	private static List<Gui> listOfGuis = new ArrayList<Gui>();

	public GuiController() {
		KeyController.getListOfKeyEventListeners().add(this);
		init();
	}

	/**
	 * Create all GUI instances.
	 */
	protected void init() {

	}

	@Override
	public void update(float delta) {

	}

	@Override
	public boolean isEnabled() {
		return true;
	}

	@Override
	public boolean isDeleted() {
		return false;
	}

	public static List<Gui> getListOfGuis() {
		return listOfGuis;
	}

	public static void setListOfGuis(List<Gui> listOfGuis) {
		GuiController.listOfGuis = listOfGuis;
	}

	@Override
	public void keyPressed(KeyEvent key) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyReleased(KeyEvent key) {
		// TODO Auto-generated method stub
		
	}
}
