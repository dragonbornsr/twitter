package com.coffeagame.Engine.gui;

import java.awt.Font;

import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.GL11;
import org.newdawn.slick.Color;
import org.newdawn.slick.TrueTypeFont;
import org.newdawn.slick.opengl.Texture;
import org.newdawn.slick.opengl.TextureImpl;

import com.coffeagame.Engine.Game;
import com.coffeagame.Engine.Objects.DrawableGameObject;
import com.coffeagame.Engine.event.interfaces.IKeyEvent;
import com.coffeagame.Engine.event.interfaces.IMouseEvent;
import com.coffeagame.Engine.event.types.KeyEvent;
import com.coffeagame.Engine.event.types.MouseEvent;
import com.coffeagame.Engine.media.ResourceManager;
import com.coffeagame.Engine.scene.GLViewPort;

public class GuiTextBox extends DrawableGameObject implements IMouseEvent, IKeyEvent{
	
	protected String text = "";
	protected String displayString = "";
	protected String defaultString = "";
	protected String fontName = "TimesNewRoman";
	protected int width = 0;
	protected int height = 0;
	protected float padding = 3f;
	
	protected int posX, posY;
	
	protected Gui parent;
	protected int ID = 0;
	
	protected Font awtFont = null;
	protected Color fontcolor = Color.white;
	protected TrueTypeFont font;
	protected float fontSize = 20f;

	protected int maxStringLenght = -1;
	protected boolean limitedLenght = false;
	
	protected Texture background;
	
	protected boolean caps = false;
	
	protected int currentFirstRow = 0;
	
	protected String cursor = "|";
	protected float cursorSpeed = 1.2f;
	private float tick = 0;
	private boolean cursorEnabled = true;
	
	private float deleteThreshold = 0;
	
	private String pattern = "^[0-9a-zA-Z -,.#@?��������]*$";
	
	public GuiTextBox(Gui parent, int ID){
		this.parent = parent;
		this.ID = ID;
		parent.listOfGuiTextBoxes.add(this);
		register(parent.priority + 1);
		
		awtFont = ResourceManager.getFont(fontName);
		awtFont = awtFont.deriveFont(Font.BOLD, fontSize);
		font = new TrueTypeFont(awtFont, true);
	}
	
	public GuiTextBox setText(String str){
		this.text = str;
		return this;
	}
	
	public GuiTextBox setDefaultString(String str){
		this.defaultString = str;
		this.updateDisplayString();
		return this;
	}
	
	public void setFocused() {
		Game.focused = this;
		this.updateDisplayString();
	}
	
	public GuiTextBox setFontSize(float size) {
		this.fontSize = size;
		awtFont = ResourceManager.getFont(fontName);
		awtFont = awtFont.deriveFont(Font.BOLD, size);
		font = new TrueTypeFont(awtFont, true);
		return this;
	}
	
	public GuiTextBox setFontColor(Color c) {
		this.fontcolor = c;
		return this;
	}
	
	public GuiTextBox setFontName(String name) {
		this.fontName = name;
		awtFont = ResourceManager.getFont(fontName);
		awtFont = awtFont.deriveFont(Font.BOLD, fontSize);
		font = new TrueTypeFont(awtFont, true);
		return this;
	}
	
	public GuiTextBox setBackground(Texture bg) {
		background = bg;
		return this;
	}
	public GuiTextBox setCursorEnabled(boolean enabled) {
		this.cursorEnabled = enabled;
		return this;
	}
	
	public GuiTextBox setRegex(String regex) {
		this.pattern = regex;
		return this;
	}
	public GuiTextBox setPositionAndSize(int x, int y, int width, int height){
		this.width = width;
		this.height = height;
		this.posX = x;
		this.posY = y;
		return this;
	}
	public GuiTextBox setMaxTextLenght(int lenght, boolean limit){
		this.limitedLenght = limit;
		this.maxStringLenght = lenght;
		return this;
	}
	
	public GuiTextBox setCursorSpeed(float speed){
		this.cursorSpeed = speed;
		return this;
	}
	
	@Override
	public void update(float delta) {
		if (Game.isFocused(this)) {
			tick += delta*cursorSpeed;
			if (tick > 60) {
				tick = 0;
				cursor = cursor.equals("")?"|":"";
			}
			
			if (Keyboard.isKeyDown(Keyboard.KEY_BACK)) {
				this.deleteThreshold += delta;
				if (this.deleteThreshold > 33) {
					this.removeLastCharacter();
					this.updateDisplayString();
					this.deleteThreshold -= 3;
				} 
			 } else 
			 {
				 if (this.deleteThreshold != 0)
					 this.deleteThreshold = 0;
			 }
		}
	}
	
	@Override
	public void draw(float delta) {
		GLViewPort.startGLMode2D();
		
		GL11.glPushMatrix();
		TextureImpl.bindNone();
		Color.white.bind();
		GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
		GL11.glEnable(GL11.GL_BLEND);
		background.bind();
		GL11.glBegin(GL11.GL_QUADS);
			GL11.glTexCoord2f(0, 0);
			GL11.glVertex2f(parent.posX + posX,  parent.posY + posY);
			GL11.glTexCoord2f(1, 0);
			GL11.glVertex2f(parent.posX + posX + width,  parent.posY + posY);
			GL11.glTexCoord2f(1, 1);
			GL11.glVertex2f(parent.posX + posX + width,  parent.posY + posY + height);
			GL11.glTexCoord2f(0, 1);
			GL11.glVertex2f(parent.posX + posX,  parent.posY + posY + height);
		GL11.glEnd();
		GL11.glPopMatrix();	
		
		GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
		GL11.glEnable(GL11.GL_BLEND);
		TextureImpl.bindNone();
		Color.white.bind();
				font.drawString(this.parent.posX + this.posX + padding, this.parent.posY + 
						this.posY + (this.height / 2 - (this.fontSize / 1.5f)), 
						this.displayString + (Game.isFocused(this)&&isCursorEnabled()?cursor:""), this.fontcolor);	
		
		GL11.glDisable(GL11.GL_BLEND);
		GL11.glPopMatrix();
		GLViewPort.endGLMode2D();
	}
	
	@Override
	public boolean isEnabled() {
		return parent.isVisible;
	}

	@Override
	public boolean isDeleted() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isHover() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean setHover(boolean state) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void mousePressed(MouseEvent e) {
		Game.focused = this;
		updateDisplayString();
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseMoved(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean inBounds(MouseEvent e) {
		if ((posX + parent.posX <= e.posX) && (posY + parent.posY <= e.posY))
			if ((posX + parent.posX + width >= e.posX)
			&& (posY + height + parent.posY >= e.posY))
			return true;
		this.updateDisplayString();
		if (Game.isFocused(this) && e.key != -1) {
			Game.focused = null;
		}
		return false;
	}
	
	private void addCharacter(String c){
		if (text.length() < this.maxStringLenght || !this.limitedLenght)
			this.text += c;
	}
	
	private void removeLastCharacter(){
		if(this.text.length()>0){
			this.text = this.text.substring(0,this.text.length()-1);
		}
	}
	
	public String getText() {
		return this.text;
	}
	
	public boolean isEmty() {
		if (this.text.length() < 1)
			return true;
		return false;
	}
	
	public void updateDisplayString() {
		if (text.equals("") && !Game.isFocused(this))
			this.displayString = fitStringIntoField(this.defaultString);
		else
			this.displayString = fitStringIntoField(text);
	}
	
	public boolean isCursorEnabled() {
		return this.cursorEnabled;
	}
	
	protected String fitStringIntoField(String a) {
		String result = "";
		float length = 0f;
		for (int pos = a.length(); pos > 0; pos--) {
			String c = a.substring(pos - 1,pos);
			length += this.font.getWidth(c);
			if (length > this.width - (2*this.padding) - (isCursorEnabled()?this.font.getWidth("|")/3:0)) {
				return result;
			}
			result = c + result;
		}
		return result;
	}

	@Override
	public void keyPressed(KeyEvent key) {
		if (Game.isFocused(this)) {
			String k = key.getModifiedKeyAsString();
			
			if (key.getKey() == Keyboard.KEY_BACK) {
				this.removeLastCharacter();
				this.updateDisplayString();
				return;
			}
			
			if (k.matches(this.pattern)) {
				this.addCharacter(k);
				this.updateDisplayString();
				return;
			} 
		}
	}

	@Override
	public void keyReleased(KeyEvent key) {
		// TODO Auto-generated method stub
		
	}
}
