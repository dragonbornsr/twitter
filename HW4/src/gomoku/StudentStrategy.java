package gomoku;

import java.util.ArrayList;
public class StudentStrategy implements ComputerStrategy {

	private static int maxDepth = 3 - 1;
	private static float reverse = (maxDepth % 2 != 0)?-1:1;

	private static int myButton = 0;
	
	
	
	@Override
	public Location getMove(SimpleBoard board, int player) {
		if (myButton == 0)
			myButton = player;
		
		
		ArrayList<Location> locations = possibleMoves(board);
		float b = Float.NEGATIVE_INFINITY;
		Location best = null;
		
		for (Location l : locations) {
			board.getBoard()[l.getRow()][l.getColumn()] = player;
			if (checkWin(board.getBoard()))
				best = l;
				ArrayList<Location> locations2 = possibleMoves(board);
				for (Location l2 : locations2) {
					board.getBoard()[l2.getRow()][l2.getColumn()] = player;
					if (checkWin(board.getBoard()))
						best = l2;
					board.getBoard()[l2.getRow()][l2.getColumn()] = SimpleBoard.EMPTY;
				}
			board.getBoard()[l.getRow()][l.getColumn()] = SimpleBoard.EMPTY;
		}
		if (best != null) return best;
		
		for (Location l : locations) {
			board.getBoard()[l.getRow()][l.getColumn()] = player;
			if (checkWin(board.getBoard()))
				best = l;
				ArrayList<Location> locations2 = possibleMoves(board);
				for (Location l2 : locations2) {
					board.getBoard()[l2.getRow()][l2.getColumn()] = player;
					if (checkWin(board.getBoard()))
						best = l2;
					board.getBoard()[l2.getRow()][l2.getColumn()] = SimpleBoard.EMPTY;
				}
			board.getBoard()[l.getRow()][l.getColumn()] = SimpleBoard.EMPTY;
		}
		if (best != null) return best;
		
		for (Location l : locations) {
			board.getBoard()[l.getRow()][l.getColumn()] = -player;
			if (checkWin(board.getBoard()))
				best = l;
				ArrayList<Location> locations2 = possibleMoves(board);
				for (Location l2 : locations2) {
					board.getBoard()[l2.getRow()][l2.getColumn()] = -player;
					if (checkWin(board.getBoard()))
						best = l2;
					board.getBoard()[l2.getRow()][l2.getColumn()] = SimpleBoard.EMPTY;
				}
			board.getBoard()[l.getRow()][l.getColumn()] = SimpleBoard.EMPTY;
		}
		if (best != null) return best;
		
		best = checkHybridWin(0,board, player);
		if (best != null) return best;
		
		for (Location l : locations) {
			Object[] o = miniMax(0, board, l, 
					(player == SimpleBoard.PLAYER_WHITE)?true:false);
			if ((float)o[1] > b) {
				b = (float)o[1];
				best = l;
			}
		}
		return best;
	}
	
	private float locationEvaluation(SimpleBoard b, boolean player) {
		float res = 0;
		int count = 0;
		boolean firstBlocked = true;
		//check diagonals
		  for( int k = 0 ; k < b.getHeight() * 2 ; k++ ) {
		        for( int j = 0 ; j <= k ; j++ ) {
		            int i = k - j;
		            if( i < b.getHeight() && j < b.getHeight() ) {
		            	
		            	if ((b.getBoard()[i][j]) == ((player)?SimpleBoard.PLAYER_WHITE:SimpleBoard.PLAYER_BLACK)){
							count++;
						} else {
							res += (calcScore(count, firstBlocked, !(b.getBoard()[i][j] == SimpleBoard.EMPTY)));
							firstBlocked = !(b.getBoard()[i][j] == SimpleBoard.EMPTY);
							count = 0;
						}
		            }
		        }
		        count = 0;
		        firstBlocked = true;
		    }
		  for( int k = 0 ; k < b.getHeight() * 2 ; k++ ) {
		        for( int j = 0 ; j <= k ; j++ ) {
		            int i = k - j;
		            if( i < b.getHeight() && j < b.getHeight() ) {
		            	
		            	if ((b.getBoard()[j][i]) == ((player)?SimpleBoard.PLAYER_WHITE:SimpleBoard.PLAYER_BLACK)){
							count++;
						} else {
							res += (calcScore(count, firstBlocked, !(b.getBoard()[i][j] == SimpleBoard.EMPTY)));
							firstBlocked = !(b.getBoard()[i][j] == SimpleBoard.EMPTY);
							count = 0;
						}
		            }
		        }
		        count = 0;
		    }
		
		for (int x = 0 ; x < b.getHeight() ; x++) {
			for (int y = 0 ; y < b.getWidth() ; y++) {
				//System.out.print(b.getBoard()[x][y]);
				if ((b.getBoard()[y][x]) == ((player)?SimpleBoard.PLAYER_WHITE:SimpleBoard.PLAYER_BLACK)){
					count++;
				} else {
					res += (calcScore(count, firstBlocked, !(b.getBoard()[y][x] == SimpleBoard.EMPTY)));
					firstBlocked = !(b.getBoard()[y][x] == SimpleBoard.EMPTY);
					count = 0;
				}
			} 		      //  System.out.println();
		}///System.out.println();
		for (int x = 0 ; x < b.getHeight() ; x++) {
			for (int y = 0 ; y < b.getWidth() ; y++) {
				if ((b.getBoard()[x][y]) == ((player)?SimpleBoard.PLAYER_WHITE:SimpleBoard.PLAYER_BLACK)){
					count++;
				} else {
					res += (calcScore(count, firstBlocked, !(b.getBoard()[y][x] == SimpleBoard.EMPTY)));
					firstBlocked = !(b.getBoard()[y][y] == SimpleBoard.EMPTY);
					count = 0;
				}
			}
		}
		
		//System.out.println(res);
		return res*reverse;
	}
	
	private ArrayList<Location> possibleMoves(SimpleBoard b) {
		ArrayList<Location> moves = new ArrayList<Location>();
		for (int x = 0 ; x < b.getHeight() ; x++) {
			for (int y = 0 ; y < b.getWidth() ; y++) {
				if (b.getBoard()[x][y] == SimpleBoard.EMPTY)
					moves.add(new Location(x,y));
			}
		}
		return moves;
	}

	@Override
	public String getName() {
		return "Oliver Tiit";
	}
	
	public Object[] miniMax(int depth, SimpleBoard b, Location p, boolean player) {
		if (depth >= maxDepth) {
			SimpleBoard b2 = new SimpleBoard(b.getBoard());
			b2.getBoard()[p.getRow()][p.getColumn()] = (player)?SimpleBoard.PLAYER_WHITE:SimpleBoard.PLAYER_BLACK;
			float score = locationEvaluation(b2, player);
			b2.getBoard()[p.getRow()][p.getColumn()] = SimpleBoard.EMPTY;
			return new Object[] {p, score};
		}
		
		ArrayList<Location> moves = possibleMoves(b);
		Object[] best = null;
		for (Location l : moves) {
			SimpleBoard b2 = new SimpleBoard(b.getBoard());
			b2.getBoard()[p.getRow()][p.getColumn()] = (player)?SimpleBoard.PLAYER_WHITE:SimpleBoard.PLAYER_BLACK;
			Object[] v2 = miniMax(depth + 1, b2, l, player);
			b2.getBoard()[p.getRow()][p.getColumn()] = SimpleBoard.EMPTY;
			float score = -(float)v2[1];
			if (best == null)
				 best = new Object[] {l, score};
			if (score > (float)best[1]) {
				best = new Object[] {l, score};
			}
			if ((float)best[1] >= score)
				break;
		}
		return (best);
	}
	
    public boolean checkWin(int[][] b) {
        for (int i = 0; i < b.length; i++) {
                for (int j = 0; j < b.length; j++) {
                        if(f(i,j, 0, 1, b) || f(i,j, 1,1, b) || f(i,j, 1, 0, b) || f(i,j, 1, -1, b)) {
                                return true;
                        }
                }
        }
        return false;
    }
    
    public boolean f(int x, int y, int dx, int dy, int[][] b) {
        if(x + dx*4 >= b.length || y + dy*4 >= b.length || x + dx*4 < 0 || y + dy*4 < 0) {
                return false;
        }
        int score = 0;
        for (int i = 0; i < 5; i++) {
                score+= b[x + i*dx][y + i*dy];
                if(Math.abs(score) != i + 1) {
                        return false;
                }
        }
        return true;
    }
    
    public Location checkHybridWin(int depth, SimpleBoard b, int player) {
    	Location best = null;
    	if (depth >= maxDepth) return null;
    	ArrayList<Location> locations2 = possibleMoves(b);
		for (Location l2 : locations2) {
			b.getBoard()[l2.getRow()][l2.getColumn()] = player;
			if (checkWin(b.getBoard())) {
				b.getBoard()[l2.getRow()][l2.getColumn()] = SimpleBoard.EMPTY;
				return l2;
			}else { 
				best = checkHybridWin(depth+1, b, -player);
			}
			b.getBoard()[l2.getRow()][l2.getColumn()] = SimpleBoard.EMPTY;
			if (best!=null)
				return best;
		}
		return null; 
    }
    
    public float calcScore(int count, boolean f, boolean l) {
    	int mult = 0;
    	if (f) mult += 1;
    	if (l) (mult) += 1;
    	if (l && f) mult += 1;
    	float res = 0;
    	if (count == 0) res += 0;
		if (count == 1) res += 10;
		if (count == 2) res += 100;
		if (count == 3) res += 1000;
		if (count == 4) res += 10000;
		if (count == 5) res += 100000;
		
		return res*mult;
    }	
		
}
